/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver.h
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPMSOLVER_H
#define DFPMSOLVER_H

#define VERSION 0.0.1

#endif // DFPMSOLVER_H //
