/*-------------------------------------------------------

 - Module : intpt
 - File : common.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_INTPT_COMMON_HPP
#define DFPM_INTPT_COMMON_HPP

//! \file common.hpp common definition for interior point methods

#include <vector>
#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace dfpmsolver {
namespace intpt {

// Typedef
// -------

using index_t = int;
using scalar_t = double;
using vector_t = Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>;
using matrix_t = Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
using spmatrix_t  = Eigen::SparseMatrix<scalar_t>;
using triplet_t = Eigen::Triplet<scalar_t>;
using list_triplet_t = std::vector<triplet_t>;


} // end namespace intpt

} // end namespace dfpmsolver

#endif // DFPM_INTPT_COMMON_HPP
