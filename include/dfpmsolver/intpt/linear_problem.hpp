/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver/linear_problem.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_LINEAR_PROBLEM_HPP
#define DFPM_LINEAR_PROBLEM_HPP

//! \file linear_problem.hpp description of a linear problem as solve by linear interior point method

#include <cassert>
#include <cmath>

#include "common.hpp"

namespace dfpmsolver {
//! namespace intpt namespace for interior point method
namespace intpt {

class LinearProblem
{
public:
    LinearProblem(int nb_variables, int nb_constraints, scalar_t infinity=1e19):
        m_nb_variables(nb_variables),
        m_nb_constraints(nb_constraints),
        m_obj_function(vector_t::Zero(nb_variables)),
        m_obj_shift(0),
        m_constraints(nb_constraints, nb_variables),
        m_lower_bound_constraints(matrix_t::Constant(nb_constraints, 1, -infinity)),
        m_upper_bound_constraints(matrix_t::Constant(nb_constraints, 1, infinity)),
        m_lower_bound_variables(matrix_t::Constant(nb_variables, 1, -infinity)),
        m_upper_bound_variables(matrix_t::Constant(nb_variables, 1, infinity))
    {
    }

    // Initialization
    // ---------------

    //! Set the objective function
    void set_objective(vector_t& objective, scalar_t shift=0) {
        assert(objective.rows() == get_nb_variables());
        m_obj_function = objective;
        m_obj_shift = shift;
    }

    //! Set the constraints from a list of coefficient (row, col, value)
    void set_constraints(list_triplet_t& coefficients) {
        m_constraints.setFromTriplets(coefficients.begin(), coefficients.end());
    }

    // Variables bound
    // ---------------

    //! Set lower bounds of the variables
    void set_var_lower_bounds(vector_t& lower_bounds) {
        assert(lower_bounds.rows() == get_nb_variables());
        m_lower_bound_variables = lower_bounds;
    }
    //! Set upper bounds of the variables
    void set_var_upper_bounds(vector_t& upper_bounds) {
        assert(upper_bounds.rows() == get_nb_variables());
        m_upper_bound_variables = upper_bounds;
    }

    //! Read only access to lower bound of x_i
    scalar_t var_lower_bound(index_t i) const {return m_lower_bound_variables(i);}
    //! Read only access to lower bound of x_i
    scalar_t& var_lower_bound(index_t i) {return m_lower_bound_variables(i);}
    //! Read only access to upper bound of x_i
    scalar_t var_upper_bound(index_t i) const {return m_upper_bound_variables(i);}
    //! Read only access to upper bound of x_i
    scalar_t& var_upper_bound(index_t i) {return m_upper_bound_variables(i);}

    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    vector_t& lower_bounds_variables() {return m_lower_bound_variables;}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    vector_t& upper_bounds_variables() {return m_upper_bound_variables;}

    // Constraints bound
    // -----------------

    //! Set lower bounds of the constraints
    void set_constraints_lower_bounds(vector_t& lower_bounds) {
        assert(lower_bounds.rows() == get_nb_constraints());
        m_lower_bound_constraints = lower_bounds;
    }
    //! Set upper bounds of the constraints
    void set_constraints_upper_bounds(vector_t& upper_bounds) {
        assert(upper_bounds.rows() == get_nb_constraints());
        m_upper_bound_constraints = upper_bounds;
    }

    //! Read only access to lower bound of constraints i
    scalar_t constraints_lower_bound(index_t i) const {return m_lower_bound_constraints(i);}
    //! Read only access to lower bound of constraints i
    scalar_t& constraints_lower_bound(index_t i) {return m_lower_bound_constraints(i);}
    //! Read only access to upper bound of constraints i
    scalar_t constraints_upper_bound(index_t i) const {return m_upper_bound_constraints(i);}
    //! Read only access to upper bound of constraints i
    scalar_t& constraints_upper_bound(index_t i) {return m_upper_bound_constraints(i);}

    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    vector_t& lower_bounds_constraints() {return m_lower_bound_constraints;}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    vector_t& upper_bounds_constraints() {return m_upper_bound_constraints;}

    // Access to problem
    // -----------------

    //! Return the number of variables
    index_t get_nb_variables() {return m_nb_variables;}
    //! Return the number of constraints
    index_t get_nb_constraints() {return m_nb_constraints;}

    //! Return the value of the objective function at x
    scalar_t get_value_objective(vector_t& x) {
        assert(x.rows() == get_nb_variables());
        return m_obj_function.transpose()*x + m_obj_shift;
    }

    //! return the vector of value of the objective
    vector_t& get_objective() { return m_obj_function;}
    //! Return offset of the objective function
    scalar_t get_objective_shift() {return m_obj_shift;}

    //! Return  values of the constraints at x
    vector_t get_values_constraints(vector_t& x) {return m_constraints*x;}

    //! Return the sparse matrix of the constraints
    spmatrix_t& get_constraints() {return m_constraints;}

private:
    index_t m_nb_variables;    //!< number of variables    = n
    index_t m_nb_constraints;  //!< number of constraints  = m

    vector_t m_obj_function;   //!<  the objective function = c
    scalar_t m_obj_shift;      //!< shift of the objective function = f
                               //!< obj = c^T*x + f

    spmatrix_t m_constraints;            //!< The constraints
    vector_t  m_lower_bound_constraints; //!< Lower bound of the constraints
    vector_t  m_upper_bound_constraints; //!< Upper bound of the constraints

    vector_t m_lower_bound_variables;    //!< Lower bound of the variables
    vector_t m_upper_bound_variables;    //!< Upper bound of the variables
};

} // end namespace intpt
} // end namespace dfpmsolver


#endif // DFPM_LINEAR_PROBLEM_HPP
