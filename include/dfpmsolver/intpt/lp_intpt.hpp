/*-------------------------------------------------------

 - Module : intpt
 - File : lp_intpt.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFMP_LP_INTPT_HPP
#define DFMP_LP_INTPT_HPP

//! \file lp_intpt.hpp interior point method for linear problem

#include <memory>
#include "common.hpp"
#include "linear_problem.hpp"

namespace dfpmsolver {
namespace intpt {

//! Smart pointer to a Linear problem
using lpptr_t = std::shared_ptr<LinearProblem>;

//! Options to customize the behavior of the InteriorPointLP class
struct InteriorPointLPOptions
{
    double init_slack; //!< Value used to initialize the slack variables
    double tol_norm;   //!< Tolerance to decide of optimality
    double threshold_infeasible; //!< Threshold to decide of infeasibility
    double delta;      //!< Multiplication factor to update mu
    double underrelax_factor; //!< Underrelaxation factor to ensure positivity
    int max_iter;             //!< Maximum number of iterations

    InteriorPointLPOptions():
        init_slack(1000.0),
        tol_norm(1e-8),
        threshold_infeasible(1e19),
        delta(0.02),
        underrelax_factor(0.9),
        max_iter(500)
    {}
};

//! Return Code of the InteriorPointLP::solve method
enum ReturnCodeIntPtLP
{
    MaxIterReached   = -3, //!< Maximum number of iterations reached
    DualInfeasible   = -2, //!< Dual infeasibility
    PrimalInfeasible = -1, //!< Primal infeasibility
    NotConvergedYet = 0,   //!< No convergence yet
    Optimal = 1            //!< Optimal solution found
};

//! \brief Primal-Dual Interior-Point method for linear problem
//!
//! solve :
//!    maximize c^T x
//!    subject to  a =< Ax =< b
//!                l =<  x =< u
//!
//! Reference : Linear Programming, Foundations and Extensions
//!             Robert J. Vanderbei 2007 (third edition)
//!
class InteriorPointLP
{
    using solver_t = Eigen::SimplicialLDLT<spmatrix_t>;                           // the solver
    using selfadjoint_t = Eigen::SparseSelfAdjointView<spmatrix_t, Eigen::Upper>; // the matrix is self adjoint
public:
    //! Construct the solver
    //!
    //! problem is a smart pointer towards a linear problem.
    InteriorPointLP(lpptr_t problem):
        m_pb(problem)
    {}

    //! Solve the problem
    ReturnCodeIntPtLP solve(vector_t& x);

    //! Read-Write access to the options
    InteriorPointLPOptions& get_options() {return m_options;}


    //! Return the value of the objective function
    scalar_t primal_objective(vector_t x) {
        return m_pb->get_value_objective(x);
    }
    //! Return the value of the dual objective function
    scalar_t dual_objective() {
        return ((m_pb->upper_bounds_constraints().transpose()*sv_y) + m_pb->get_objective_shift());
    }

    //! Return the number of variable
    index_t get_nb_variables() {return m_pb->get_nb_variables();}
    //! Return the number of constraints
    index_t get_nb_constraints() {return m_pb->get_nb_constraints();}

    // Low-level functions
    // -------------------

    //! \brief Initialize the solver
    //!
    //! Should not be used, except for tests
    void initialization() {
        init_slack_variables();
        init_solver();
    }
    //! \brief One iteration of the interior point method
    //!
    //! Normally, this should not be used, except for test
    ReturnCodeIntPtLP one_iteration(vector_t& x);

private:
    //! Initialize the slack variables
    void init_slack_variables();
    //! Initialize the solver
    void init_solver();

    //! Solve the linear system
    vector_t solve_linear_system(vector_t& rhs);

    // Attribute
    // =========

    // Smart pointer to the linear problem
    lpptr_t m_pb;
    // Options to customize the solver
    InteriorPointLPOptions m_options;

    // slack variables
    vector_t sv_f;
    vector_t sv_p;
    vector_t sv_t;
    vector_t sv_g;
    vector_t sv_y; // dual variable
    vector_t sv_v;
    vector_t sv_q;
    vector_t sv_s;
    vector_t sv_h;

    // Primal, dual feasibility
    vector_t m_rho;
    vector_t m_sigma;
    // Parameter of log barrier function
    scalar_t m_mu;

    // Matrix for the newton system
    spmatrix_t m_matrix;
    // Solver of the newton system
    solver_t m_solver;
};

} // end namespace intpt
} // end namespace dfpmsolver

#endif // DFMP_LP_INTPT_HPP
