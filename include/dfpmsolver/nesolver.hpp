#ifndef NESOLVER_HPP
#define NESOLVER_HPP

#include <Eigen/Dense>
#include <functional>

namespace dfpmsolver {

using vector_t = Eigen::VectorXd;
using matrix_t = Eigen::MatrixXd;
using diag_matrix_t = Eigen::DiagonalMatrix<double, Eigen::Dynamic>;

//! QR decomposition
bool qr_decomposition(matrix_t& M, vector_t& M1, vector_t& M2);
void rsolve(matrix_t& M, vector_t& M2, vector_t& b);
double cond_estimates(matrix_t& M, vector_t& M2);
void qr_solve(matrix_t& M, vector_t& M1, vector_t& M2, vector_t& b);

// Cholesky decomposition
double cholesky_decomposition(matrix_t& H, double maxoffl, matrix_t& L);
void cholesky_solve(matrix_t& L, vector_t& g, vector_t& s);
void chol_lsolve(matrix_t& L, vector_t& y, vector_t& x);
void chol_ltsolve(matrix_t& L, vector_t& g, vector_t& s);

enum GlobalMethod
{
    NoGlobal,
    LineSearch,
    HookTrust,
    DogLeg
};

struct NESolverOptions
{
    bool anal_jac;  // True if analytic jacobian is used
    double eta;     // eta = 10^(-DIGITS) : Digits number of reliable digits
    double maxstep; // Max step
    double maxiter_maxstep; // number of max iterations at max step
    double steptol; // Tolerance for newton step
    double fvectol; // Tolerance for the residual
    int max_iter;   // maximum number of iterations
    GlobalMethod global;

    NESolverOptions():
        anal_jac(false),
        eta(1e-16),
        maxstep(1e3),
        maxiter_maxstep(5),
        steptol(1e-8),
        fvectol(1e-8),
        max_iter(200),
        global(LineSearch)
    {}
};

enum NewtonReturnState
{
    FailedGlobalMethod = -3,
    MaximumNewtonLength = -2,
    MaximumNumberOfIterations = -1,
    NoConvergenceYet = 0,
    NewtonSuccess = 1,
    ResidualMinimized = 2,
    ErrorMinimized = 3

};


struct HookStepParam
{
    double delta;
    double delta_prev;
    double mu;
    bool firsthook;
    double newtlen;
    bool newttaken;
    vector_t s;
    vector_t x_prev;
    vector_t xc;
    double phi;
    double phiprime;
    double phiprimeinit;
    int retcode;
    double fcp_prev;
};

class NESolver
{
public:
    // void ResidualF(solution, residual)
    using ResidualF_t = std::function<void (const vector_t&, vector_t&)> ;
    // void JacobianF(solution, residual, jacobian)
    using JacobianF_t = std::function<void (vector_t&, matrix_t&)>;


    NESolver(int neq, ResidualF_t residual_f):
        m_neq(neq),
        m_options(),
        m_compute_residuals(residual_f),
        m_compute_jacobian(helper_func_num_jacobian()),
        m_scaling_x(Eigen::VectorXd::Ones(neq)),
        m_scaling_f(Eigen::VectorXd::Ones(neq)),
        m_residuals(neq),
        m_jacobian(neq, neq),
        m_gradient(neq),
        m_max_taken(false)
    {
    }

    // ### Further settings ### //

    void set_anal_jac(JacobianF_t jacobian_f, bool analytic_jacobian) {
        m_options.anal_jac = analytic_jacobian;
        m_compute_jacobian = jacobian_f;
    }

    void set_scaling_residual(vector_t& scaling) {m_scaling_f = scaling;}
    // ### Access to attribute ### //

    int get_neq() {return m_neq;}
    NESolverOptions& get_options() {return m_options;}
    void set_global_method(GlobalMethod method) {
        get_options().global = method; }

    vector_t& get_residual_ref() {return m_residuals;}
    matrix_t& get_jacobian_ref() {return m_jacobian;}

    double scaling_x(int j) {return m_scaling_x(j);}
    double scaling_f(int j) {return m_scaling_f(j);}

    int get_nb_iter() {return nb_iter;}
    // ### Residual stuff ### //

    //! Compute the residual
    void compute_residual(const vector_t& x) {
        m_compute_residuals(x, m_residuals);
    }

    //! Compute the residual and store it in 'residual'
    void compute_residual(const vector_t& x, vector_t& residual) {
        m_compute_residuals(x, residual);
    }

    //! compute the scaled squared norm of the residual
    double squared_residual(const vector_t& x);

    //! compute the jacobian
    void compute_jacobian(vector_t& x) {
        m_compute_jacobian(x, m_jacobian);
    }

    //! \brief compute the gradient
    //!
    //! Assume that the residual and the jacobian had been computed before
    void compute_gradient(vector_t& x)
    {
        m_gradient = m_jacobian.transpose() * m_scaling_x.asDiagonal() * m_scaling_x.asDiagonal() * m_residuals;
    }
    double norm_inf_residual() {
        return (m_scaling_f.asDiagonal() * m_residuals).cwiseAbs().maxCoeff();
    }

    double norm_2_residual() {
        return (m_scaling_f.asDiagonal() * m_residuals).norm();
    }

    double norm_inf_solution(const vector_t& solution) {
        return (m_scaling_x.asDiagonal() * solution).cwiseAbs().maxCoeff();
    }

    double norm_2_solution(const vector_t& solution) {
        return (m_scaling_x.asDiagonal() * solution).norm();
    }

    double norm_inf_update(const vector_t& update, const vector_t& solution) {
        double maxi = std::abs(update(0))/(std::max(std::abs(solution(0)), 1/scaling_x(0)));
        for (int i=1; i<get_neq(); ++i)
        {
            maxi = std::max(maxi, std::abs(update(i))/(std::max(std::abs(solution(i)), 1/scaling_x(i))));
        }
        return maxi;
    }

    // ########## driver #####################

    //! driver
    int newton_driver(vector_t& solution);

    //! newton model
    int newton_model(vector_t& update, matrix_t &M, vector_t& M2, matrix_t &H, matrix_t& L);

    //! Check convergence, divegence, solution, ....
    NewtonReturnState check_convergence(int itncount, int retcode, vector_t& update, vector_t& solution);

    //! Backtracking linesearch
    int linesearch(vector_t& p, vector_t& x, double fc);

    // hook driver
    int hook_driver(int itncount,
                    HookStepParam& param,
                    double fc,
                    vector_t& update,
                    vector_t& solution,
                    matrix_t& H, matrix_t &L);

    void hook_step(HookStepParam& param, vector_t& update, matrix_t& H, matrix_t& L);
    void trust_region_update(int steptype, HookStepParam& param, vector_t& solution, double fc, matrix_t& H);

protected:
    //! Compute the jacobian using finite difference jacobian
    void finite_difference_jacobian(vector_t& solution,
                                    matrix_t& jacobian);

    //! helper function that return the residual function that compute the numerical jacobian
    JacobianF_t helper_func_num_jacobian() {
        return std::bind(
                    std::mem_fn(&NESolver::finite_difference_jacobian), this,
                    std::placeholders::_1, std::placeholders::_2
                    );
    }

private:
    int m_neq;
    NESolverOptions m_options;

    ResidualF_t m_compute_residuals;
    JacobianF_t m_compute_jacobian;

    vector_t m_scaling_x;
    vector_t m_scaling_f;
    vector_t m_residuals;
    matrix_t m_jacobian;
    vector_t m_gradient;

    bool m_max_taken;
    int m_consec_max;

    int nb_iter;
};

} // end namespace dfpmsolver

#endif // NESOLVER_HPP

