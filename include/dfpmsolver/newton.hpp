/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver/newton.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_NEWTON_H
#define DFPM_NEWTON_H

//! \file newton.hpp Driver for Newton, Quasi-newton method

#include <functional>
#include <string>
#include <Eigen/Dense>
#include "options.h"
//#include <iostream>
namespace dfpmsolver {


// The vector type
typedef Eigen::VectorXd VectorT;


enum IterationReturnCode
{
    NoConvergence,
    IterationSuccess,
    IterationNoChange,
    MaxIterations,
    DivergenceDetected,
    SingularMatrix,
    ZeroGradient
};

enum LineSearch
{
    NoLinesearch, //default
    Strang,
    Backtracking
};

//! Used to check the performance of the driver
struct PerformanceCounter
{
    PerformanceCounter():
        nb_call_residual(0),
        nb_call_jacobian(0),
        nb_inversions(0),
        nb_iterations(0)
    {}

    int nb_call_residual;
    int nb_call_jacobian;
    int nb_inversions;
    int nb_iterations;
    std::string message;
};

// ############ Norms ##############################

inline double norm_inf(const VectorT& residu)
{
    return  residu.array().abs().maxCoeff();
}

inline double norm_2(const VectorT& residu)
{
    return residu.norm();
}


// ############ Newton driver #######################

//! \brief The newton driver
//!
//!
template <class MatrixT, class Derived>
class NewtonDriverBase
{
public:
    // void ResidualF(solution, residual)
    typedef std::function<void (const VectorT&, VectorT&)> ResidualF;
    // void JacobianF(solution, residual, jacobian)
    typedef std::function<void (const VectorT&, MatrixT&)> JacobianF;
    // int SolveF(residual, jacobian, update)
    typedef std::function<int (const VectorT&, MatrixT&, VectorT&)> SolveF;
    // double ResidualNormF(residual)
    typedef std::function<double (const VectorT&)> ResidualNormF;
    //
    typedef std::function<double (const VectorT& residual0, VectorT& solution, VectorT& update)> LineSearchF;

    NewtonDriverBase(int nb_equations,
                     ResidualF f_residuals,
                     JacobianF f_jacobian):
        neq(nb_equations),
        m_tolerance(),
        m_performance(),
        m_compute_residual(f_residuals),
        m_compute_jacobian(f_jacobian),
        analjac(true)
    {
        common_init();
        set_linesearch(NoLinesearch);
    }

    NewtonDriverBase(int nb_equations,
                     ResidualF f_residuals):
        neq(nb_equations),
        m_tolerance(),
        m_performance(),
        m_compute_residual(f_residuals),
        analjac(false)
    {
        common_init();
        set_linesearch(NoLinesearch);
    }

    void set_linesearch(LineSearch linesearch)
    {
        switch (linesearch) {
        case Strang:
            m_linesearch = std::bind(std::mem_fn(&NewtonDriverBase::strang_linesearch),
                                     this, std::placeholders::_1,  std::placeholders::_2, std::placeholders::_3);
            break;
        case Backtracking:
            m_linesearch = std::bind(std::mem_fn(&NewtonDriverBase::backtracking_linesearch),
                                     this, std::placeholders::_1,  std::placeholders::_2, std::placeholders::_3);
            break;
        default:
            m_linesearch = std::bind(std::mem_fn(&NewtonDriverBase::no_linesearch),
                                     this, std::placeholders::_1,  std::placeholders::_2, std::placeholders::_3);
            break;

        }
    }

    //! Scaling of the solution vector
    void set_scaling(const VectorT& typx) {
        m_typx = typx;
    }
    //! Scaling of the solution vector and the residual
    void set_scaling(const VectorT& typx, const VectorT& typf) {
        m_typx = typx;
        m_typf = typf;
    }
    //! Scale norm
    double scaled_norm_inf(const VectorT& residual)
    {
        return (m_typf.asDiagonal().inverse() * residual).array().abs().maxCoeff();
    }


    //! Run the quasi-newton iterations
    IterationReturnCode run_quasinewton_iterations(VectorT& solution, int nstep);
    //! Run the newton iterations
    IterationReturnCode run_newton_iterations(VectorT& solution) {
        return run_quasinewton_iterations(solution, 1);
    }
    //IterationReturnCode run_broyden_iterations();


    void compute_residual(const VectorT& solution, VectorT& residual) {
        m_compute_residual(solution, residual);
        m_performance.nb_call_residual += 1;
    }

    void compute_jacobian(VectorT& solution,
                          VectorT& residual,
                          MatrixT& jacobian)
    {
        if (analjac) m_compute_jacobian(solution, jacobian);
        else compute_fd_jacobian(solution, residual, jacobian);
        m_performance.nb_call_jacobian += 1;
    }

    void compute_fd_jacobian(VectorT& solution, VectorT& residual, MatrixT& jacobian){
        return derived()->finite_difference_jacobian(solution, residual, jacobian);
    }

    int solve(const VectorT& residual,
               MatrixT& jacobian,
               VectorT& update)
    {
        int ret = derived()->solve_linear_system(residual, jacobian, update);
        m_performance.nb_inversions += 1;
        return ret;
    }

    NewtonTolerance* get_tolerance() {return &m_tolerance;}
    PerformanceCounter* get_performance() {return &m_performance;}


protected:

    Derived* derived() {return static_cast<Derived*>(this);}

    void common_init()
    {
        m_previous_solution = VectorT::Zero(neq);
        m_typx = VectorT::Ones(neq);
        m_typf = VectorT::Ones(neq);
        m_norm = std::bind(std::mem_fn(&NewtonDriverBase::scaled_norm_inf), this, std::placeholders::_1);
    }

    IterationReturnCode  stop_convergence(const VectorT& solution,
                                          const VectorT& residuals,
                                          double residual_tolerance);

    //-------------
    // linesearches
    //-------------
    //! Perform no solution but check that the solution does not diverge
    double no_linesearch(const VectorT&, VectorT&, VectorT& update);

    //! The objective function to minimize in the strang linesearch
    double strang_residual(const VectorT& solution, const VectorT& update, double s);
    //! The Strang linesearch
    double strang_linesearch(const VectorT& residual0, VectorT& solution, VectorT& update);

    //! Compute the initial gradient for backtracking linesearch
    double backtracking_fp0(const VectorT& residual0, VectorT& solution, VectorT& update);
    //! The backtracking linesearch
    double backtracking_linesearch(const VectorT& residual0, VectorT& solution, VectorT& update);

    // ----------
    // attributes
    // ----------

    int neq;
    NewtonTolerance m_tolerance;
    PerformanceCounter m_performance;

    VectorT m_typx;
    VectorT m_typf;

    ResidualF m_compute_residual;
    JacobianF m_compute_jacobian;
    ResidualNormF m_norm;
    LineSearchF m_linesearch;

    bool analjac;

    // check for divergence
    VectorT m_previous_solution;

    bool m_maxtaken;
    int m_consecmax;
};

// ############## Implementation ###################### //

template <class MatrixT, class Derived>
IterationReturnCode  NewtonDriverBase<MatrixT, Derived>::stop_convergence(
                                   const VectorT& solution,
                                   const VectorT& residuals,
                                   double residual_tolerance)
{
    residual_tolerance = m_tolerance.residual_tolerance;
    // Check convergence of residuals
    if (m_norm(residuals) < residual_tolerance) return IterationSuccess;

    // Check for a change in the solution
    Eigen::ArrayXXd rel_sol = (solution - m_previous_solution);
    rel_sol = rel_sol.abs() / solution.array().abs().max(m_typx.array());

    if (rel_sol.maxCoeff() <= m_tolerance.step_tolerance) {
        m_performance.message += "No change detected in the last iteration \n";
        return IterationNoChange;
    }

     // Check for divergence
    if (m_maxtaken)
    {
        m_consecmax += 1;
        if (m_consecmax == 5)
        {
            m_performance.message += "Newton length exceeded max length for 5 iterations...\n";
            return DivergenceDetected;
        }
    }

    m_previous_solution = solution;

    return NoConvergence;
}

template <class MatrixT, class Derived>
IterationReturnCode NewtonDriverBase<MatrixT, Derived>::run_quasinewton_iterations(VectorT& solution, int nstep)
{
    assert(solution.rows() == neq);
    int cnt = 0;
    IterationReturnCode retcode = NoConvergence;
    m_consecmax = 0;
    m_maxtaken = 0;

    VectorT residual(VectorT::Zero(neq));
    MatrixT jacobian(neq, neq);
    VectorT update;

    bool reform_jacobian = true;

    compute_residual(solution, residual);
    // scale tolerances
    if (m_norm(residual) <= 0.01 * m_tolerance.residual_tolerance) return IterationSuccess;
    double residual_tolerance = m_norm(residual)*m_tolerance.residual_tolerance;
    //
    m_tolerance.maxstep *= std::max(norm_2(m_typx.asDiagonal().inverse() * solution), norm_2(m_typx.array().inverse()));


    while (cnt < m_tolerance.max_iterations)
    {
        if (reform_jacobian) compute_jacobian(solution, residual, jacobian);
        int ret = solve(-residual, jacobian, update);
        if (ret > 0) {
            m_performance.message += "Problem when inverting the linear system, return code : " + std::to_string(ret) + " .\n";
            retcode = SingularMatrix; break;}
        // update
        double s = m_linesearch(residual, solution, update); //strang_linesearch(residual, solution, update);
        solution += s*update;
        // end iterations
        compute_residual(solution, residual);
        ++cnt;
        retcode = stop_convergence(solution, residual, residual_tolerance);
        if (retcode != NoConvergence) break;
        if (cnt % nstep == 0) {reform_jacobian = true;}
        else {reform_jacobian = false;}
    }
    if (cnt == m_tolerance.max_iterations) {
        m_performance.message += "Max number of iteration reached.\n";
        retcode = MaxIterations;
    }
    m_performance.nb_iterations = cnt;
    return retcode;
}

// -----------------------------------------------------------------------
// ################# Linesearches ########################################
// -----------------------------------------------------------------------

//! If we don't use a linesearch, we still have to check for convergence
template <class MatrixT, class Derived>
double NewtonDriverBase<MatrixT, Derived>::no_linesearch(const VectorT&,
                                                         VectorT&,
                                                         VectorT& update)
{
    double s = 1.0;
    double newtlen = norm_2(m_typx.asDiagonal().inverse() * update);
    if (newtlen > m_tolerance.maxstep) {
        m_maxtaken = true;
        s = (m_tolerance.maxstep / newtlen);
    }
    return s;
}

// ################# Strang linesearches ##################################


inline bool same_sign(double a, double b)
{
    return (std::copysign(1., a) * std::copysign(1., b) > 0);
}

//---------------------//
//- strang linesearch -//
//---------------------//
template <class MatrixT, class Derived>
double NewtonDriverBase<MatrixT, Derived>::strang_residual(const VectorT& solution,
                                                           const VectorT& update,
                                                           double s)
{
    VectorT residual(neq);
    compute_residual(solution + s*update, residual);
    return 0.5 * residual.dot(residual);
}

template <class MatrixT, class Derived>
double NewtonDriverBase<MatrixT, Derived>::strang_linesearch(const VectorT &residual0,
                                                             VectorT& solution,
                                                             VectorT& update)
{
    const double s_tol = 0.5;
    const int s_max = 2;
    const int lin_max = 10;

    std::function<double (double)> g = std::bind(std::mem_fn(
          &NewtonDriverBase<MatrixT, Derived>::strang_residual),
                                                this,
                                                solution,
                                                update,
                                                std::placeholders::_1);

    double g_a = g(1);
    double g_b = 0.5*residual0.dot(residual0);
    double s_a = 1.0;
    double s_b = 0;

    if (std::fabs(g_a) <= s_tol*std::fabs(g_b))
        return no_linesearch(residual0, solution, update);

    while (same_sign(g_a, g_b) and s_a < s_max)
    {
        s_b = s_a;
        s_a = 2*s_a;
        g_b = g_a;
        g_a = g(s_a);
    }

    double g_ = g_a;
    double g_0 = g_a;
    double s = s_a;

    for (int l=0; l<lin_max; ++l)
    {
        if (same_sign(g_a, g_b) and (
                    std::fabs(g_) < s_tol*std::fabs(g_0)
                    or std::fabs(s_a - s_b) < s_tol*(s_a + s_b)/2 ))
            break;

        s = s_a - g_a * ( s_a - s_b)/(g_a - g_b);
        g_ = g(s);
        if (same_sign(g_, g_a))
            g_b = g_b/2.;
        else
        {
            s_b = s_a;
            g_b = g_a;
        }
        s_a = s;
        g_a = g_;

    }
    double newtlen = norm_2(m_typx.asDiagonal().inverse() * s * update);
    if (newtlen > m_tolerance.maxstep) {
        m_maxtaken = true;
        s = (m_tolerance.maxstep / newtlen);
    }

    return s;
}

// backtracking linesearch

template <class MatrixT, class Derived>
double NewtonDriverBase<MatrixT, Derived>::backtracking_fp0(const VectorT& residual0,
                                                            VectorT& solution,
                                                            VectorT& update)
{
    double eps = std::pow(m_tolerance.machine_epsilon, 1.0/3.0);
    VectorT residualp(neq);
    VectorT residualm(neq);

    compute_residual(solution + eps*update, residualp);
    compute_residual(solution - eps*update, residualm);
    return residual0.dot((residualp - residualm)/(2*eps));
}

template <class MatrixT, class Derived>
double NewtonDriverBase<MatrixT, Derived>::backtracking_linesearch(const VectorT& residual0,
                                                                   VectorT& solution,
                                                                   VectorT& update)                                                     //linesearch_residual_t& f,
                                       //std::function<double ()>& fp0_comp)
{
    std::function<double (double)> f = std::bind(std::mem_fn(
          &NewtonDriverBase<MatrixT, Derived>::strang_residual),
                                                this,
                                                solution,
                                                update,
                                                std::placeholders::_1);
    double f0 = 0.5*residual0.dot(residual0);

    const double alphab = 1e-4;
    const double sl = 0.1;
    const double su = 0.5;
    double s = 1;
    double sp = 1;

    if (f(1) < (1 - 2*alphab) *f0) { return no_linesearch(residual0, solution, update);}

    double fp0 = backtracking_fp0(residual0, solution, update) ; // compute only if needed
    if (fp0 > 0) { return 1;} //###FIXME

    // quadratic approximation
    s = - fp0 / (2*(f(1)-f0-fp0));
    s = std::max(s,sl*sp);
    double fs = f(s);
    if (not (s> sl*sp and fs <= f0 + alphab*s*fp0))
    {
    // cubic approximation
    do {
        const double rhs0 = fs  - fp0*s  - f0;
        const double rhs1 = f(sp) - fp0*sp - f0;
        const double a = 1/(s-sp) * ( rhs0/std::pow(s, 2) - rhs1/std::pow(sp, 2));
        const double b = 1/(s-sp) * ( -sp*rhs0/std::pow(s, 2) + s*rhs1/std::pow(sp, 2));

        sp = s;
        if (a < std::sqrt(1e-16)) {s = - fp0/(2*b);}
        else (s = -b + std::sqrt(std::pow(b, 2) - 3*a*fp0));

        s = std::max(s, sl*sp);
        s = std::min(s, su*sp);
        fs = f(s);

    } while (fs > f0 + alphab*s*fp0);
    }
    double newtlen = norm_2(m_typx.asDiagonal().inverse() * s * update);
    if (newtlen > m_tolerance.maxstep) {
        m_maxtaken = true;
        s = (m_tolerance.maxstep / newtlen);
    }
    return s;
}


} // end namespace dfpmsolver

#endif //DFPM_NEWTON_H
