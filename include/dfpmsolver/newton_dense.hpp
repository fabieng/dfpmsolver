/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver/newton.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

//! \file newton_dense.hpp Driver for Newton, Quasi-newton method for dense matrix

#include "newton.hpp"

namespace dfpmsolver {

using DenseMatrixT = Eigen::MatrixXd;

class NewtonDriver: public NewtonDriverBase<DenseMatrixT, NewtonDriver>
{
public:
    NewtonDriver(int nb_equations,
                     ResidualF f_residuals,
                     JacobianF f_jacobian):
        NewtonDriverBase(nb_equations, f_residuals, f_jacobian)
    {}

    NewtonDriver(int nb_equations,
                     ResidualF f_residuals):
        NewtonDriverBase(nb_equations, f_residuals)
    {}

    int solve_linear_system(const VectorT& residual,
                            DenseMatrixT& jacobian,
                            VectorT& update);

    void finite_difference_jacobian(VectorT &solution,
                                    VectorT &residual,
                                    DenseMatrixT& jacobian);

};

} // end namespace dfpm
