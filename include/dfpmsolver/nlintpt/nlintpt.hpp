/*-------------------------------------------------------

 - Module : nlintpt
 - File : nlintpt.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_NLINTPT_HPP
#define DFPM_NLINTPT_HPP

//! \file nlintpt.hpp description

#include <memory>
#include <functional>
#include "nlprog.hpp"

namespace dfpmsolver {
namespace nlintpt {

//! Options to customize the behavior of the NonLinearIntPoint class
struct NonLinearIntPointOptions
{
    double init_slack; //!< Value used to initialize the slack variables
    double tol_norm;   //!< Tolerance to decide of optimality
    double threshold_infeasible; //!< Threshold to decide of infeasibility
    double delta;      //!< Multiplication factor to update mu
    double underrelax_factor; //!< Underrelaxation factor to ensure positivity
    int max_iter;             //!< Maximum number of iterations
    double maxstepx;

    NonLinearIntPointOptions():
        init_slack(1.0),
        tol_norm(1e-8),
        threshold_infeasible(1e19),
        delta(0.1),
        underrelax_factor(0.95),
        max_iter(500),
        maxstepx(5)
    {}
};

struct NonLinearIntPointPerformance
{
    double nb_iterations;
    double nb_factorisation;
};

//! Return code for the NL interior point algorithm
enum NLIntPtReturnCode
{
    NegativeVariableThatIsNotSupposedToBe = -5,
    FatalError          = -4,
    MaxIterationReached = -3,
    DualInfeasible      = -2,
    PrimalInfeasible    = -1,
    NotConvergedYet     = 0,
    Optimal             = 1
};


template <class Program, typename ScalarT>
class NonLinearIntPoint
{
public:
    using traits = NonLinearProgramTraits<ScalarT>;
    using program_ptr = std::shared_ptr<Program>;
    using solver_t = Eigen::SimplicialLDLT<typename traits::spmatrix_t>;                           // the solver
    using selfadjoint_t = Eigen::SparseSelfAdjointView<typename traits::spmatrix_t, Eigen::Lower>; // the matrix is self adjoint

    NonLinearIntPoint(program_ptr program):
        m_program(program)
    {}


    // Easy access
    // -----------

    //! Return number of variables of the problem
    int get_nb_variables() {return m_program->get_number_variables();}
    //! Return the number of constraints of the problem
    int get_nb_constraints() {return m_program->get_number_constraints();}

    //! Read-Write access to the options
    NonLinearIntPointOptions& get_options() {return m_options;}

    // Algorithm
    // ----------

    //! Optimize using starting point x
    NLIntPtReturnCode optimize(typename traits::vector_t& x);

    //! Initialize the problem
    void initialization(typename traits::vector_t& x);
    //! Check if the algorithm has converged
    NLIntPtReturnCode check_convergence(typename traits::vector_t& x);
    //! Choose a barrier parameter
    void choose_mu();
    //! solve KKT system
    void solve_KKT(typename traits::vector_t& x);

    NonLinearIntPointPerformance get_performance() const {return m_perf;}
    NonLinearIntPointPerformance& get_performance() {return m_perf;}

    int linesearch(typename traits::vector_t& dx,
               typename traits::vector_t& x,
               double& lambda);

private:
    // Private methods
    // ###############
    ScalarT has_d_correct_signs(const typename traits::vector_t& d);
    void search_for_jamming(const typename traits::vector_t& u,
                            const typename traits::vector_t& du,
                            int &k,
                            double &d);
    void correct_jamming(const typename traits::vector_t& u,
                         typename traits::vector_t& du,
                         double d);
    ScalarT barrier_function(const typename traits::vector_t& x);
    // Matrix stuff
    // ------------
    //! build the KKT matrix to solve
    void build_skeleton_KKT_matrix(const typename traits::vector_t &x);
    //! perturb the upper diagonal of the KKT matrix
    void perturb_KKT_matrix(typename traits::vector_t& d_factorization);


    // Attributes
    // ##########

    // The program
    // -----------

    program_ptr m_program;

    // Options
    // -------

    NonLinearIntPointOptions m_options;

    // Matrix stuff
    // ------------

    typename traits::spmatrix_t m_matrix;
    solver_t m_solver;
    bool first_iter;

    // Barrier parameter
    // -----------------

    ScalarT m_mu;

    // Slack variables
    // ---------------

    // primal
    typename traits::vector_t sv_w;
    typename traits::vector_t sv_p;
    typename traits::vector_t sv_g;
    typename traits::vector_t sv_t;
    // dual
    typename traits::vector_t sv_y;
    typename traits::vector_t sv_v;
    typename traits::vector_t sv_q;
    typename traits::vector_t sv_h;
    typename traits::vector_t sv_s;

    // Feasibilities
    // -------------

    typename traits::vector_t m_rho;
    typename traits::vector_t m_sigma;
    ScalarT m_norm_rho;
    ScalarT m_norm_sigma;
    ScalarT m_gamma;

    // Check performance
    // -----------------

    NonLinearIntPointPerformance m_perf;


};

} // end namespace nlintpt
} // end namespace dfpmsolver

// implementation
// --------------
#include "nlintpt.inl"


#endif // DFPM_NLINTPT_HPP
