/*-------------------------------------------------------

 - Module : nlinpt
 - File : nlintpt.inl
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include "nlintpt.hpp" // for syntaxic coloration only, should be invisible to real compiler
#include <exception>
#include <iostream>

namespace dfpmsolver {
namespace nlintpt {

template <class Program, typename ScalarT>
void NonLinearIntPoint<Program, ScalarT>::initialization(typename traits::vector_t& x)
{
    const int n = get_nb_variables();
    const int m = get_nb_constraints();

    // the matrix
    m_matrix = typename traits::spmatrix_t(n+m,n+m);
    // primal slack variables
    //sv_g = traits::vector_t::Constant(n, get_options().init_slack);
    sv_g = (m_program->lower_bounds_variables()-x).cwiseMax(traits::vector_t::Constant(n,get_options().init_slack));
    //sv_t = traits::vector_t::Constant(n, get_options().init_slack);
    sv_t = (x-m_program->upper_bounds_variables()).cwiseMax(traits::vector_t::Constant(n,get_options().init_slack));
    //sv_w = traits::vector_t::Constant(m, get_options().init_slack);
    sv_w = (m_program->constraints(x)
            - m_program->lower_bounds_constraints()).cwiseMax(traits::vector_t::Constant(m,get_options().init_slack));
    //sv_p = traits::vector_t::Constant(m, get_options().init_slack);
    sv_p = (m_program->upper_bounds_constraints() - sv_w).cwiseMax(traits::vector_t::Constant(m,get_options().init_slack));
    // dual slack variables
    sv_y = traits::vector_t::Constant(m, get_options().init_slack);
    sv_h = traits::vector_t::Constant(n, get_options().init_slack);
    sv_s = traits::vector_t::Constant(n, get_options().init_slack);
    sv_v = traits::vector_t::Constant(m, get_options().init_slack);
    sv_q = traits::vector_t::Constant(m, get_options().init_slack);
    // infeasibilities
    m_rho = typename traits::vector_t(m);
    m_sigma = typename traits::vector_t(n);

}


template <class Program, typename ScalarT>
NLIntPtReturnCode NonLinearIntPoint<Program, ScalarT>::optimize(typename traits::vector_t& x)
{
    initialization(x);
    int cnt = 0;
    NLIntPtReturnCode retcode = NotConvergedYet;
    first_iter = true;
    while (cnt < get_options().max_iter)
    {
        ++cnt;
        //std::cout << "Iteration : " << cnt << std::endl;
        retcode = check_convergence(x);

        if (m_gamma < 0) // Check for a bad update
        {
            retcode = NegativeVariableThatIsNotSupposedToBe;
            break;
        }
        if (retcode != NotConvergedYet)
        {
            break;
        }
        choose_mu();
        solve_KKT(x);
        m_program->hook_end_iter(x);

        //std::cout << "sv_y : \n" << sv_y << std::endl;
        //std::cout << "concentration : \n" << x.block(0,0,7,1) << std::endl;
//                  << "Ionic Strength : " << x(7) << std::endl
//                  << "log gamma : \n " << x.block(8,0,7,1) << std::endl;
        //if (x(7) < 0) x(7) = 1e-4;

    }
    if (cnt == get_options().max_iter)
    {
        retcode = MaxIterationReached;
    }
    get_performance().nb_iterations = cnt;

    return retcode;
}

template <class Program, typename ScalarT>
NLIntPtReturnCode NonLinearIntPoint<Program, ScalarT>::check_convergence(typename traits::vector_t& x)
{
    build_skeleton_KKT_matrix(x);
    m_rho.noalias() = m_program->lower_bounds_constraints()  + sv_w - m_program->constraints(x);
    m_sigma.noalias() = m_program->gradient_objective_function(x) + sv_s - sv_h;
    m_sigma.noalias() -= m_matrix.block(get_nb_variables(), 0, get_nb_constraints(), get_nb_variables()).transpose()*sv_y;

    m_gamma = sv_w.dot(sv_v) + sv_p.dot(sv_q) + sv_t.dot(sv_s) + sv_g.dot(sv_h);
    m_norm_rho = m_rho.norm();
    m_norm_sigma = m_sigma.norm();

    //std::cout << "norm : " << m_norm_rho << " - " << m_norm_sigma << " - " << m_gamma << std::endl;

    if (   ( m_norm_rho < get_options().tol_norm )
       and ( m_norm_sigma < get_options().tol_norm )
       and ( m_gamma < get_options().tol_norm )
       )
    {
        return Optimal;
    }
    else if (m_norm_rho > get_options().threshold_infeasible)
    {
        return PrimalInfeasible;
    }
    else if (m_norm_sigma > get_options().threshold_infeasible)
    {
        return DualInfeasible;
    }
    return NotConvergedYet;
}


template <class Program, typename ScalarT>
void NonLinearIntPoint<Program, ScalarT>::choose_mu()
{
    const double m = get_nb_constraints();
    const double n = get_nb_variables();
    double xi = m*(sv_w.cwiseProduct(sv_v).minCoeff())/sv_w.dot(sv_v);
    xi = std::min(m*(sv_p.cwiseProduct(sv_q).minCoeff())/sv_p.dot(sv_q),xi);
    xi = std::min(n*(sv_g.cwiseProduct(sv_h).minCoeff())/sv_g.dot(sv_h),xi);
    xi = std::min(n*(sv_s.cwiseProduct(sv_t).minCoeff())/sv_t.dot(sv_s),xi);
    xi = std::min((1.0-get_options().underrelax_factor)*((1-xi)/xi), 2.0);
    m_mu = get_options().delta * std::pow(xi,3.0)*m_gamma/(2*m+2*n);
//    m_mu = std::abs(m_mu);
    //m_mu = get_options().delta * m_gamma/(2*m+2*n);
    //std::cout << "mu : " << m_mu << std::endl;
}

template <class Program, typename ScalarT>
void NonLinearIntPoint<Program, ScalarT>::search_for_jamming(const typename traits::vector_t& u,
                                                             const typename traits::vector_t& du,
                                                             int &k,
                                                             double &d)
{
    for (int i=0;i<u.cols();++i)
    {
        if ((u(i) < 1e-5) and (std::abs(du(i))/u(i) > 1e5))
        {
            ++k;
            d = d + 1/u(i);
        }
    }
}

template <class Program, typename ScalarT>
void NonLinearIntPoint<Program, ScalarT>::correct_jamming(const typename traits::vector_t &u,
                                                          typename traits::vector_t &du,
                                                          double d)
{
    for (int i=0;i<u.cols();++i)
    {
        if ((u(i) < 1e-5) and (std::abs(du(i))/u(i) > 1e5))
        {
            du.coeffRef(i) += d;
        }
    }
}

template <class Program, typename ScalarT>
void
NonLinearIntPoint<Program, ScalarT>::solve_KKT(typename traits::vector_t &x)
{
    // Temp vector
    // -----------
    const auto gamma_w = m_mu*sv_v.cwiseInverse() - sv_w;
    const auto gamma_q = m_mu*sv_p.cwiseInverse() - sv_q;
    const auto gamma_h = m_mu*sv_g.cwiseInverse() - sv_h;
    const auto gamma_s = m_mu*sv_t.cwiseInverse() - sv_s;

    typename traits::vector_t alpha = m_program->upper_bounds_constraints() - sv_w - sv_p;
    alpha.noalias() -= (sv_p.array()/sv_q.array()).matrix().asDiagonal()*gamma_q;
    typename traits::vector_t beta = sv_y + sv_q - sv_v;
    beta.noalias() -= (sv_v.array()/sv_w.array()).matrix().asDiagonal()*gamma_w;
    typename traits::vector_t nu = m_program->lower_bounds_variables() -x + sv_g;
    nu.noalias() +=  (sv_g.array()/sv_h.array()).matrix().asDiagonal()*gamma_h;
    typename traits::vector_t tau =  m_program->upper_bounds_variables() - x  -sv_t;
    tau.noalias() -= (sv_t.array()/sv_s.array()).matrix().asDiagonal()*gamma_s;

    const typename traits::vector_t f =  (sv_s.array()/sv_t.array()).matrix() + (sv_h.array()/sv_g.array()).matrix();
    const typename traits::vector_t e =  (sv_v.array()/sv_w.array()).matrix() + (sv_q.array()/sv_p.array()).matrix();

    //std::cout << " e : \n-----------\n" << e << "\n -------- \n";

    // Add E and F in KKT
    // ------------------
    for (int i=0; i<get_nb_variables(); ++i)
    {
        m_matrix.coeffRef(i,i) += -f(i);
    }
    for (int i=0; i<get_nb_constraints(); ++i)
    {
        double tmp = 1/e(i);
        if (tmp < 1e-12) tmp = 1e-12;
        m_matrix.coeffRef(get_nb_variables()+i,get_nb_variables()+i) = tmp;
    }

    // Perturbation for positive definiteness
    // --------------------------------------
    double lambda = 0;
    bool trial = false;

    int cnt = 0;
    while (cnt < 100)
    {
        ++cnt;
        if (first_iter)
        {
            m_solver.analyzePattern(selfadjoint_t(m_matrix));
            first_iter = false;
        }

        if (lambda > 0)
        {
            for (int i=0; i<get_nb_variables(); ++i)
            {
                m_matrix.coeffRef(i,i) += -lambda;
            }
        }
        m_solver.factorize(selfadjoint_t(m_matrix));
        get_performance().nb_factorisation += 1;
        double lambda_0 = has_d_correct_signs(m_solver.permutationPinv() * m_solver.vectorD());
        if (lambda_0 == 0 and trial == false)
        {
            if (lambda > 0.0)
            {
                break;
                //lambda = - std::abs(lambda) / 2;
                //trial = true;
            }
            else {
                break;
            }
        }
        else
        {
            if (trial == true)
            {
                lambda += std::abs(lambda);
                trial = false;
            }
            else lambda = 1.2*lambda_0;
        }
    }
    //std::cout << "Lambda : " << lambda << std::endl;
    //std::cout << "P^{-1}*D" << "\n---------\n" << m_solver.permutationPinv() * m_solver.vectorD() << "\n----------\n";
    // assemble RHS
    // ------------
    typename traits::vector_t rhs(get_nb_variables()+get_nb_constraints());
    rhs.block(0,0,get_nb_variables(),1) = ( m_sigma
                                            - (sv_s.array()/sv_t.array()).matrix().cwiseProduct(tau)
                                            - (sv_h.array()/sv_g.array()).matrix().cwiseProduct(nu)
                                            );
    rhs.block(get_nb_variables(),0,get_nb_constraints(),1) = (m_rho  - e.asDiagonal().inverse()*(
                                    beta -(sv_q.array()/sv_p.array()).matrix().cwiseProduct(alpha)));
    // Solve the system
    // -----------------
    const typename traits::vector_t solution = m_solver.solve(rhs);
    if (m_solver.info() != Eigen::Success)
    {
       throw std::runtime_error("planquez vous");
    }


    // Updates
    // -------

    typename traits::vector_t dx = solution.block(0,0,get_nb_variables(),1);
    const auto dy = solution.block(get_nb_variables(),0,get_nb_constraints(),1);
    typename traits::vector_t dw = e.asDiagonal().inverse()*(
                - dy - beta + (sv_q.array()/sv_p.array()).matrix().cwiseProduct(alpha));
    typename traits::vector_t dq = (sv_q.array()/sv_p.array()).matrix().cwiseProduct(dw - alpha);
    typename traits::vector_t dh = (sv_h.array()/sv_g.array()).matrix().cwiseProduct(nu - dx);
    typename traits::vector_t ds = (sv_s.array()/sv_t.array()).matrix().cwiseProduct(dx - tau);
    typename traits::vector_t dv = (sv_v.array()/sv_w.array()).matrix().cwiseProduct(gamma_w - dw);
    typename traits::vector_t dt = (sv_t.array()/sv_s.array()).matrix().cwiseProduct(gamma_s - ds);
    typename traits::vector_t dg = (sv_g.array()/sv_h.array()).matrix().cwiseProduct(gamma_h - dh);
    typename traits::vector_t dp = (sv_p.array()/sv_q.array()).matrix().cwiseProduct(gamma_q - dq);

    // Anti jamming
    // ------------
    //bool jamming = false;
    double d = 0;
    int k = 0;

    //search_for_jamming(sv_y, dy, k, d);
    search_for_jamming(sv_w, dw, k, d);
    search_for_jamming(sv_v, dv, k, d);
    search_for_jamming(sv_p, dp, k, d);
    search_for_jamming(sv_q, dq, k, d);
    search_for_jamming(sv_t, dt, k, d);
    search_for_jamming(sv_s, ds, k, d);
    search_for_jamming(sv_g, dg, k, d);
    search_for_jamming(sv_h, dh, k, d);
    if (k > 0)
    {
        d = k/d;
        //d = std::max(k/d, 0.1);
        //std::cout << "JAAAAAAAAAAAMMMMMMIIIIIIIIIIIIIIIIIIINNNNNNNNNNNNGGGG : " << d << std::endl;
        //correct_jamming(sv_y, dy, d);
        correct_jamming(sv_w, dw, d);
        correct_jamming(sv_v, dv, d);
        correct_jamming(sv_p, dp, d);
        correct_jamming(sv_q, dq, d);
        correct_jamming(sv_t, dt, d);
        correct_jamming(sv_s, ds, d);
        correct_jamming(sv_g, dg, d);
        correct_jamming(sv_h, dh, d);
    }

    // linesearch
    // ----------
    double theta = std::max({
                      (-dw.array()/sv_w.array()).maxCoeff(),
                      (-dp.array()/sv_p.array()).maxCoeff(),
                      (-dt.array()/sv_t.array()).maxCoeff(),
                      (-dg.array()/sv_g.array()).maxCoeff(),
                      (-dv.array()/sv_v.array()).maxCoeff(),
                      (-dq.array()/sv_q.array()).maxCoeff(),
                      (-ds.array()/sv_s.array()).maxCoeff(),
                      (-dh.array()/sv_h.array()).maxCoeff(),
             });
    theta = std::min(get_options().underrelax_factor/theta, 1.0);

    linesearch(dx, x, theta);
    sv_y += theta*dy;
    sv_w += theta*dw;
    sv_v += theta*dv;
    sv_p += theta*dp;
    sv_q += theta*dq;
    sv_t += theta*dt;
    sv_s += theta*ds;
    sv_h += theta*dh;
    sv_g += theta*dg;
    //std::cout << "theta : " << theta << std::endl;
//    m_rho.noalias() = m_program->lower_bounds_constraints()  + sv_w - m_program->constraints(x);
//    m_sigma.noalias() = m_program->gradient_objective_function(x) + sv_s - sv_h;
//    m_sigma.noalias() -= m_matrix.block(get_nb_variables(), 0, get_nb_constraints(), get_nb_variables()).transpose()*sv_y;

//    x += theta*dx;
//    sv_y += theta*dy;
//    sv_w += theta*dw;
//    sv_v += theta*dv;
//    sv_p += theta*dp;
//    sv_q += theta*dq;
//    sv_t += theta*dt;
//    sv_s += theta*ds;
//    sv_h += theta*dh;
//    sv_g += theta*dg;

//    // no linesearch if jamming had been detected
//    if (k > 0)
//    {

//        double phi_0 = barrier_function(x);
//        double der_phi = (m_mu*(sv_w.cwiseInverse().dot(dw)
//                                + sv_p.cwiseInverse().dot(dp) + sv_g.cwiseInverse().dot(dg) + sv_t.cwiseInverse().dot(dt))
//                          + m_program->gradient_objective_function(x).dot(dx));
//        double rho_0 = m_rho.norm();
//        double der_rho = (dw - m_matrix.block(get_nb_variables(), 0, get_nb_constraints(), get_nb_variables())*dx).norm();

//        //double sigma_0 = m_sigma(0);
//        //double der_sigma = (- m_matrix.block(get_nb_variables(), 0, get_nb_constraints()
//        //                                     , get_nb_variables()).transpose()*dy + ds -dh).norm();
//        //while (theta/dx.norm() > 1e-2)
//        while (theta > 1e-5)
//        {
//            //ScalarT new_sigma = (m_program->gradient_objective_function(x) + sv_s - sv_h
//            //                     - m_matrix.block(get_nb_variables(), 0, get_nb_constraints(),
//            //                                      get_nb_variables()).transpose()*sv_y).norm();
//            if ((barrier_function(x) <= phi_0 + 1e-4*theta*der_phi)
//                    or ( (m_program->lower_bounds_constraints()
//                          + sv_w - m_program->constraints(x)).norm() <= rho_0 + 1e-4*theta*der_rho)
//                    //or ( new_sigma <= sigma_0 +1e-4*theta*der_sigma)
//                    )
//            {
//                break;
//            }
//            theta = theta * 0.75;
//            x -= theta*dx;
//            sv_y -= theta*dy;
//            sv_w -= theta*dw;
//            sv_v -= theta*dv;
//            sv_p -= theta*dp;
//            sv_q -= theta*dq;
//            sv_t -= theta*dt;
//            sv_s -= theta*ds;
//            sv_h -= theta*dh;
//            sv_g -= theta*dg;
//        }
//    }
    //std::cout << "theta " << theta << std::endl;
    //std::cout << "dx : \n--------------"  << std::endl << dx << "\n--------------" << std::endl;
    //std::cout << "dy : \n--------------"  << std::endl << dy << "\n--------------" << std::endl;
}


template <class Program, typename ScalarT>
void NonLinearIntPoint<Program, ScalarT>::build_skeleton_KKT_matrix(const typename traits::vector_t& x)
{
    typename traits::list_triplets_t triplet_list;
    m_program->hessian_mod_objective_function(x, triplet_list, 0, 0);
    m_program->hessian_mod_constraints(x, sv_y, triplet_list, 0, 0);
    m_program->jacobian_constraints(x, triplet_list, get_nb_variables(), 0);
    // add a full diagonal // E,F and lambda I will be added later
    triplet_list.reserve(triplet_list.size() + get_nb_constraints());
    for (int i=0; i<get_nb_constraints() + get_nb_variables(); ++i)
    {
        triplet_list.push_back(typename traits::triplet_t(i,i,0));
    }
    // build the matrix
    m_matrix.setFromTriplets(triplet_list.begin(), triplet_list.end());
}

template <class Program, typename ScalarT>
ScalarT NonLinearIntPoint<Program, ScalarT>::has_d_correct_signs(const typename traits::vector_t& d)
{
    double lambda_0 = 0;
    for (int i=0; i<get_nb_variables(); ++i)
    {
        if (d.coeff(i) > 0) lambda_0 = std::max(lambda_0, d.coeff(i));
        else if (d.coeff(i) == 0.0) lambda_0 = std::max(lambda_0,
                                            1e-8);
    }
//    for (int i=get_nb_variables(); i<get_nb_constraints()+get_nb_variables(); ++i)
//    {
//        if (d.coeff(i) < 0) lambda_0 = std::max(lambda_0, -d.coeff(i));
//        else if (d.coeff(i) == 0.0) lambda_0 = std::max(lambda_0,
//                1e-10);
//    }
    return lambda_0;
}


template <class Program, typename ScalarT>
ScalarT NonLinearIntPoint<Program, ScalarT>::barrier_function(const typename traits::vector_t& x)
{
    double phi = 0;
    for (int i=0; i<get_nb_constraints(); ++i)
    {
        phi += std::log(sv_w(i)) + std::log(sv_p(i));
    }
    for (int i=0; i<get_nb_variables(); ++i)
    {
        phi += std::log(sv_g(i)) + std::log(sv_t(i));
    }
    phi = m_program->objective_function(x) - m_mu*phi;
    return phi;
}




template <class Program, typename ScalarT>
int NonLinearIntPoint<Program, ScalarT>::linesearch(typename traits::vector_t& dx,
                                                   typename traits::vector_t& x,
                                                   double& lambda)
{

    const int n = get_nb_variables();
    const int me = get_nb_constraints();

    int retcode = 2;
    const double alpha = 1e-4;
//    double newtlen = dx.norm();
//    if (newtlen > get_options().maxstepx )
//    {
//        dx = dx*get_options().maxstepx/newtlen;
//        newtlen = get_options().maxstepx;
//    }
    typename traits::vector_t gradient = -m_matrix.block(n, 0, me, n)*m_rho;
    double init_slope = gradient.dot(dx);
    double rellength = (dx.array().abs()/x.array()).maxCoeff();
    double minlambda = get_options().tol_norm / rellength;
    double lambda_prev = 1.0;

    typename traits::vector_t xp(me);
    typename traits::vector_t rhop(me);
    double fc = m_norm_rho;
    double fcp;
    double fcp_prev = fc;
    do
    {
        xp = x + lambda*dx;
        rhop = m_program->constraints(xp);
        fcp = rhop.norm();
        if (fcp <= fc + alpha*lambda*init_slope)
        {
            retcode = 0;
            //if (lambda ==1 and (newtlen > 0.99 * get_options().maxstepx)) {
            //    //m_max_taken = true;
           // }
            break;
        }
        else if (lambda < minlambda)
        {
            retcode = 1;
            break;
        }
        else
        {
            double lambdatmp;
            if (lambda == 1.0) {
                lambdatmp = - init_slope / (2*(fcp - fc -init_slope));
            }
            else
            {
                const double factor = 1 /(lambda - lambda_prev);
                const double x1 = fcp - fc - lambda*init_slope;
                const double x2 = fcp_prev - fc - lambda_prev*init_slope;

                const double a = factor * ( x1/(lambda*lambda) - x2/(lambda_prev*lambda_prev));
                const double b = factor * ( -x1*lambda_prev/(lambda*lambda) + x2*lambda/(lambda_prev*lambda_prev));

                if (a == 0)
                { // cubic is quadratic
                    lambdatmp = - init_slope/(2*b);
                }
                else
                {
                    const double disc = b*b-3*a*init_slope;
                    lambdatmp = (-b+std::sqrt(disc))/(3*a);
                }
                if (lambdatmp > 0.5 ) lambdatmp = 0.5*lambda;
            }
            lambda_prev = lambda;
            fcp_prev = fcp;
            if (lambdatmp < 0.1*lambda) {
                lambda = 0.1 * lambda;
            } else {
                lambda = lambdatmp;
            }
        }
    } while(retcode < 2);
    x = xp;
    dx = lambda*dx;
    return lambda;

}

} // end namespace nlintpt
} // end namespace dfpmsolver
