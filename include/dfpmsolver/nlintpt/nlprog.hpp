/*-------------------------------------------------------

 - Module : nlintpt
 - File : nlprog.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_NLPROG_HPP
#define DFPM_NLPROG_HPP

//! \file nlprog.hpp Static interface for a nonlinear program

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <vector>

namespace dfpmsolver {
namespace nlintpt {


//! \brief Define common types
template <typename ScalarT>
struct NonLinearProgramTraits
{
    using vector_t = Eigen::Matrix<ScalarT,Eigen::Dynamic,1>;
    using matrix_t = Eigen::Matrix<ScalarT,Eigen::Dynamic,Eigen::Dynamic>;
    using spmatrix_t = Eigen::SparseMatrix<ScalarT>;
    using triplet_t = Eigen::Triplet<ScalarT>;
    using list_triplets_t = std::vector<triplet_t>;
    using index_t = typename spmatrix_t::Index;
};


//! \brief Static interface of a non linear program
//!
//! To be inheritated from using CRTP
template <class Derived, class ScalarT>
class NonLinearProgram
{
public:
    using traits = NonLinearProgramTraits<ScalarT>;
    NonLinearProgram() {}

    //! Return the derived class
    Derived* derived() {
        return static_cast<Derived*>(this);
    }

    // Return the number of variables
    int get_number_variables() {return 0;}
    // Return the number of constraints
    int get_number_constraints() {return 0;}

    //! Return objective function value
    ScalarT objective_function(const typename traits::vector_t& x) {}
    //! Return gradient of objective function
    typename traits::vector_t gradient_objective_function(const typename traits::vector_t& x) {}
    //! Return minus hessian of objective function
    void hessian_mod_objective_function(const typename traits::vector_t& x,
                                        typename traits::list_triplets_t& triplet_list,
                                        const typename traits::index_t offset_row,
                                        const typename traits::index_t offset_col) {}


    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& lower_bounds_variables() {}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& upper_bounds_variables() {}

    //! Return value constraints
    typename traits::vector_t constraints(typename traits::vector_t& x) {}
    //! Return Jacobian constraints
    void jacobian_constraints(const typename traits::vector_t& x,
                              typename traits::list_triplets_t& triplet_list,
                              const typename traits::index_t offset_row,
                              const typename traits::index_t offset_col) {}
    //! Return sum of - y times hessian_i
    void hessian_mod_constraints(const typename traits::vector_t& x,
                                 const typename traits::vector_t& y,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col) {}


    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& lower_bounds_constraints() {}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& upper_bounds_constraints() {}

    void hook_end_iter(typename traits::vector_t& x) {}
};

} // end namespace nlintpt
} // end namespace dfpmsolver


#endif // DFPM_NLPROG_HPP
