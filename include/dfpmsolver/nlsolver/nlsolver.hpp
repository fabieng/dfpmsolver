/*-------------------------------------------------------

 - Module : nlsolver
 - File : nlsolver.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_NLSOLVER_NLSOLVER_HPP
#define DFPM_NLSOLVER_NLSOLVER_HPP

//! \file nlsolver.hpp Newton method for non linear system

#include <memory>
#include "nlsystem.hpp"

namespace dfpmsolver {
namespace nlsolver {

//  Types
// =======

//! \Brief Define the solver to use
//!
//! Two types must be define:
//!     - qr_solver_t : QR decomposition
//!     - ldlt_solver_t : LDL^T decomposition
//!
//! Both must be Eigen solver.
//!
template <typename ScalarT, bool isSparse>
struct NLSolverTypes
{
};

//! The solver for a sparse problem
template <typename ScalarT>
struct NLSolverTypes<ScalarT, true>
{
    using qr_solver_t =  Eigen::SparseQR<typename NLSystemTypes<ScalarT,true>::matrix_t, Eigen::COLAMDOrdering<int>>;
    using ldlt_solver_t = Eigen::SimplicialLDLT<typename NLSystemTypes<ScalarT,true>::matrix_t>;
};

//! The solver for a dense problem
template <typename ScalarT>
struct NLSolverTypes<ScalarT, false>
{
    using qr_solver_t = Eigen::ColPivHouseholderQR<typename NLSystemTypes<ScalarT, false>::matrix_t>;
    using ldlt_solver_t = Eigen::LDLT<typename NLSystemTypes<ScalarT, false>::matrix_t>;
};

//  Options
// =========

struct NLSolverOptions
{
    double eta;     // eta = 10^(-DIGITS) : Digits number of reliable digits
    double maxstep; // Max step
    double maxiter_maxstep; // number of max iterations at max step
    double steptol; // Tolerance for newton step
    double fvectol; // Tolerance for the residual
    int max_iter;   // maximum number of iterations

    NLSolverOptions():
        eta(1e-16),
        maxstep(1e3),
        maxiter_maxstep(5),
        steptol(1e-8),
        fvectol(1e-8),
        max_iter(200)
    {}
};

//  Performance
// =============


//! Used to check the performance of the driver
struct PerformanceCounter
{
    PerformanceCounter():
        nb_call_residual(0),
        nb_call_jacobian(0),
        nb_inversions(0),
        nb_iterations(0)
    {}

    int nb_call_residual;
    int nb_call_jacobian;
    int nb_inversions;
    int nb_iterations;
};

// Return Code
// ===========

//! The return codes
enum class NLSolverReturnCode
{
    FailedGlobalMethod = -3,        //!< For linesearch, indicates no big enough step was found
    MaximumNewtonLength = -2,       //!< The maximum step allowed was taken (n) successive steps
    MaximumNumberOfIterations = -1, //!< Solver has reached the maximum allowed number of iterations
    NoConvergenceYet = 0,           //!< Let's do it again
    NewtonSuccess = 1,              //!< Yeah success !! It can be for two reason :
    ResidualMinimized = 2,          //!< The residual has been minimized (what we want most of the time)
    ErrorMinimized = 3              //!< The error was minimized, may indicate the the algorithm is stuck...

};

//! Return code used by the solvers
enum class SolverReturnCode
{
    Success,
    InitialisationFailure,
    FactorisationFailure,
    SolveFailure
};


// Global Method
// =============

enum class GlobalMethod {
    Backtracking
                        };

namespace internal {

template <class SolverT, GlobalMethod global_t>
struct GlobalMethodDriver
{
    static int run(SolverT& solver,
                   typename SolverT::types::vector_t& solution,
                   typename SolverT::types::vector_t& update,
                   typename SolverT::types::scalar_t fc);
};


// Solver Interface
// ----------------

template <bool isSparse>
struct SolverInterface
{
    template <class SolverT>
    static SolverReturnCode initialize(SolverT& solver, typename SolverT::MatrixType& matrix) {
        return SolverReturnCode::Success;}

    template <class SolverT>
    static SolverReturnCode factorize(SolverT& solver, typename SolverT::MatrixType& matrix);

    template <class SolverT, class Rhs, class Sol>
    static SolverReturnCode solve(SolverT& solver,
                                  const typename Eigen::MatrixBase<Rhs>& rhs,
                                  typename Eigen::MatrixBase<Sol>& solution)
    {
        return solve(solver, rhs, solution);
    }

    template <class SolverT, class Rhs, class Sol>
    static SolverReturnCode solve(SolverT& solver,
                                  typename Eigen::MatrixBase<Rhs>& rhs,
                                  typename Eigen::MatrixBase<Sol>& solution);

    template <class SolverT, class Rhs>
    static SolverReturnCode solve(SolverT& solver, typename Eigen::MatrixBase<Rhs>& rhs);
};


} // end namespace internal;

// ============//
//  The Solver //
// ============//

//! Newton solver for nonlinear system of equations
//!
//! References :
//!     - Numerical methods for unconstrained optimization and nonlinear equations
//!         J. E. Dennis Jr
//!         Robert B. Schnabel
//!      Classics in Applied Mathematics - SIAM 1996
//!     - Newton methods for Nonlinear problems -
//!         Affine Invariance and adaptive algorithms
//!         Peter Deuflhard -  Springer - Springer Series in Computational Mathematics 2005
//!
template <typename System, typename ScalarT, bool isSparse>
class NLSolver
{
public:
    using SystemPtr = std::shared_ptr<System>;
    using types = typename System::types;
    using solver_types = NLSolverTypes<ScalarT, isSparse>;

    NLSolver(SystemPtr system_ptr):
        m_system(system_ptr)
    {}

    const NLSolverOptions& get_options() const {return m_options;}
    NLSolverOptions& get_options() {return m_options;}

    SystemPtr get_system() {return m_system;}

    // Algorithm
    // =========

    //! driver
    template <GlobalMethod global_t>
    NLSolverReturnCode solve(typename types::vector_t& solution);

    NLSolverReturnCode newton_model(typename types::vector_t& solution);

    //template <GlobalMethod global_t>
    //struct global_method: internal::GlobalMethodImpl<ScalarT, global_t>
    //{
    //};

    //! Check for convergence
    NLSolverReturnCode check_convergence(int nb_iteration,
                                        int global_retcode,
                                        typename types::vector_t& update,
                                        typename types::vector_t& solution);

    //! Return number of equation
    int get_neq() {return m_system->get_number_variables();}

    // Norms
    // =====

    //! Return the l-p norm of the residuals
    template <int p>
    ScalarT norm_residual() const {
        return ((m_system->scaling_f()).asDiagonal() * m_residuals).template lpNorm<p>();
    }
    //! Return the l-p norm of the update
    template <int p>
    ScalarT norm_update(const typename types::vector_t& update,
                        const typename types::vector_t& solution) const {
        return (update.array().abs()/(solution.array().max(m_system->scaling_x().array()))
                ).matrix().template lpNorm<p>();
    }
    //! Return the l-p norm of the solution
    template <int p>
    ScalarT norm_solution(const typename types::vector_t& solution) const {
        return (m_system->scaling_x().asDiagonal() * solution).template lpNorm<p>();
    }
    //! Return matrix norm (max of column-vectors norm)
    template <int p, class Derived>
    static ScalarT norm_matrix(const Eigen::MatrixBase<Derived> & m) {
        ScalarT norm = m.col(0).template lpNorm<p>();
        for (int i=1; i<m.cols(); ++i)
        {
            norm = std::max(norm, m.col(i).template lpNorm<p>());
        }
        return norm;
    }

    template <int p, class Derived>
    static ScalarT norm_matrix(const Eigen::SparseMatrixBase<Derived> & m) {
        //fixme
        return m.derived().norm();
    }

    // Residual, gradient and jacobian
    // ===============================

    //! Compute the jacobian
    void compute_jacobian(const typename types::vector_t& x)
    {
        m_system->get_jacobian(x, m_jacobian);
        m_perf.nb_call_jacobian += 1;
    }

    //! Compute the residuals and store it in 'residual'
    void compute_residual(const typename types::vector_t& x,
                          typename types::vector_t& residual) const
    {
        m_system->get_residual(x, residual);
        m_perf.nb_call_residual += 1;
    }

    //! Compute the residuals, use internal storage
    void compute_residual(const typename types::vector_t& x)
    {
        m_system->get_residual(x, m_residuals);
        m_perf.nb_call_residual += 1;
    }

    double squared_residual(const typename types::vector_t& x) {
        compute_residual(x);
        return 0.5 * (m_system->scaling_f().asDiagonal() * m_residuals).squaredNorm();
    }

    //! \brief compute the gradient
    //!
    //! Assume that the residual and the jacobian had been computed before
    void compute_gradient(const typename types::vector_t& x)
    {
        m_gradient = ( m_jacobian.transpose()
                     * m_system->scaling_x().asDiagonal() * m_system->scaling_x().asDiagonal()
                     * m_residuals);
    }

    // Access
    // ------

    //! Return the gradient (J^T * Df^2 * R)
    const typename types::vector_t& get_gradient() const {return m_gradient;}
    //! Return the residuals (R)
    const typename types::vector_t& get_residuals() const {return m_residuals;}

    //! Return the scaling vector of the solution
    const typename types::vector_t& scaling_x() const {return m_system->scaling_x();}
    //! Return the scaling vector of the residuals
    const typename types::vector_t& scaling_f() const {return m_system->scaling_f();}

    const PerformanceCounter& get_performance() {return m_perf;}

    //

    void initialisationImpl(std::false_type){}
    void initialisationImpl(std::true_type)
    {
        typename types::list_triplets_t triplet_list;
        m_system->get_structure_jacobian(triplet_list);
        m_jacobian.setFromTriplets(triplet_list.begin(), triplet_list.end());
        m_qr_solver.analyzePattern(m_jacobian);

        m_sym_jacobian = m_jacobian.transpose() * m_jacobian;
        m_ldlt_solver.analyzePattern(m_sym_jacobian);
    }

    void initialize()
    {

        m_residuals = types::vector_t::Zero(get_neq());
        m_gradient = types::vector_t::Zero(get_neq());
        m_jacobian = typename types::matrix_t(get_neq(), get_neq());
        m_sym_jacobian = typename types::matrix_t(get_neq(), get_neq());

        initialisationImpl(std::integral_constant<bool, isSparse>());
    }

    // Matrix ...
    // ==========


    // Change state
    void set_maxstep_taken(bool is_step_max_taken = true) {m_max_taken = is_step_max_taken;}

private:
    // Global Method
    // ==============

    //! Call the global method
    template <GlobalMethod global_t>
    struct GlobalMethodImpl: public internal::GlobalMethodDriver<NLSolver, global_t> {
    };

    //! Estimate the condition number of R matrix from QR decomposition
    ScalarT estimate_condition_number();


    // The system to solve
    // --------------------
    std::shared_ptr<System> m_system;

    // Matrix stuff
    // ------------
    typename types::matrix_t m_jacobian;
    typename types::matrix_t m_sym_jacobian;
    typename solver_types::qr_solver_t m_qr_solver;
    typename solver_types::ldlt_solver_t m_ldlt_solver;

    // Residual
    // --------
    typename types::vector_t m_residuals;
    typename types::vector_t m_gradient;

    // Internal state
    // --------------
    bool m_max_taken;
    int m_consec_max;

    // Options
    // -------
    NLSolverOptions m_options;
    mutable PerformanceCounter m_perf;
};

} // end namespace nlsolver
} // end namespace dfpmsolver

#include "nlsolver.inl"

#endif // DFPM_NLSOLVER_NLSOLVER_HPP
