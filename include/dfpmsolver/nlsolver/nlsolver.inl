/*-------------------------------------------------------

 - Module : nlsolver
 - File : nesolver.inl
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include "nlsolver.hpp" // for syntaxic coloration
#include <float.h>

namespace dfpmsolver {
namespace nlsolver {

template <typename System, typename ScalarT, bool isSparse>
NLSolverReturnCode NLSolver<System, ScalarT, isSparse>::check_convergence(
        int nb_iteration,
        int global_retcode,
        typename types::vector_t& update,
        typename types::vector_t& solution)
{
    NLSolverReturnCode termcode = NLSolverReturnCode::NoConvergenceYet;
    if (global_retcode==1)
    {
        termcode = NLSolverReturnCode::FailedGlobalMethod;
    }
    else if (norm_residual<Eigen::Infinity>() < get_options().fvectol)
    {
        termcode = NLSolverReturnCode::ResidualMinimized;
    }
    else if (norm_update<Eigen::Infinity>(update, solution) < get_options().steptol)
    {
        termcode = NLSolverReturnCode::ErrorMinimized;
    }
    else if (nb_iteration >  get_options().max_iter)
    {
        termcode = NLSolverReturnCode::MaximumNumberOfIterations;
    }
    else if (m_max_taken)
    {
        ++m_consec_max;
        if (m_consec_max == get_options().maxiter_maxstep) {
            termcode = NLSolverReturnCode::MaximumNewtonLength;
        }
    }
    else
    {
        m_consec_max = 0;
    }
    return termcode;
}


template <typename System, typename ScalarT, bool isSparse>
template <GlobalMethod global_t>
NLSolverReturnCode NLSolver<System, ScalarT, isSparse>::solve(
        typename types::vector_t& solution)
{
    assert(solution.rows() == get_neq());
    initialize();
    double fc = squared_residual(solution);
    compute_jacobian(solution);
    compute_gradient(solution);
    int cnt = 0;
    NLSolverReturnCode termcode =  NLSolverReturnCode::NoConvergenceYet;
    while (termcode == NLSolverReturnCode::NoConvergenceYet)
    {
       typename types::vector_t update(types::vector_t::Zero(get_neq()));

        cnt += 1;
        newton_model(update);
        int retcode = GlobalMethodImpl<global_t>::run(*this, solution, update, fc);
        fc = squared_residual(solution);
        compute_jacobian(solution);
        compute_gradient(solution);
        termcode = check_convergence(cnt, retcode, update, solution);
        m_perf.nb_iterations += 1;
    }
    return termcode;
}

namespace internal {

template <class Derived1, class Derived2>
inline void add_to_diagonal(Eigen::MatrixBase<Derived1> matrix, Eigen::MatrixBase<Derived2> vector)
{
    matrix += vector.asDiagonal();
}

template <class Derived1, class Derived2>
inline void add_to_diagonal(Eigen::SparseMatrixBase<Derived1> matrix, Eigen::MatrixBase<Derived2> vector)
{
    assert(vector.rows() == matrix.rows());
    for (int i=0;i<matrix.rows(); ++i)
    {
        matrix.derived().coeffRef(i, i) += vector(i);
    }
}
} // end namespace internal

// ============ //
// Newton Model //
// ============ //

template <typename System, typename ScalarT, bool isSparse>
NLSolverReturnCode NLSolver<System, ScalarT, isSparse>::newton_model(typename types::vector_t& update)
{
    //M =  m_scaling_f.asDiagonal() * m_jacobian;


    double sqrtmacheps = std::sqrt(DBL_EPSILON); // Fixme

    m_jacobian = scaling_f().asDiagonal() * m_jacobian;
    internal::SolverInterface<isSparse>::factorize(m_qr_solver, m_jacobian);

    //if (m_qr_solver.info() != Eigen::Success)
    //{
    //    throw std::runtime_error("QR factorization failed");
   // }
    double est = estimate_condition_number();

    if ((m_qr_solver.rank() != get_neq()) or (est > 1.0/sqrtmacheps))
    {
        m_sym_jacobian = m_jacobian.transpose() * m_jacobian;
        // compute hnorm
        double hnorm = norm_matrix<1>(m_sym_jacobian);
        internal::add_to_diagonal(m_sym_jacobian, (std::sqrt(get_neq())*sqrtmacheps*hnorm*
                           scaling_x().cwiseProduct(scaling_x())));
        internal::SolverInterface<isSparse>::factorize(m_ldlt_solver, m_sym_jacobian);
        internal::SolverInterface<isSparse>::solve(m_ldlt_solver, get_gradient(), update);
    }
    else
    { // normal Newton step
        update = scaling_f().asDiagonal() * (-m_residuals);
        internal::SolverInterface<isSparse>::solve(m_qr_solver, update);
    }

    m_jacobian = scaling_f().asDiagonal().inverse() * m_jacobian;
    return NLSolverReturnCode::NoConvergenceYet;
}

template <typename System, typename ScalarT, bool isSparse>
ScalarT NLSolver<System, ScalarT, isSparse>::estimate_condition_number()
{
    // R <- R.D_x^-1
    //typename types::matrix_t
    const auto scaled = (m_qr_solver.matrixR().template triangularView<Eigen::Upper>() *
                                       (m_qr_solver.colsPermutation() * scaling_x().array().inverse().matrix()) );

    typename types::vector_t pm(get_neq());
    typename types::vector_t p(get_neq());
    typename types::vector_t x(get_neq());

    //ScalarT est = norm_matrix<1>(scaled.template triangularView<Eigen::Upper>() * types::matrix_t::Identity(get_neq(), get_neq()));
    ScalarT est = scaled.coeff(0, 0);
    for (int i=1; i<scaled.cols(); ++i)
    {
        est = std::max(est, scaled.block(0, i, i+1, 1).template lpNorm<1>());
    }

    x(0) = 1.0/scaled(0, 0);
    for (int i=1;i<get_neq();++i)
    {
        p(i) = scaled.coeff(1, i)*x(0);
    }
    for (int j=1; j<get_neq(); ++j)
    {
        double xp = (1-p.coeff(j))/scaled.coeff(j, j);
        double xm = (-1-p.coeff(j))/scaled.coeff(j, j);
        double temp = std::abs(xp);
        double tempm = std::abs(xm);
        for (int i=j+1; i<get_neq(); ++i)
        {
            pm.coeffRef(i) = p.coeff(i) + scaled.coeff(j, i)*xm;
            tempm += (std::abs(pm.coeff(i)) / std::abs(scaled.coeff(i, i)));
            p.coeffRef(i) += scaled.coeff(j, i)*xp;
            temp += (std::abs(p.coeff(i)) / std::abs(scaled.coeff(i, i)));
        }
        if (temp >= tempm)
        {
            x.coeffRef(j) = xp;
        }
        else
        {
            x.coeffRef(j) = xm;
            for (int i=j+1; i<get_neq(); ++i) p.coeffRef(i) = pm.coeff(i);
        }
        ScalarT xnorm = x.template lpNorm<1>();
        est = est / xnorm;
        scaled.template triangularView<Eigen::Upper>().solve(x);
        xnorm = x.template lpNorm<1>();
        est = est * xnorm;
    }
    return est;
}

// ==============//
// Global Method //
// ==============//

namespace internal {

template <class SolverT>
struct GlobalMethodDriver<SolverT, GlobalMethod::Backtracking>
{
    static int run(
            SolverT& solver,
            typename SolverT::types::vector_t& x,
            typename SolverT::types::vector_t& p,
            typename SolverT::types::scalar_t fc)
    {
        typename SolverT::SystemPtr system = solver.get_system();
        const NLSolverOptions& options = solver.get_options();
        solver.set_maxstep_taken(false);
        int retcode = 2;
        const double alpha = 1e-4;
        double newtlen = (system->scaling_x().asDiagonal()*p).norm();
        if (newtlen > options.maxstep )
        {
            p = p*options.maxstep/newtlen;
            newtlen = options.maxstep;
        }
        double init_slope = solver.get_gradient().dot(p);
        double rellength = solver.template norm_update<Eigen::Infinity>(p, x);
        double minlambda = options.steptol / rellength;
        double lambda = 1.0;
        double lambda_prev = 1.0;

        typename SolverT::types::vector_t xp(solver.get_neq());
        double fcp;
        double fcp_prev;
        do
        {
            xp = x + lambda*p;
            fcp = solver.squared_residual(xp);
            if (fcp <= fc + alpha*lambda*init_slope)
            {
                retcode = 0;
                if (lambda ==1 and (newtlen > 0.99 * options.maxstep)) {
                    solver.set_maxstep_taken();
                }
                break;
            }
            else if (lambda < minlambda)
            {
                retcode = 1;
                break;
            }
            else
            {
                double lambdatmp;
                if (lambda == 1.0) {
                    lambdatmp = - init_slope / (2*(fcp - fc -init_slope));
                }
                else
                {
                    const double factor = 1 /(lambda - lambda_prev);
                    const double x1 = fcp - fc - lambda*init_slope;
                    const double x2 = fcp_prev - fc - lambda_prev*init_slope;

                    const double a = factor * ( x1/(lambda*lambda) - x2/(lambda_prev*lambda_prev));
                    const double b = factor * ( -x1*lambda_prev/(lambda*lambda) + x2*lambda/(lambda_prev*lambda_prev));

                    if (a == 0)
                    { // cubic is quadratic
                        lambdatmp = - init_slope/(2*b);
                    }
                    else
                    {
                        const double disc = b*b-3*a*init_slope;
                        lambdatmp = (-b+std::sqrt(disc))/(3*a);
                    }
                    if (lambdatmp > 0.5 ) lambdatmp = 0.5*lambda;
                }
                lambda_prev = lambda;
                fcp_prev = fcp;
                if (lambdatmp < 0.1*lambda) {
                    lambda = 0.1 * lambda;
                } else {
                    lambda = lambdatmp;
                }
            }
        } while(retcode < 2);
        x = xp;
        p = lambda*p;
        return retcode;
    }
};

// Solver interface
// ================

// non sparse
// ----------

template <>
template <class SolverT>
inline SolverReturnCode SolverInterface<false>::factorize(SolverT& solver, typename SolverT::MatrixType& matrix)
{
    solver.compute(matrix);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::FactorisationFailure;
}

template <>
template <class SolverT, class Rhs, class Sol>
inline SolverReturnCode SolverInterface<false>::solve(SolverT& solver,
                                               const typename Eigen::MatrixBase<Rhs>& rhs,
                                               typename Eigen::MatrixBase<Sol>& solution)
{
    solution = solver.solve(rhs);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::SolveFailure;
}

template <>
template <class SolverT, class Rhs>
inline SolverReturnCode SolverInterface<false>::solve(SolverT& solver, typename Eigen::MatrixBase<Rhs>& rhs)
{
    rhs = solver.solve(rhs);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::SolveFailure;
}

// sparse
// ------

template <>
template <class SolverT>
inline SolverReturnCode SolverInterface<true>::factorize(SolverT& solver, typename SolverT::MatrixType& matrix)
{
    solver.factorize(matrix);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::FactorisationFailure;
}

template <>
template <class SolverT, class Rhs, class Sol>
inline SolverReturnCode SolverInterface<true>::solve(SolverT& solver,
                                               const typename Eigen::MatrixBase<Rhs>& rhs,
                                               typename Eigen::MatrixBase<Sol>& solution)
{
    solution = solver.solve(rhs);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::SolveFailure;
}

template <>
template <class SolverT, class Rhs>
inline SolverReturnCode SolverInterface<true>::solve(SolverT& solver, typename Eigen::MatrixBase<Rhs>& rhs)
{
    rhs = solver.solve(rhs);
    if (solver.info() == Eigen::Success) return SolverReturnCode::Success;
    else return SolverReturnCode::SolveFailure;
}

} // end namespace internal



} // end namespace nlsolver
} // end namespace dfpms
