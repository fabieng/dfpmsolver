/*-------------------------------------------------------

 - Module : nlsolver
 - File : nlsystem.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_NLSOLVER_NLSYSTEM_HPP
#define DFPM_NLSOLVER_NLSYSTEM_HPP

//! \file nesystem.hpp A system to solver

#include <type_traits>
#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace dfpmsolver {
namespace nlsolver {

template <typename ScalarT, bool isSparse>
struct NLSystemTypes
{
};

template <typename ScalarT>
struct NLSystemTypes<ScalarT, true>
{
    using scalar_t = ScalarT;
    using vector_t = Eigen::Matrix<ScalarT, Eigen::Dynamic, 1>;
    using matrix_t = Eigen::SparseMatrix<ScalarT>;
    template <class Derived>
    using matrixbase_t = Eigen::SparseMatrixBase<Derived>;

    using triplet_t = Eigen::Triplet<ScalarT>;
    using list_triplets_t = std::vector<triplet_t>;
};

template <typename ScalarT>
struct NLSystemTypes<ScalarT, false>
{
    using scalar_t = ScalarT;
    using vector_t = Eigen::Matrix<ScalarT, Eigen::Dynamic, 1>;
    using matrix_t = Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic>;
    template <class Derived>
    using matrixbase_t = Eigen::MatrixBase<Derived>;

    using list_triplets_t = int;
};

namespace internal {


template <bool isSparse>
struct FormStructJacobianImpl
{
    template <typename ScalarT>
    static inline void add_non_zero(typename NLSystemTypes<ScalarT, isSparse>::list_triplets_t& triplet_list, int row, int col);

};

template <>
template <typename ScalarT>
inline void FormStructJacobianImpl<true>::add_non_zero(
        typename NLSystemTypes<ScalarT, true>::list_triplets_t& triplet_list, int row, int col)
{
    triplet_list.push_back(typename NLSystemTypes<ScalarT, true>::triplet_t(row, col, 1.0));
}


template <>
template <typename ScalarT>
inline void FormStructJacobianImpl<false>::add_non_zero(
        typename NLSystemTypes<ScalarT, false>::list_triplets_t& triplet_list, int row, int col)
{}

} // end namespace internal

template <typename ScalarT, bool isSparse>
class NLSystem
{
public:
    using types = NLSystemTypes<ScalarT, isSparse>;

    NLSystem() {}

    int get_number_variables();
    int get_number_equations() {return get_number_variables();}

    void get_residual(const typename types::vector_t& x,
                      typename types::vector_t& residual);

    void add_nonzero(typename types::list_triplets_t& triplet_list,
                     int row,
                     int col)
    {
        internal::FormStructJacobianImpl<isSparse>::template add_non_zero<ScalarT>(triplet_list, row, col);
    }

    void get_structure_jacobian(typename std::enable_if<isSparse, typename types::list_triplets_t>& triplet_list);

    // fixme : use triplet list also
    void get_jacobian(const typename types::vector_t& x,
                      typename types::matrix_t& jacobian);

    const typename types::vector_t& scaling_x();
    ScalarT& scaling_x(int i);
    const typename types::vector_t& scaling_f();
    ScalarT& scaling_f(int j);
};

} // end namespace nlsolver
} // end namespace dfpms

#endif // DFPM_NESOLVER_NESYSTEM_HPP
