/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver/options.h
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_OPTIONS_H
#define DFPM_OPTIONS_H

//! \file options.h define options for the solver

#include <float.h>

namespace dfpmsolver {

//! \enum JacobianUpdate Options to perform Quasi-Newton w or w/o Broyden update
enum JacobianUpdate {
    NOUPDATE, //!< No update performed
    BROYDEN,  //!< 'good' Broyden update, following Deuflhard algorithm
    BROYDEN_ENGELMAN //!< 'good' Broyden update, following Engelman algorithm
};


struct NewtonTolerance
{
    NewtonTolerance():
        machine_epsilon(DBL_EPSILON),
        max_iterations(50),
        residual_tolerance(1e-10),
        step_tolerance(std::pow(machine_epsilon, 2.0/3.0)),
        maxstep(1e3), // will be scaled at the beginning of the problem
        eps_j(1e-8)
    {}

    double machine_epsilon;
    int max_iterations;   //!< Maximum non-linear iterations allowed
    double residual_tolerance;     //!< Relative tolerance of the solver
    double step_tolerance;         //!< 'steptol'
    double maxstep;                //!< To detect domain oleaving and divergence
    double eps_j;                  //!< Precision to compute the FD jacobian
};


} // end namespace dfpmsolver

#endif // DFPM_OPTIONS_H
