/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : include/dfpmsolver/simplex.h
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_SIMPLEX_H
#define DFPM_SIMPLEX_H

//! \file simplex.h Non-linear system solver using SIMPLEX algorithm
//!
//! References :
//!  - Wood, J. (1993). Calculation of fluid-mineral equilibria using the simplex algorithm.
//!                  Computers & Geosciences, 19(1):23-39.
//!  - Caceci, M. S. and Cacheris, W. P. (1984). Fitting curves to data.
//!                  Byte, 9(5):340-362.
//!


#include <functional>
#include <Eigen/Dense>

namespace dfpmsolver {


class SimplexSolver
{
public:
    using ResidualF = std::function<double (const Eigen::VectorXd&)>;

    SimplexSolver(const Eigen::ArrayXXd& simplex, ResidualF residual):
        m_dim(simplex.cols()),
        m_simplex(simplex),
        m_residuals(m_dim+1),
        m_comp_residual(residual),
        m_alpha(1.0),
        m_beta(0.5),
        m_gamma(2.0)
    {
        for (int vertex=0; vertex<m_dim+1; ++vertex)
            m_residuals(vertex) = compute_residual(m_simplex.row(vertex).transpose());

        sort();
    }

    int run_iteration();

    Eigen::VectorXd get_best_approximation()
    {
        return m_simplex.row(m_best).transpose();
    }

    int solve();

    bool check_convergence();

private:
    double compute_residual(const Eigen::VectorXd& vertex)
    {
        return m_comp_residual(vertex);
    }


    void sort();

    int m_dim;
    Eigen::ArrayXXd m_simplex;
    Eigen::VectorXd m_residuals;

    ResidualF m_comp_residual;

    double m_alpha;
    double m_beta;
    double m_gamma;

    int m_best;
    int m_worst;

    Eigen::ArrayXXd m_old;

};

} // end namespace dfpmsolver

#endif // DFPM_SIMPLEX_H
