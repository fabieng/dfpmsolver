/*-------------------------------------------------------

 - Module : snlintp
 - File : snlintpt.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_SNLINTPT_SNLINPT_HPP
#define DFPM_SNLINTPT_SNLINPT_HPP

//! \file snlintpt.hpp Interior point method for equalities and inequalities

#include <memory>
#include <functional>
#include "snlprog.hpp"

namespace dfpmsolver {
namespace snlintpt {

//! Options to customize the behavior of the NonLinearIntPoint class
struct SNLInteriorPointOptions
{
    double init_slack; //!< Value used to initialize the slack variables
    double tol_norm;   //!< Tolerance to decide of optimality
    double threshold_infeasible; //!< Threshold to decide of infeasibility
    double delta;      //!< Multiplication factor to update mu
    double underrelax_factor; //!< Underrelaxation factor to ensure positivity
    int max_iter;             //!< Maximum number of iterations
    double maxstepx;

    SNLInteriorPointOptions():
        init_slack(1.0),
        tol_norm(1e-8),
        threshold_infeasible(1e19),
        delta(0.1),
        underrelax_factor(0.95),
        max_iter(500),
        maxstepx(10.0)
    {}
};

struct SNLInteriorPointPerformance
{
    double nb_iterations;
    double nb_factorisation;
};

//! Return code for the NL interior point algorithm
enum SNLInteriorPointReturnCode
{
    NegativeVariableThatIsNotSupposedToBe = -5,
    FatalError          = -4,
    MaxIterationReached = -3,
    DualInfeasible      = -2,
    PrimalInfeasible    = -1,
    NotConvergedYet     = 0,
    Optimal             = 1
};

template <class Program, typename ScalarT>
class SNLInteriorPoint
{
public:
    using traits = SNonLinearProgramTraits<ScalarT>;
    using program_ptr = std::shared_ptr<Program>;
    //using solver_t = Eigen::SimplicialLDLT<typename traits::spmatrix_t>;                           // the solver
    using solver_t = Eigen::SparseLU<typename traits::spmatrix_t>;                           // the solver
    using selfadjoint_t = Eigen::SparseSelfAdjointView<typename traits::spmatrix_t, Eigen::Lower>; // the matrix is self adjoint

    SNLInteriorPoint(program_ptr program):
        m_program(program)
    {}


    // Easy access
    // -----------

    //! Return number of variables of the problem
    int get_nb_variables() {return m_program->get_number_variables();}
    //! Return the number of constraints of the problem
    int get_nb_equalities() {return m_program->get_number_equalities();}
    int get_nb_inequalities() {return m_program->get_number_inequalities();}
    int get_nb_constraints() {return m_program->get_number_equalities() + m_program->get_number_inequalities();}

    //! Read-Write access to the options
    SNLInteriorPointOptions& get_options() {return m_options;}

    // Algorithm
    // ----------

    //! Optimize using starting point x
    SNLInteriorPointReturnCode optimize(typename traits::vector_t& x);

    //! Initialize the problem
    void initialization(typename traits::vector_t& x);
    //! Check if the algorithm has converged
    SNLInteriorPointReturnCode check_convergence(typename traits::vector_t& x);
    //! Choose a barrier parameter
    void choose_mu();
    //! solve KKT system
    void solve_KKT(typename traits::vector_t& x);

    SNLInteriorPointPerformance get_performance() const {return m_perf;}
    SNLInteriorPointPerformance& get_performance() {return m_perf;}

private:
    int linesearch(typename traits::vector_t& dx,
                   typename traits::vector_t& x,
                   double& lambda);
    // Private methods
    // ###############
    void search_for_jamming(const typename traits::vector_t& u,
                            const typename traits::vector_t& du,
                            int &k,
                            double &d);
    void correct_jamming(const typename traits::vector_t& u,
                         typename traits::vector_t& du,
                         double d);
    ScalarT barrier_function(const typename traits::vector_t& x);

    // Matrix stuff
    // ------------
    //! build the KKT matrix to solve
    void build_skeleton_KKT_matrix(const typename traits::vector_t &x);
    //! perturb the upper diagonal of the KKT matrix
    void perturb_KKT_matrix(typename traits::vector_t& d_factorization);


    // Attributes
    // ##########

    // The program
    // -----------

    program_ptr m_program;

    // Options
    // -------

    SNLInteriorPointOptions m_options;

    // Matrix stuff
    // ------------

    typename traits::spmatrix_t m_matrix;
    solver_t m_solver;
    bool first_iter;

    // Barrier parameter
    // -----------------

    ScalarT m_mu;

    // Slack variables
    // ---------------

    // primal
    typename traits::vector_t sv_s;
    // dual
    typename traits::vector_t sv_y;
    typename traits::vector_t sv_z;

    // Feasibilities
    // -------------

    typename traits::vector_t m_rho_e;
    typename traits::vector_t m_rho_i;
    typename traits::vector_t m_sigma;
    ScalarT m_norm_rho_e;
    ScalarT m_norm_rho_i;
    ScalarT m_norm_sigma;
    ScalarT m_gamma;

    // Check performance
    // -----------------

    SNLInteriorPointPerformance m_perf;


};

} // end namespace snlintpt
} // end namespace dfpmsolver

// implementation
#include "snlintpt.inl"


#endif // DFPM_SNLINTPT_SNLINPT_HPP
