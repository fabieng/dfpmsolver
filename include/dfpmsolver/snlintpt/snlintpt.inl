/*-------------------------------------------------------

 - Module : snlintpt
 - File : snlintpt.inl
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include "snlintpt.hpp" // for syntax coloring...

#include <iostream>

namespace dfpmsolver {
namespace snlintpt {

template <class Program, typename ScalarT>
SNLInteriorPointReturnCode SNLInteriorPoint<Program, ScalarT>::optimize(typename traits::vector_t& x)
{
    initialization(x);
    int cnt = 0;
    SNLInteriorPointReturnCode retcode = NotConvergedYet;
    first_iter = true;
    while (cnt < get_options().max_iter)
    {
        ++cnt;
//        std::cout << "Iteration : " << cnt << std::endl;
        retcode = check_convergence(x);

        if (m_gamma < 0) // Check for a bad update
        {
            retcode = NegativeVariableThatIsNotSupposedToBe;
            break;
        }
        if (retcode != NotConvergedYet)
        {
            break;
        }
        choose_mu();
        solve_KKT(x);
        m_program->hook_end_iter(x);
    }
    if (cnt == get_options().max_iter)
    {
        retcode = MaxIterationReached;
    }
    get_performance().nb_iterations = cnt;
    return retcode;
}

template <class Program, typename ScalarT>
void SNLInteriorPoint<Program, ScalarT>::initialization(typename traits::vector_t& x)
{
    const int n = get_nb_variables();
    const int me = get_nb_equalities();
    const int mi = get_nb_inequalities();

    // the matrix
    m_matrix = typename traits::spmatrix_t(n+me+mi,n+me+mi);
    // primal slack variables
    // Lagrangian variable
    sv_y = traits::vector_t::Constant(me, 1.0);// get_options().init_slack);
    // infeasibilities
    m_rho_e = typename traits::vector_t(me);
    if (mi>0)
    {
        sv_s = typename traits::vector_t(mi);
        m_program->inequalities(x, sv_s);
        sv_s = (sv_s).cwiseMax(traits::vector_t::Constant(n,get_options().init_slack));
        m_rho_i = traits::vector_t::Zero(mi);
        sv_z = traits::vector_t::Constant(mi, get_options().init_slack);
    }
    m_norm_rho_i = 0;
    m_sigma = typename traits::vector_t(n);
}


template <class Program, typename ScalarT>
SNLInteriorPointReturnCode SNLInteriorPoint<Program, ScalarT>::check_convergence(typename traits::vector_t& x)
{
    const int n = get_nb_variables();
    const int me = get_nb_equalities();
    const int mi = get_nb_inequalities();
    //
    build_skeleton_KKT_matrix(x);
    //
    m_program->equalities(x, m_rho_e);
    m_sigma.noalias() = m_program->gradient_objective_function(x);
    m_sigma.noalias() -= m_matrix.block(n, 0, me, n).transpose()*sv_y;
    if (mi > 0)
    {
        m_program->inequalities(x, m_rho_i);
        m_rho_i = sv_s - m_rho_i;
        m_norm_rho_i = m_rho_i.norm();
        m_sigma.noalias() += m_matrix.block(n+me, 0, mi, n).transpose()*sv_z;
    }

    m_gamma = sv_z.dot(sv_s);
    m_norm_rho_e = m_rho_e.norm();
    m_norm_sigma = m_sigma.norm();

//    std::cout << "norm : " << m_norm_rho_e
//              << " - " << m_norm_rho_i
//              << " - " << m_norm_sigma
//              << " - " << m_gamma << std::endl;

    //std::cout << "sv_y : " << sv_y << std::endl;

    if (   ( m_norm_rho_e < get_options().tol_norm )
       and ( m_norm_rho_i < get_options().tol_norm )
       and ( m_norm_sigma < get_options().tol_norm )
       and ( m_gamma < get_options().tol_norm )
       )
    {
        return Optimal;
    }
    else if (m_norm_rho_e > get_options().threshold_infeasible
             or m_norm_rho_i > get_options().threshold_infeasible)
    {
        return PrimalInfeasible;
    }
    else if (m_norm_sigma > get_options().threshold_infeasible)
    {
        return DualInfeasible;
    }
    return NotConvergedYet;
}

template <class Program, typename ScalarT>
void SNLInteriorPoint<Program, ScalarT>::build_skeleton_KKT_matrix(const typename traits::vector_t& x)
{
    const int n = get_nb_variables();
    const int me = get_nb_equalities();
    const int mi = get_nb_inequalities();
    typename traits::list_triplets_t triplet_list;
    m_program->hessian_mod_objective_function(x, triplet_list, 0, 0);
    m_program->hessian_mod_equalities(x, sv_y, triplet_list, 0, 0);
    m_program->hessian_mod_inequalities(x, sv_z, triplet_list, 0, 0);
    m_program->jacobian_equalities(x, triplet_list, n, 0);
    m_program->jacobian_inequalities(x, triplet_list, n+me, 0);
    // add a full diagonal // E,F and lambda I will be added later
    triplet_list.reserve(triplet_list.size() + mi);
    for (int i=n+me; i<n+me+mi; ++i)
    {
        triplet_list.push_back(typename traits::triplet_t(i,i,0));
    }
    // build the matrix
    m_matrix.setFromTriplets(triplet_list.begin(), triplet_list.end());
}


template <class Program, typename ScalarT>
void SNLInteriorPoint<Program, ScalarT>::choose_mu()
{
    const double mi = get_nb_inequalities();
    if (mi == 0)
    {
      m_mu = 0.0;
      return;
     }
    double xi = mi*(sv_z.cwiseProduct(sv_s).minCoeff())/sv_z.dot(sv_s);
    xi = std::min((1.0-get_options().underrelax_factor)*((1-xi)/xi), 2.0);
    m_mu = get_options().delta * std::pow(xi,3.0)*m_gamma/(2*mi);
}


template <class Program, typename ScalarT>
void SNLInteriorPoint<Program, ScalarT>::solve_KKT(typename traits::vector_t &x)
{

    const int n = get_nb_variables();
    const int me = get_nb_equalities();
    const int mi = get_nb_inequalities();
    typename traits::vector_t gamma_s,e;
    // Temp vector
    // -----------
    if (mi > 0)
    {
        gamma_s = m_mu*sv_s.cwiseInverse() - sv_z;
        e =  (sv_s.array()/sv_z.array()).matrix();

        // Add E  in KKT
        // ------------------
        for (int i=0; i<mi; ++i)
        {
            m_matrix.coeffRef(n+me+i,n+me+i) += e(i);
        }
    }

    if (first_iter)
    {
        m_solver.isSymmetric(true);
        m_solver.analyzePattern(selfadjoint_t(m_matrix));
        //m_solver.analyzePattern(m_matrix.block(n, 0, me, n));
        first_iter = false;
    }

    m_solver.factorize(selfadjoint_t(m_matrix));
    //m_solver.factorize(m_matrix.block(n, 0, me, n));
    get_performance().nb_factorisation += 1;

    // assemble RHS
    // ------------
    typename traits::vector_t rhs(get_nb_variables()+get_nb_constraints());
    rhs.block(0,0,n,1) = ( m_sigma );
    rhs.block(n,0,me,1) = ( m_rho_e );
    if (mi > 0)
    {
        rhs.block(n+me,0,mi,1) = ( m_rho_i + e.cwiseProduct(gamma_s) );
    }

    //std::cout << m_matrix.block(n, 0, me, n) << std::endl;
    //std::cout << rhs.block(n, 0, n, 1) << std::endl;
    // Solve the system
    // -----------------

    const typename traits::vector_t solution = m_solver.solve(rhs);
    //const typename traits::vector_t solution = m_solver.solve(rhs.block(n, 0, n, 1));
    if (m_solver.info() != Eigen::Success)
    {
       throw std::runtime_error("Error when Solving KKT system... ");
    }


    // Updates
    // -------

    typename traits::vector_t dx = solution.block(0,0,n,1);
    const auto dy = solution.block(n,0,me,1);
    typename traits::vector_t dz, ds;
    double theta = 1.0;
    if (dx.norm() > 3) theta /= dx.norm();
    int k = 0;

    if (mi > 0)
    {
        dz = solution.block(n+me,0,mi,1);
        ds = e.cwiseProduct(gamma_s - dz);
        // Anti jamming
        // ------------
        //bool jamming = false;
        double d = 0;
        search_for_jamming(sv_s, ds, k, d);
        if (k > 0)
        {
            d = k/d;
            correct_jamming(sv_s, ds, d);
        }

    // linesearch
    // ----------
    double theta = std::max({
                      (-ds.array()/sv_s.array()).maxCoeff(),
                      (-dz.array()/sv_z.array()).maxCoeff(),
             });
    theta = std::min(get_options().underrelax_factor/theta, 1.0);
    }
    //std::cout << "theta : " << theta << std::endl;


    linesearch(dx, x, theta);

    //x += theta*dx;
    sv_y += theta*dy;
    if (mi > 0)
    {
        sv_s += theta*ds;
        sv_z += theta*dz;
    }

    //std::cout << "theta " << theta << std::endl;
   // std::cout << "dx : \n--------------"  << std::endl << dx << "\n--------------" << std::endl;
    //std::cout << "dy : \n--------------"  << std::endl << dy << "\n--------------" << std::endl;
}

template <class Program, typename ScalarT>
ScalarT SNLInteriorPoint<Program, ScalarT>::barrier_function(const typename traits::vector_t& x)
{
    double phi = 0;
    for (int i=0; i<get_nb_inequalities(); ++i)
    {
        phi += std::log(sv_s(i));
    }
    phi = m_program->objective_function(x) - m_mu*phi;
    return phi;
}


template <class Program, typename ScalarT>
int SNLInteriorPoint<Program, ScalarT>::linesearch(typename traits::vector_t& dx,
                                                   typename traits::vector_t& x,
                                                   double& lambda)
{

    const int n = get_nb_variables();
    const int me = get_nb_equalities();

    int retcode = 2;
    const double alpha = 1e-4;
    double newtlen = dx.norm();
    if (newtlen > get_options().maxstepx )
    {
        dx = dx*get_options().maxstepx/newtlen;
        newtlen = get_options().maxstepx;
    }
    typename traits::vector_t gradient = m_matrix.block(n, 0, me, n).transpose()*m_rho_e;
    double init_slope = gradient.dot(dx);
    double rellength = (dx.array().abs()/x.array()).maxCoeff();
    double minlambda = get_options().tol_norm / rellength;
    double lambda_prev = 1.0;

    typename traits::vector_t xp(me);
    typename traits::vector_t rhop(me);
    double fc = m_norm_rho_e;
    double fcp;
    double fcp_prev = fc;
    do
    {
        xp = x + lambda*dx;
        m_program->equalities(xp, rhop);
        fcp = rhop.norm();
        if (fcp <= fc + alpha*lambda*init_slope)
        {
            retcode = 0;
            if (lambda ==1 and (newtlen > 0.99 * get_options().maxstepx)) {
                //m_max_taken = true;
            }
            break;
        }
        else if (lambda < minlambda)
        {
            retcode = 1;
            break;
        }
        else
        {
            double lambdatmp;
            if (lambda == 1.0) {
                lambdatmp = - init_slope / (2*(fcp - fc -init_slope));
            }
            else
            {
                const double factor = 1 /(lambda - lambda_prev);
                const double x1 = fcp - fc - lambda*init_slope;
                const double x2 = fcp_prev - fc - lambda_prev*init_slope;

                const double a = factor * ( x1/(lambda*lambda) - x2/(lambda_prev*lambda_prev));
                const double b = factor * ( -x1*lambda_prev/(lambda*lambda) + x2*lambda/(lambda_prev*lambda_prev));

                if (a == 0)
                { // cubic is quadratic
                    lambdatmp = - init_slope/(2*b);
                }
                else
                {
                    const double disc = b*b-3*a*init_slope;
                    lambdatmp = (-b+std::sqrt(disc))/(3*a);
                }
                if (lambdatmp > 0.5 ) lambdatmp = 0.5*lambda;
            }
            lambda_prev = lambda;
            fcp_prev = fcp;
            if (lambdatmp < 0.1*lambda) {
                lambda = 0.1 * lambda;
            } else {
                lambda = lambdatmp;
            }
        }
    } while(retcode < 2);
    x = xp;
    dx = lambda*dx;
    return lambda;

}

} // end namespace snlintpt
} // end namespace dfpmsolver
