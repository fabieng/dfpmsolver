/*-------------------------------------------------------

 - Module : snlintpt
 - File : snlprog.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef DFPM_SNLINPT_SNLPROG_HPP
#define DFPM_SNLINPT_SNLPROG_HPP

//! \file snlprog.hpp non liniear program for SNLinteriorPoint

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <vector>

namespace dfpmsolver {
namespace snlintpt {


//! \brief Define common types
template <typename ScalarT>
struct SNonLinearProgramTraits
{
    using vector_t = Eigen::Matrix<ScalarT,Eigen::Dynamic,1>;
    using matrix_t = Eigen::Matrix<ScalarT,Eigen::Dynamic,Eigen::Dynamic>;
    using spmatrix_t = Eigen::SparseMatrix<ScalarT>;
    using triplet_t = Eigen::Triplet<ScalarT>;
    using list_triplets_t = std::vector<triplet_t>;
    using index_t = typename spmatrix_t::Index;
};


//! \brief Static interface of a non linear program
//!
//! To be inheritated from using CRTP
template <class Derived, class ScalarT>
class SNonLinearProgram
{
public:
    using traits = SNonLinearProgramTraits<ScalarT>;
    SNonLinearProgram() {}

    //! Return the derived class
    Derived* derived() {
        return static_cast<Derived*>(this);
    }

    // Return the number of variables
    int get_number_variables() {return 0;}
    // Return the number of equalitity constraints
    int get_number_equalities() {return 0;}
    // Return the number of inequalities constraints
    int get_number_inequalities() {return 0;}

    //! Return objective function value
    ScalarT objective_function(const typename traits::vector_t& x) {}
    //! Return gradient of objective function
    typename traits::vector_t gradient_objective_function(const typename traits::vector_t& x) {}
    //! Return minus hessian of objective function
    void hessian_mod_objective_function(const typename traits::vector_t& x,
                                        typename traits::list_triplets_t& triplet_list,
                                        const typename traits::index_t offset_row,
                                        const typename traits::index_t offset_col) {}


    //! Return value constraints
    void equalities(const typename traits::vector_t& x, typename traits::vector_t& out) {}
    //! Return Jacobian constraints
    void jacobian_equalities(const typename traits::vector_t& x,
                              typename traits::list_triplets_t& triplet_list,
                              const typename traits::index_t offset_row,
                              const typename traits::index_t offset_col) {}
    //! Return sum of - y times hessian_i
    void hessian_mod_equalities(const typename traits::vector_t& x,
                                 const typename traits::vector_t& y,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col) {}
    //! Return value constraints
    void inequalities(const typename traits::vector_t& x, typename traits::vector_t& out) {}
    //! Return Jacobian constraints
    void jacobian_inequalities(const typename traits::vector_t& x,
                              typename traits::list_triplets_t& triplet_list,
                              const typename traits::index_t offset_row,
                              const typename traits::index_t offset_col) {}
    //! Return sum of - y times hessian_i
    void hessian_mod_inequalities(const typename traits::vector_t& x,
                                 const typename traits::vector_t& y,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col) {}

    void hook_end_iter(typename traits::vector_t& x) {}
};

} // end namespace nlintpt
} // end namespace dfpmsolver


#endif // DFPM_SNLINPT_SNLPROG_HPP
