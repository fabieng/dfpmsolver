/*-------------------------------------------------------

 - Module : intpt
 - File : lp_intpt.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include "dfpmsolver/intpt/lp_intpt.hpp"
#include <iostream>

namespace dfpmsolver {
namespace intpt {

// Initialize slack variables
// ==========================
void  InteriorPointLP::init_slack_variables() {
    sv_f = matrix_t::Constant(get_nb_constraints(), 1, get_options().init_slack);
    sv_p = matrix_t::Constant(get_nb_constraints(), 1, get_options().init_slack);
    sv_t = matrix_t::Constant(get_nb_variables(), 1, get_options().init_slack);
    sv_g = matrix_t::Constant(get_nb_variables(), 1, get_options().init_slack);
    sv_y = matrix_t::Constant(get_nb_constraints(), 1, get_options().init_slack);
    sv_v = matrix_t::Constant(get_nb_constraints(), 1, get_options().init_slack);
    sv_q = matrix_t::Constant(get_nb_constraints(), 1, get_options().init_slack);
    sv_s = matrix_t::Constant(get_nb_variables(), 1, get_options().init_slack);
    sv_h = matrix_t::Constant(get_nb_variables(), 1, get_options().init_slack);
}

// Initialize the solver
// =====================
void InteriorPointLP::init_solver()
{
    const index_t n = get_nb_variables();
    const index_t m = get_nb_constraints();

    // Reserve space
    // -------------
    // remember : we only built the upper triangular of the matrix
    m_matrix = spmatrix_t(n+m, n+m);
    m_matrix.reserve(n+m+m_pb->get_constraints().nonZeros());


    // Initialize non zero coefficients in upper triangular matrix
    // ------------------------------------------------------------
    m_matrix.setIdentity();
    for (int k=0; k< m_pb->get_constraints().outerSize(); ++k)
    for (Eigen::SparseMatrix<double>::InnerIterator it(m_pb->get_constraints(),k); it; ++it)
    {
        m_matrix.coeffRef(it.row(),m+it.col()) = it.value();
    }
    // Prepare future factorizations
    // -----------------------------
    m_solver.analyzePattern(selfadjoint_t(m_matrix));
}

// One iteration of the solver
// ===========================
// Follow algorithm pg343 of Vanderbei2007
ReturnCodeIntPtLP InteriorPointLP::one_iteration(vector_t& x)
{
    // Check for convergence
    // =====================

    // Compute primal, dual feasibility
    // --------------------------------
    // note : formula are not accurate in the book
    // w = f
    // z = h -s
    m_rho = m_pb->upper_bounds_constraints() - m_pb->get_constraints()*x - sv_f;
    m_sigma = m_pb->get_objective() - m_pb->get_constraints().transpose()*sv_y - sv_s + sv_h;

    const double gamma = sv_f.dot(sv_v) + sv_p.dot(sv_q) + sv_t.dot(sv_s) + sv_g.dot(sv_h);

    const double norm_rho = m_rho.norm();
    const double norm_sigma = m_sigma.norm();

    // Check for convergence or infesibility
    // -------------------------------------
    if (norm_rho < get_options().tol_norm and norm_sigma < get_options().tol_norm and gamma  < get_options().tol_norm)
    {
        return Optimal; // OK : optimal solution found
    }
    else if (norm_rho > get_options().threshold_infeasible)
    {
        return PrimalInfeasible;
    }
    else if (norm_sigma > get_options().threshold_infeasible)
    {
        return DualInfeasible;
    }

    // Update µ
    // --------
    m_mu = get_options().delta * gamma/(get_nb_constraints() + get_nb_variables());

    // Compute elimination variables
    // -----------------------------
    const vector_t gamma_f = m_mu*sv_v.cwiseInverse() - sv_f;
    const vector_t gamma_q = m_mu*sv_p.cwiseInverse() - sv_q;
    const vector_t gamma_s = m_mu*sv_t.cwiseInverse() - sv_s;
    const vector_t gamma_h = m_mu*sv_g.cwiseInverse() - sv_h;

    const vector_t tau = m_pb->upper_bounds_variables() - x - sv_t - (
                sv_t.array()/sv_s.array()).matrix().asDiagonal()*gamma_s;
    const vector_t nu  = -m_pb->lower_bounds_variables() + x - sv_g - (
                sv_g.array()/sv_h.array()).matrix().asDiagonal()*gamma_h;
    const vector_t alpha = m_pb->upper_bounds_constraints() - m_pb->lower_bounds_constraints() - sv_f - sv_p - (
                sv_p.array()/sv_q.array()).matrix().asDiagonal()*gamma_q;
    const vector_t beta = -sv_y - sv_q + sv_v + (sv_v.array()/sv_f.array()).matrix().asDiagonal()*gamma_f;

    const vector_t d =  (sv_s.array()/sv_t.array()).matrix() + (sv_h.array()/sv_g.array()).matrix();
    const vector_t e =  (sv_v.array()/sv_f.array()).matrix() + (sv_q.array()/sv_p.array()).matrix();

    // Assemble the system
    // ===================
    const index_t n = get_nb_variables();
    const index_t m = get_nb_constraints();

    for (index_t i=0; i<m; ++i)
    {
        m_matrix.coeffRef(i, i) = -1/e.coeff(i);
    }
    for (index_t j=0; j<n; ++j)
    {
        m_matrix.coeffRef(j+m, j+m) = d.coeff(j);
    }

    vector_t rhs(n+m);
    rhs.block(0,0,m,1) = (m_rho - e.asDiagonal().inverse()*(
                              beta + (sv_q.array()/sv_p.array()).matrix().cwiseProduct(alpha)));
    rhs.block(m,0,n,1)= m_sigma + (  (sv_s.array()/sv_t.array()).matrix().cwiseProduct(tau)
                                   - (sv_h.array()/sv_g.array()).matrix().cwiseProduct(nu) );

    // Solve the system
    // ================
    const vector_t solution = solve_linear_system(rhs);

    // Update
    // ======

    // Compute delta
    // -------------
    const auto dy = solution.block(0,0,m,1);
    const auto dx = solution.block(m,0,n,1);
    const vector_t delta_f  = e.asDiagonal().inverse()*(beta + (sv_q.array()/sv_p.array()).matrix().cwiseProduct(alpha) - dy);
    const vector_t delta_s  = - (sv_s.array()/sv_t.array()).matrix().cwiseProduct(tau - dx);
    const vector_t delta_h  = - (sv_h.array()/sv_g.array()).matrix().cwiseProduct(nu + dx);
    const vector_t delta_q  = - (sv_q.array()/sv_p.array()).matrix().cwiseProduct(alpha -delta_f);
    const vector_t delta_v  = (sv_v.array()/sv_f.array()).matrix().cwiseProduct(gamma_f - delta_f);
    const vector_t delta_p  = (sv_p.array()/sv_q.array()).matrix().cwiseProduct(gamma_q - delta_q);
    const vector_t delta_g  = (sv_g.array()/sv_h.array()).matrix().cwiseProduct(gamma_h - delta_h);
    const vector_t delta_t  = (sv_t.array()/sv_s.array()).matrix().cwiseProduct(gamma_s - delta_s);

    // compute theta
    // -------------
    double theta = std::max({
                      (-delta_f.array()/sv_f.array()).maxCoeff(),
                      (-delta_p.array()/sv_p.array()).maxCoeff(),
                      (-delta_t.array()/sv_t.array()).maxCoeff(),
                      (-delta_g.array()/sv_g.array()).maxCoeff(),
                      (-delta_v.array()/sv_v.array()).maxCoeff(),
                      (-delta_q.array()/sv_q.array()).maxCoeff(),
                      (-delta_s.array()/sv_s.array()).maxCoeff(),
                     (-delta_h.array()/sv_h.array()).maxCoeff(),
             });
    theta = std::min(get_options().underrelax_factor/theta, 1.0);

    // Update
    // -------
    x += theta*dx;
    sv_y += theta*dy;
    sv_f += theta*delta_f;
    sv_g += theta*delta_g;
    sv_h += theta*delta_h;
    sv_p += theta*delta_p;
    sv_q += theta*delta_q;
    sv_s += theta*delta_s;
    sv_v += theta*delta_v;
    sv_t += theta*delta_t;

    return NotConvergedYet; // default
}

// Solve the linear problem
// ========================
ReturnCodeIntPtLP InteriorPointLP::solve(vector_t& x)
{
    initialization();
    int cnt =0;
    ReturnCodeIntPtLP retcode;
    do
    {
        ++cnt;
        retcode = one_iteration(x);
    } while (retcode == NotConvergedYet and cnt < get_options().max_iter);
    if (cnt == get_options().max_iter) { retcode = MaxIterReached; } // no solution found
    return retcode;
}

// Solve the linear system assembled during an iteration
// =====================================================
vector_t InteriorPointLP::solve_linear_system(vector_t& rhs)
{
    m_solver.factorize(selfadjoint_t(m_matrix)); // selfadjoint_t is important since we only built upper half of the matrix
    return m_solver.solve(rhs);
}

} // end namespace intpt
} // end namespace dfp
