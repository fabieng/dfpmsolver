#include <dfpmsolver/nesolver.hpp>
#include <iostream>
#include <float.h>

namespace dfpmsolver {


double cond_estimates(matrix_t& M, vector_t& M2)
{
    int n = M.rows();
    assert(M2.rows() == n);
    // algoA3.3.1
    vector_t pm(n);
    vector_t p(n);
    vector_t x(n);

    double est = std::abs(M2.coeff(0));
    for (int j=1; j<n;++j)
    {
        double temp = std::abs(M2(j));
        for (int i=0;i<j;++i)
        {
            temp =  std::abs(M.coeff(i, j));
            //Zstd::cout << std::abs(M.coeff(i, j)) << std::endl;
        }
        est = std::max(est, temp);
    }
    x(0) = 1.0/M2(0);
    for (int i=1;i<n;++i)
    {
        p(i) = M.coeff(1, i)*x(0);
    }
    for (int j=1; j<n; ++j)
    {
        double xp = (1-p.coeff(j))/M2.coeff(j);
        double xm = (-1-p.coeff(j))/M2.coeff(j);
        double temp = std::abs(xp);
        double tempm = std::abs(xm);
        for (int i=j+1; i<n; ++i)
        {
            pm.coeffRef(i) = p.coeff(i) + M.coeff(j, i)*xm;
            tempm += (std::abs(pm.coeff(i)) / std::abs(M2.coeff(i)));
            p.coeffRef(i) += M.coeff(j, i)*xp;
            temp += (std::abs(p.coeff(i)) / std::abs(M2.coeff(i)));
        }
        if (temp >= tempm)
        {
            x.coeffRef(j) = xp;
        }
        else
        {
            x.coeffRef(j) = xm;
            for (int i=j+1; i<n; ++i) p.coeffRef(i) = pm.coeff(i);
        }
        double xnorm = x.cwiseAbs().sum();
        est = est / xnorm;
        rsolve(M, M2, x);
        xnorm = x.cwiseAbs().sum();
        est = est * xnorm;
    }
    return est;
}


bool qr_decomposition(matrix_t& M, vector_t& M1, vector_t& M2)
{
    int n = M.rows();
    // alqo A3.2.1 Dennis1983
    bool sing(false);
    for (int k=0; k<n-1; ++k)
    {
        double eta = std::abs(M.coeff(k, k));
        for (int i=k+1; i<n; ++i)
        {
            if (std::abs(M.coeff(i, k)) > eta) eta = std::abs(M.coeff(i, k));
        }
        //std::cout << "eta " << eta <<std::endl;
        if (eta == 0.0)
        { // matrix singular
            M1(k) = 0.0;
            M2(k) = 0.0;
            sing = true;
        }
        else
        {
            M.block(k, k, n-k, 1) /= eta;
            double sigma = M.block(k, k, n-k, 1).norm();
            //std::cout << "Youplaboum : " << M.coeff(k, k) << std::endl;
            sigma = std::copysign(sigma, M.coeff(k, k));
            M.coeffRef(k, k) += sigma;
            M1.coeffRef(k) = sigma * M.coeff(k, k);
            M2.coeffRef(k) = -eta * sigma;
            for (int j=k+1;j<n;++j)
            {
                double tau = 0;
                for (int i=k; i<n; ++i)
                {
                    tau += M.coeff(i, k)*M.coeff(i, j);
                }
                tau /= M1.coeff(k);
                for (int i=k;i<n;++i)
                {
                    M.coeffRef(i, j) -= tau*M.coeff(i, k);
                }
            }
        }
    }
    if (M.coeff(n-1, n-1) == 0) sing = true;
    M2.coeffRef(n-1) = M.coeff(n-1, n-1);
    return sing;
}

void qr_solve(matrix_t& M, vector_t& M1, vector_t& M2, vector_t& b)
{
    int n = M.rows();
    assert(M.cols() == n);
    assert(M1.rows() == n);
    assert(M2.rows() == n);
    for (int j=0; j<n-1; ++j)
    {
        double tau=0;
        for (int i=j; i<n; ++i)
        {
            tau += M.coeff(i,j)*b.coeff(i)/M1.coeff(j);
        }
        for (int i=j; i<n; ++i)
        {
            b.coeffRef(i) += -tau * M.coeff(i, j);
        }
    }
    rsolve(M, M2, b);
}

void rsolve(matrix_t& M, vector_t& M2, vector_t& b)
{
    int n = M.cols();
    assert(M.rows() == n);
    // algo A.3.2.2a
    b.coeffRef(n-1) /= M2.coeff(n-1);
    for (int i=n-2;i>-1;--i)
    {
        double tmp = b.coeff(i);
        for (int j=i+1; j<n; ++j)
        {
            tmp -= M.coeff(i, j)*b.coeff(j);
        }
        b.coeffRef(i) = tmp/M2.coeff(i);
    }
}

double cholesky_decomposition(matrix_t& H, double maxoffl, matrix_t& L)
{
    int n = H.cols();
    assert(H.rows() == n);
    assert(L.rows() == n);
    assert(L.cols() == n);
    double minl = std::pow(DBL_EPSILON, 0.25)*maxoffl;
    if (maxoffl == 0)
    {
        maxoffl = std::sqrt(H.diagonal().cwiseAbs().maxCoeff());
    }
    double minl2 =  std::sqrt(DBL_EPSILON)*maxoffl;
    double maxadd = 0;
    for (int j=0;j<n;++j)
    {
        L.coeffRef(j, j) = H.coeff(j, j);
        for (int i=0; i<j; ++i)
        {
            L.coeffRef(j, j) -= L.coeff(j, i)*L.coeff(j, i);
        }
        double minljj = 0;
        for (int i=j+1; i<n; ++i)
        {
            L.coeffRef(i, j) = H.coeff(j, i);
            for (int k=0; k<j; ++k)
            {
                L.coeffRef(i, j) -= L.coeff(i, k)*L.coeff(j, k);
            }
            minljj = std::max(std::abs(L.coeff(i, j)), minljj);
        }
        minljj = std::max(minljj/maxoffl, minl);
        if (L.coeff(j, j) > minljj*minljj)
        {   // normal cholesky
            L.coeffRef(j, j) = std::sqrt(L.coeff(j,j));
        }
        else
        {   // h += eps
            if (minljj < minl2) minljj = minl2;
            maxadd = std::max(maxadd, minljj*minljj-L.coeff(j,j));
            L.coeffRef(j,j) = minljj;
        }
        for (int i=j+1;i<n;++i)
        {
            L.coeffRef(i,j) /= L.coeff(j, j);
        }
    }
    return maxadd;
}

void cholesky_solve(matrix_t& L, vector_t& g, vector_t& s)
{
    // solve (LLT)s = -g
    // algo A3.2.3
    int n = L.cols();
    assert(n == L.rows());
    chol_lsolve(L, g, s);
    chol_ltsolve(L, s, s);
    s = -s;
}

void chol_lsolve(matrix_t& L, vector_t& g, vector_t& s)
{
    int n = L.cols();
    assert(n == L.rows());
    s.coeffRef(0) = g.coeff(0) / L.coeff(0, 0);
    for (int i=1; i<n; ++i)
    {
        double tmp = g.coeff(i);
        for (int j=0; j<i; ++j)
        {
            tmp -= L.coeff(i,j)*s(j);
        }
        s.coeffRef(i) = tmp / L.coeff(i, i);
    }
}

void chol_ltsolve(matrix_t& L, vector_t& y, vector_t& x)
{
    int n = L.cols();
    assert(n == L.rows());
    x.coeffRef(n-1) = y.coeff(n-1) / L.coeff(n-1, n-1);
    for (int i=n-2; i>-1; --i)
    {
        double tmp = y.coeff(i);
        for (int j=i+1; j<n; ++j)
        {
            tmp -= L.coeff(j,i)*x(j);
        }
        x.coeffRef(i) = tmp / L.coeff(i, i);
    }
}



double NESolver::squared_residual(const vector_t& x) {
    compute_residual(x);
    return 0.5 * (m_scaling_f.asDiagonal() * m_residuals).squaredNorm();
}

void NESolver::finite_difference_jacobian(vector_t& solution,
                                          matrix_t& jacobian)
{
    vector_t perturbed_res(get_neq());
    vector_t& residual = get_residual_ref();
    double sqrteta = std::sqrt(get_options().eta);
    for (int j=0; j<get_neq(); ++j)
    {
        double h = sqrteta* std::max(std::abs(solution.coeff(j)), 1.0/scaling_x(j));
        h = std::copysign(h, solution.coeff(j));
        if (h ==0) h=sqrteta;
        double tmp = solution.coeff(j);
        solution(j) += h;
        h = solution.coeff(j) - tmp;
        compute_residual(solution, perturbed_res);
        for (int i=0; i<get_neq(); ++i)
        {
            jacobian.coeffRef(i, j) = (perturbed_res.coeff(i) - residual.coeff(i))/h;
        }
        solution.coeffRef(j) = tmp;
    }
    return;
}


int NESolver::newton_model(vector_t& update, matrix_t& M, vector_t& M2, matrix_t& H, matrix_t& L)
{
    vector_t M1(get_neq());
    M =  m_scaling_f.asDiagonal() * m_jacobian;

    double sqrtmacheps = std::sqrt(DBL_EPSILON);

    bool sing = qr_decomposition(M, M1, M2);
    double est = 0;
    if (not sing)
    {
        // estimate condition number
        // R <- R.D_x^-1
        for (int j=0; j<get_neq(); ++j)
        {
            for (int i=0; i<j; ++i)
            {
                M.coeffRef(i, j) /= scaling_x(j);
            }
            M2.coeffRef(j) /= scaling_x(j);
        }
        est = cond_estimates(M, M2);
    }
    if (sing or est > 1.0/sqrtmacheps)
    {
        H = m_jacobian.transpose() * m_scaling_f.cwiseProduct(m_scaling_f).asDiagonal() * m_jacobian;
        // compute hnorm
        double hnorm = 0;
        for (int j=0; j<get_neq(); ++j)
        {
            hnorm += std::abs(H.coeff(0, j))/scaling_x(j);
        }
        hnorm /= scaling_x(0);
        for (int i=1;i<get_neq();++i)
        {
            double tmp =0;
            for (int j=0; j<i+1; ++j)
            {
                tmp += std::abs(H.coeff(j, i))/scaling_x(j);
            }
            for (int j=i+1; j<get_neq(); ++j)
            {
                tmp +=  std::abs(H.coeff(i, j))/scaling_x(j);
            }
            tmp /= scaling_x(i);
            hnorm = std::max(hnorm, tmp);
        }
        for (int i=0; i<get_neq(); ++i)
        {
            H.coeffRef(i, i) += std::sqrt(get_neq())*sqrtmacheps*hnorm*scaling_x(i)*scaling_x(i);
        }
        // Cholesky decomposition
        cholesky_decomposition(H, 0, L);
        cholesky_solve(L, m_gradient, update);
    }
    else
    { // normal Newton step
        // reset R
        for (int j=0; j<get_neq(); ++j)
        {
            for (int i=1; i<j; ++i)
            {
                M.coeffRef(i, j) *= scaling_x(j);
            }
            M2.coeffRef(j) *= scaling_x(j);
        }
        update = m_scaling_f.asDiagonal() * (-m_residuals);
        qr_solve(M, M1, M2, update);

        if (get_options().global == HookTrust)
        {
            for (int i=0; i<get_neq(); ++i)
            {
                M.coeffRef(i, i) = M2.coeff(i);
                for (int j=0; j<i; ++j)
                {
                    M.coeffRef(i, j) = M.coeff(j, i);
                }
            }
            for (int i=0; i<get_neq(); ++i)
            {
                double sum =0;
                for (int k=0; k<i+1; ++k) sum+= M.coeff(i,k)*M.coeff(i, k); //FIXME
                H.coeffRef(i, i) = sum;
                for (int j=0;j<get_neq(); ++j)
                {
                    sum = 0;
                    for (int k=0; k<i+1; ++k) sum+= M.coeff(i,k)*M.coeff(j, k); //FIXME
                    H.coeffRef(i, j) = sum;
                }
            }
        }
    }
    return 1;
}

int NESolver::newton_driver(vector_t& solution)
{
    HookStepParam param;
    if (get_options().global == HookTrust)
    {
        param.s = vector_t(get_neq());
        param.delta = -1;
        param.delta_prev = 0.0;
        param.phi = 0.0;
        param.phiprime = 0.0;
        param.phiprimeinit = 0.0;
    }
    double fc = squared_residual(solution);
    compute_jacobian(solution);
    compute_gradient(solution);
    //std::cout << solution(0) << "\t" << solution(1) << "\n ";
    int cnt = 0;
    int termcode = NoConvergenceYet;
    while (termcode == NoConvergenceYet)
    {
        matrix_t M(matrix_t::Zero(get_neq(), get_neq()));
        vector_t M2(vector_t::Zero(get_neq()));
        matrix_t H(matrix_t::Zero(get_neq(), get_neq()));
        matrix_t L(matrix_t::Zero(get_neq(), get_neq()));
        vector_t update(vector_t::Zero(get_neq()));

        cnt += 1;
        //std::cout << "Entering iteration : " << cnt << std::endl;
        newton_model(update, M, M2, H, L);
        //std::cout << update << std::endl;
        // Global method
        //solution += update;
        //int retcode = linesearch(update, solution, fc);
        int retcode =0;
        if (get_options().global == NoGlobal)
        {
            double newtlen = (m_scaling_x.asDiagonal()*update).norm();
            if (newtlen > get_options().maxstep )
            {
                update = update*get_options().maxstep/newtlen;
                newtlen = get_options().maxstep;
                m_max_taken = true;
            }
            solution += update;
        }
        if (get_options().global == HookTrust)
        {
            retcode = hook_driver(cnt, param,
                                      fc,
                                      update,
                                      solution,
                                      H, M);
        }
        else if (get_options().global == LineSearch)
        {
            retcode = linesearch(update, solution, fc);
        }
        //std::cout << solution << std::endl;
        //
        fc = squared_residual(solution);
        compute_jacobian(solution);
        compute_gradient(solution);
        termcode = check_convergence(cnt, retcode, update, solution);

        //
        //std::cout << "Solution \n ----------\n" << solution << "\n ------- \n ";

        //std::cout << solution(0) << "\t" << solution(1) << "\n ";
    }
    nb_iter = cnt;
    return termcode;
}


int NESolver::linesearch(vector_t& p, vector_t& x, double fc)
{
    m_max_taken = false;
    int retcode = 2;
    const double alpha = 1e-4;
    double newtlen = (m_scaling_x.asDiagonal()*p).norm();
    if (newtlen > get_options().maxstep )
    {
        p = p*get_options().maxstep/newtlen;
        newtlen = get_options().maxstep;
    }
    double init_slope = m_gradient.dot(p);
    double rellength = std::abs(p(0))/(std::max(std::abs(x(0)), 1.0/scaling_x(0)));
    for (int i=1; i<get_neq(); ++i)
    {
        rellength = std::max(rellength, std::abs(p(i))/(std::max(std::abs(x(i)), 1.0/scaling_x(i))));
    }
    double minlambda = get_options().steptol / rellength;
    double lambda = 1.0;
    double lambda_prev = 1.0;

    vector_t xp(get_neq());
    double fcp;
    double fcp_prev;
    do
    {
        xp = x + lambda*p;   
        fcp = squared_residual(xp);
        if (fcp <= fc + alpha*lambda*init_slope)
        {
            retcode = 0;
            if (lambda ==1 and (newtlen > 0.99 * get_options().maxstep)) {
                m_max_taken = true;
            }
            break;
        }
        else if (lambda < minlambda)
        {
            retcode = 1;
            break;
        }
        else
        {
            double lambdatmp;
            if (lambda == 1.0) {
                lambdatmp = - init_slope / (2*(fcp - fc -init_slope));
            }
            else
            {
                const double factor = 1 /(lambda - lambda_prev);
                const double x1 = fcp - fc - lambda*init_slope;
                const double x2 = fcp_prev - fc - lambda_prev*init_slope;

                const double a = factor * ( x1/(lambda*lambda) - x2/(lambda_prev*lambda_prev));
                const double b = factor * ( -x1*lambda_prev/(lambda*lambda) + x2*lambda/(lambda_prev*lambda_prev));

                if (a == 0)
                { // cubic is quadratic
                    lambdatmp = - init_slope/(2*b);
                }
                else
                {
                    const double disc = b*b-3*a*init_slope;
                    lambdatmp = (-b+std::sqrt(disc))/(3*a);
                }
                if (lambdatmp > 0.5 ) lambdatmp = 0.5*lambda;
            }
            lambda_prev = lambda;
            fcp_prev = fcp;
            if (lambdatmp < 0.1*lambda) {
                lambda = 0.1 * lambda;
            } else {
                lambda = lambdatmp;
            }
        }
    } while(retcode < 2);
    x = xp;
    p = lambda*p;
    return retcode;

}


NewtonReturnState NESolver::check_convergence(int itncount, int retcode, vector_t& update, vector_t& solution)
{
    NewtonReturnState termcode = NoConvergenceYet;
    if (retcode==1)
    {
        termcode = FailedGlobalMethod;
    }
    else if (norm_inf_residual() < get_options().fvectol)
    {
        termcode = ResidualMinimized;
    }
    else if (norm_inf_update(update, solution) < get_options().steptol)
    {
        termcode = ErrorMinimized;
    }
    else if (itncount >  get_options().max_iter)
    {
        termcode = MaximumNumberOfIterations;
    }
    else if (m_max_taken)
    {
        ++m_consec_max;
        if (m_consec_max == get_options().maxiter_maxstep) {
            termcode = MaximumNewtonLength;
        }
    }
    else
    {
        m_consec_max = 0;
    }
    return termcode;
}

int NESolver::hook_driver(int itncount,
                          HookStepParam& param,
                          double fc,
                          vector_t& update,
                          vector_t& solution,
                          matrix_t& H,
                          matrix_t& L)
{
    // algo #6.4.1
    param.s = update;
    param.retcode = 4;
    param.firsthook = true;
    param.newtlen = norm_2_solution(update);
    //std::cout << "Newton length : " << param.newtlen << std::endl;
    param.xc = solution;

     // initialize driver
     if (itncount == 1 or param.delta == -1) {
         param.mu = 0;
         if (param.delta == -1)
         {
            double alpha = (m_scaling_x.asDiagonal().inverse() * m_gradient).squaredNorm();
            double beta = 0;
            for (int i =0; i<get_neq(); ++i)
            {
                double temp = 0;
                for (int j=i; j<get_neq(); ++j)
                {
                    temp += L(j, i)*m_gradient(j)/(scaling_x(j)*scaling_x(j));
                }
                beta += temp*temp;
            }
            param.delta = alpha*sqrt(alpha)/beta;
            if (param.delta > get_options().maxstep) param.delta = get_options().maxstep;
            //std::cout << "Init delta : " << param.delta << std::endl;
         }
     }
     do
     {
         hook_step(param, update, H, L);
         param.delta_prev = param.delta;
         trust_region_update(1, param, solution, fc, H);
         //std::cout << "Retcode : " << param.retcode << std::endl;
         //std::cout << "Delta : "<< param.delta << std::endl;
     } while (param.retcode > 2);
    return param.retcode;
}

void NESolver::hook_step(HookStepParam& param, vector_t& update, matrix_t& H, matrix_t &L)
{
    // algo 6.4.2
    double hi = 1.5;
    double lo = -.75;

    if (param.newtlen <= hi*param.delta)
    {
        param.newttaken = true;
        param.s = update;
        param.mu = 0;
        param.delta = std::min(param.delta, param.newtlen);
        return;
    }
    else
    {
        vector_t tempvec;
        param.newttaken = false;
        if (param.mu > 0)
        {
            param.mu = param.mu - ((param.phi + param.delta_prev)/param.delta) *
                    (((param.delta_prev - param.delta) + param.phi)/ param.phiprime);
        }
        param.phi = param.newtlen - param.delta;
        if (param.firsthook)
        {
            param.firsthook = false;
            tempvec = (m_scaling_x.cwiseProduct(m_scaling_x)).asDiagonal()*update;
            chol_lsolve(L, tempvec, tempvec);
            param.phiprimeinit = - tempvec.squaredNorm() / param.newtlen;
            //std::cout << "phi prim init " <<  param.phiprimeinit << std::endl;
        }
        double mulow = -param.phi / param.phiprimeinit;
        double muup  = (m_scaling_x.asDiagonal().inverse() * m_gradient).norm()/param.delta;
        std::cout << "mu_low :" << mulow << " # mu_up : " << muup << std::endl;
        bool done = false;
        do
        {
            if ((param.mu < mulow) or (param.mu > muup)) {
                param.mu = std::max(std::sqrt(mulow*muup), 1e-3*muup);
            }
            H += param.mu*m_scaling_x.cwiseProduct(m_scaling_x).asDiagonal();
            //std::cout << " mu : " << param.mu << std::endl;
            cholesky_decomposition(H, 0, L);
            cholesky_solve(L, m_gradient, param.s);
            H -=  param.mu*m_scaling_x.cwiseProduct(m_scaling_x).asDiagonal();
            double steplen = norm_2_solution(m_scaling_x.asDiagonal()*param.s);
            //std::cout << "Step len : " << steplen << std::endl;
            param.phi = steplen - param.delta;
            tempvec = m_scaling_x.cwiseProduct(m_scaling_x).asDiagonal()*param.s;
            chol_lsolve(L, tempvec, tempvec);
            param.phiprime = - tempvec.squaredNorm() / steplen;
            if (((steplen >= lo*param.delta) and (steplen <= hi*param.delta)) or (muup-mulow <= 0))
            {
                done = true;
            }
            else
            {
                mulow = std::max(mulow, param.mu - (param.phi/param.phiprime));
                if (param.phi < 0) muup = param.mu;
                param.mu =param. mu - (steplen/param.delta)*(param.phi/param.phiprime);
            }
        } while (not done);

        return;
    }
}

void NESolver::trust_region_update(int steptype, HookStepParam& param, vector_t& solution, double fc, matrix_t& H)
{
    // algo 6.4.5
    m_max_taken = false;
    const double alpha = 1e-4;
    double steplen = (m_scaling_x.asDiagonal()*param.s).norm();
    // update
    solution = param.xc + param.s;
    double fcp = squared_residual(solution);
    double delta_f = fcp  -fc;
    double init_slope = m_gradient.dot(param.s);
    if (param.retcode != 3) param.fcp_prev = 0;
    if ((param.retcode == 3 ) and ((fcp >= param.fcp_prev ) or (delta_f > alpha*init_slope)))
    {
        param.retcode = 0;
        solution = param.x_prev;
        fcp = param.fcp_prev;
        param.delta = param.delta/2.0;
        compute_residual(solution);
        return;
        // good
    }
    else if (delta_f >= alpha*init_slope)
    {
        double rellength = norm_inf_update(param.s, solution);
        if (rellength < get_options().steptol)
        { // step too small, EXIT_FAILURE
            param.retcode = 1;
            return;
        }
        else
        { // we reduce delta
            param.retcode = 2;
            double delta_temp = -init_slope*steplen/(2*(delta_f - init_slope));
            if (delta_temp < 0.1*param.delta) param.delta = 0.1*param.delta;
            else if (delta_temp > 0.5*param.delta) param.delta = 0.5*param.delta;
            else param.delta = delta_temp;
            return;
        }
    }
    else
    {
        double delta_f_pred = init_slope;
        if (steptype == 1)
        {   // hookstep
            delta_f_pred += 0.5 * param.s.transpose() * H * param.s;
        }
        // FIXME dogleg step
        if (     (param.retcode != 2)
             and (   (std::abs(delta_f_pred - delta_f) <= 0.1*std::abs(delta_f))
                  or (delta_f <= init_slope))
             and (not param.newttaken)
             and (param.delta <= 0.99*get_options().maxstep)
            )
        {
            param.retcode = 3;
            param.x_prev = solution;
            param.fcp_prev = fcp;
            param.delta = std::min(2*param.delta, get_options().maxstep);
            // FIXME FV+_prev
            return;
        }
        else
        {   // accept solution
            param.retcode = 0;
            if (steplen > 0.99*get_options().maxstep) m_max_taken = true;
            if (delta_f >= 0.1*delta_f_pred) {
                param.delta = param.delta / 2;
            }
            else if (delta_f <= 0.75 * delta_f_pred) {
                param.delta = std::min(2*param.delta, get_options().maxstep);
            }
            return;
        }
    }
}

} // end namespace dfpmsolver
