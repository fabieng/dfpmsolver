/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : newton_dense.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include "dfpmsolver/newton_dense.hpp"

#include <iostream>

namespace dfpmsolver {

int NewtonDriver::solve_linear_system(const VectorT& residual,
                                      Eigen::MatrixXd& jacobian,
                                      VectorT& update)
{
    //Eigen::ColPivHouseholderQR<Eigen::MatrixXd> solver = jacobian.colPivHouseholderQr();
    Eigen::FullPivHouseholderQR<Eigen::MatrixXd> solver = jacobian.fullPivHouseholderQr();

    if (not solver.isInvertible()) {return 2;}
    update = solver.solve(residual);
    //std::cout << "update : " << update << std::endl;
    const double relative_error = norm_2(jacobian*update - residual) / norm_2(residual);
    if (relative_error > std::sqrt(m_tolerance.machine_epsilon))// *100000)
        return 1;
    else
        return 0;
}


void NewtonDriver::finite_difference_jacobian(VectorT& solution,
                                VectorT& residual,
                                DenseMatrixT& jacobian)
{
    VectorT perturbed_res(neq);

    for (int j=0; j<neq; ++j)
    {
        double h = m_tolerance.eps_j * std::max(std::abs(solution(j)), m_typx.coeff(j));
        h = std::copysign(h, solution.coeff(j));
        double tmp = solution.coeff(j);
        solution(j) += h;
        h = solution.coeff(j) - tmp;
        compute_residual(solution, perturbed_res);
        for (int i=0; i<neq; ++i)
        {
            jacobian.coeffRef(i, j) = (perturbed_res.coeff(i) - residual.coeff(i))/h;
        }
        solution.coeffRef(j) = tmp;
    }
    return;
}

} // end namespace dfpmsolver
