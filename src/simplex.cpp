#include "dfpmsolver/simplex.hpp"
#include <iostream>

namespace dfpmsolver {


int SimplexSolver::run_iteration()
{
    Eigen::VectorXd center(Eigen::VectorXd::Zero(m_dim));
    for (int i =0; i<m_dim+1; ++i)
    {
        if (i == m_worst) continue;
        center = center + m_simplex.row(i).transpose().matrix();
    }
    center /= m_dim;

    // 2: Reflected vertex
    Eigen::VectorXd test(m_dim);
    double res_test;
    Eigen::VectorXd reflect(m_dim);
    double res_reflect;
    test = center + m_alpha * ( center - m_simplex.row(m_worst).transpose().matrix() );
    reflect = test;

    // 3: is reflected better than best ?
    res_test = compute_residual(test);
    res_reflect = res_test;

    if (res_reflect < m_residuals(m_best))
    {
        // 4: Expanded vertex
        test = center + m_gamma * ( center - m_simplex.row(m_worst).transpose().matrix());
        // 5: compare residuals
        res_test = compute_residual(test);

        if (res_test < m_residuals(m_best))
        {
            // 6: accept expanded
            m_residuals(m_worst) = res_test;
            m_simplex.row(m_worst) = test;
        }
        else
        {
            // 7: accept reflected
            m_residuals(m_worst) = res_reflect;
            m_simplex.row(m_worst) = reflect;
        }
     }
     // 8 : is reflected better than worst ?
     if (res_reflect < m_residuals(m_worst))
     {
        // 7: accept reflected
        m_residuals(m_worst) = res_reflect;
        m_simplex.row(m_worst) = reflect;
     }
     else
     {
         // 9: find contracted
         test = center - m_beta * (center - m_simplex.row(m_worst).transpose().matrix());
         //10: compare residuals
         res_test = compute_residual(test);
         if (res_test < m_residuals(m_worst))
         {
             // 11: accept expanded
             m_residuals(m_worst) = res_test;
             m_simplex.row(m_worst) = test;
         }
         else
         {
             // 12: collapse all but best vector
             for (int vertex=0; vertex<m_dim+1;++vertex)
             {
                 if (vertex == m_best) continue;
                 m_simplex.row(vertex) = m_simplex.row(m_best) - m_beta * (m_simplex.row(m_best) - m_simplex.row(vertex));
                 m_residuals(vertex) = compute_residual(m_simplex.row(vertex).transpose());
             }
         }
     }

    sort();
    return 0;
}

void SimplexSolver::sort()
{
    int best = 1;
    int worst = 1;
    for (int i = 2; i<m_dim+1; ++i)
    {
        if (m_residuals(i) > m_residuals(worst)) worst = i;
        else if (m_residuals(i) < m_residuals(best)) best = i;
    }
    m_best = best;
    m_worst = worst;
}

int SimplexSolver::solve()
{
    m_old = Eigen::ArrayXXd::Zero(m_dim+1,m_dim);
    sort(); // just to be sure
    int cnt =0;
    do
    {
        run_iteration();
        ++cnt;
    } while (not check_convergence() and cnt < 200);

    return cnt;
}

bool SimplexSolver::check_convergence()
{
   // if ((m_bestv_old - m_simplex.row(m_best).transpose().matrix()).maxCoeff() < 1e-10) return true;
    //m_bestv_old = m_simplex.row(m_best).transpose();

    if ((m_old - m_simplex).abs().sum() < 1e-6) return true;
    m_old = m_simplex;

    if (m_residuals(m_best) < 1e-10) return true;

    return false;
}

} // end namespace dfpmsolver
