/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : tests/aluminium_speciation.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include <iostream>
#include <functional>
#include "dfpmsolver/newton_dense.hpp"
#include "dfpmsolver/simplex.hpp"
#include "test.h"

//#include <unsupported/Eigen/LevenbergMarquardt>

using namespace dfpmsolver;


// ################ Simple Aluminium speciation -fixed pH #######################

void residual_simple_log_aluminium_speciation(const VectorT& solution, VectorT& residual)
{

    residual(0) = ( std::pow(10.0, solution.coeff(0))
                  + std::pow(10.0, solution.coeff(1))
                  + std::pow(10.0, solution.coeff(2))
                  + std::pow(10.0, solution.coeff(3))
                  + std::pow(10.0, solution.coeff(4))
                  - 1e-7);
    residual(1) =  5.0 + solution.coeff(1) -   4 - solution.coeff(0);
    residual(2) = 10.1 + solution.coeff(2) - 2*4 - solution.coeff(0);
    residual(3) = 16.9 + solution.coeff(3) - 3*4 - solution.coeff(0);
    residual(4) = 22.7 + solution.coeff(4) - 4*4 - solution.coeff(0);
}

void test_simple_aluminium_speciation()
{
    int n = 5;
    std::cout << " ### Aluminium speciation - fixed pH ### " << std::endl;
    VectorT solution(n);
    for (int i=0; i<n; ++i) solution(i)= -15;
    solution(0) = -1;
    solution(1) = -1;
    solution(2) = -1;
    solution(3) = -1;
    solution(4) = -1;

    NewtonDriver driver(n, residual_simple_log_aluminium_speciation);
    //driver.set_linesearch(Strang);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    print_performance(std::cout, perf);

//    VectorT residual(n);
//    residual_simple_log_aluminium_speciation(solution, residual);
//    std::cout << residual << std::endl;
//    std::cout << "--------"
//              << std::endl
//              << solution
//              << std::endl;
    std::cout.flush();
    std::cerr.flush();
}



// ################ Aluminium speciation #######################

void residual_log_aluminium_speciation(const VectorT& solution, VectorT& residual)
{
    residual(0) = (std::pow(10.0, solution.coeff(0)) - std::pow(10.0, solution.coeff(1))
                  + 3*std::pow(10.0, solution.coeff(2)) + 2*std::pow(10.0, solution.coeff(3)) + std::pow(10.0, solution.coeff(4))
                  - std::pow(10.0, solution.coeff(6)));
    residual(1) = -14 - solution.coeff(0) - solution.coeff(1);
    residual(2) = (std::pow(10.0, solution.coeff(2)) + std::pow(10.0, solution.coeff(3)) + std::pow(10.0, solution.coeff(4))
                  + std::pow(10.0, solution.coeff(5)) + std::pow(10.0, solution.coeff(6)) - 1e-7);
    residual(3) =  5.0 + solution.coeff(3) +   solution.coeff(0) - solution.coeff(2);
    residual(4) = 10.1 + solution.coeff(4) + 2*solution.coeff(0) - solution.coeff(2);
    residual(5) = 16.9 + solution.coeff(5) + 3*solution.coeff(0) - solution.coeff(2);
    residual(6) = 22.7 + solution.coeff(6) + 4*solution.coeff(0) - solution.coeff(2);
}

void test_aluminium_speciation()
{
    int n = 7;
    std::cout << " ### Aluminium speciation ### " << std::endl;
    VectorT solution(n);
    solution(0) = -6.9;
    solution(1) = -7.1;
    solution(2) = -9;
    solution(3) = -5.0   + solution.coeff(0) - solution.coeff(2);
    solution(4) = -10.1  + 2*solution.coeff(0) - solution.coeff(2);
    solution(5) = -16.9  + 3*solution.coeff(0) - solution.coeff(2);
    solution(6) = -22.7  + 4*solution.coeff(0) - solution.coeff(2);

    NewtonDriver driver(n, residual_log_aluminium_speciation);
    driver.get_tolerance()->max_iterations = 500;
    driver.set_linesearch(Backtracking);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    print_performance(std::cout, perf);

//    VectorT residual(n);
//    residual_log_aluminium_speciation(solution, residual);
//    std::cout << residual << std::endl;
//    std::cout << "--------"
//              << std::endl
//              << solution
//              << std::endl;
    std::cout.flush();
    std::cerr.flush();
}

// #################### Aluminium speciation - basis #################


void residual_base_log_aluminium_speciation(const VectorT& solution, VectorT& residual)
{
    double oh    = std::pow(10.0, - 14.0 -   solution.coeff(0));
    double aloh  = std::pow(10.0, -  5.0 -   solution.coeff(0) + solution.coeff(1));
    double aloh2 = std::pow(10.0, - 10.1 - 2*solution.coeff(0) + solution.coeff(1));
    double aloh3 = std::pow(10.0, - 16.9 - 3*solution.coeff(0) + solution.coeff(1));
    double aloh4 = std::pow(10.0, - 22.7 - 4*solution.coeff(0) + solution.coeff(1));

    residual(0) = (std::pow(10.0, solution.coeff(0)) - oh
                  + 3*std::pow(10.0, solution.coeff(1)) + 2*aloh + aloh2 - aloh4);
    residual(1) = (std::pow(10.0, solution.coeff(1)) + aloh + aloh2
                  + aloh3 + aloh4 - 1e-6);
}

void test_basis_aluminium_speciation()
{
    int n = 2;
    std::cout << " ### Aluminium speciation - basis ### " << std::endl;
    VectorT solution(n);
    solution(0) = -7.0;
    solution(1) = -11.0;

    NewtonDriver driver(n, residual_base_log_aluminium_speciation);
    driver.get_tolerance()->max_iterations = 150;
    driver.set_linesearch(Strang);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    print_performance(std::cout, perf);

    std::cout << "--------"
              << std::endl
              << solution
              << std::endl;

}


// ################### convergence map ###############################3

struct Answer{
    double ph;
    double logAl;
    int iterations;
    double residual;
    int iterations_pcf;
};

void onerun_map_aluminium_speciation(Answer& answer)
{
    int n = 2;
    VectorT solution(n);
    solution(0) = - answer.ph;
    solution(1) = answer.logAl;

    NewtonDriver driver(n, residual_base_log_aluminium_speciation);
    driver.get_tolerance()->max_iterations = 150;
    driver.get_tolerance()->residual_tolerance = 1e-10;
    driver.set_linesearch(Strang);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    if (ret != IterationSuccess) {
        answer.iterations = -1;

        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);
    }
    else
    {
        answer.iterations = driver.get_performance()->nb_iterations;
        answer.ph = - solution(0);
        answer.logAl = solution(1);

        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);
    }
}

void map_convergence()
{
    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            onerun_map_aluminium_speciation(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }

        }
    }

 }

// ############3 with positive continuous fraction ######################33

int positive_continuous_fraction(VectorT& solution, double tol)
{
    double h     = std::pow(10.0, solution.coeff(0));
    double oh    = std::pow(10.0, - 14.0 -   solution.coeff(0));
    double al    = std::pow(10.0, solution.coeff(1));
    double aloh  = std::pow(10.0, -  5.0 -   solution.coeff(0) + solution.coeff(1));
    double aloh2 = std::pow(10.0, - 10.1 - 2*solution.coeff(0) + solution.coeff(1));
    double aloh3 = std::pow(10.0, - 16.9 - 3*solution.coeff(0) + solution.coeff(1));
    double aloh4 = std::pow(10.0, - 22.7 - 4*solution.coeff(0) + solution.coeff(1));
    double sum_reac_0;
    double sum_prod_0;

    double T_0 = -3e-6;
    if (T_0 >= 0)
    {
        sum_reac_0 = h;
        sum_prod_0 = T_0 + aloh + 2*aloh2 + 3*aloh3 + 4*aloh4 + oh;

        //sum_reac_0 = h + al + 2*aloh + aloh2;
        //sum_prod_0 = aloh4 + oh;
    }
    else
    {
        sum_reac_0 = -T_0 + h ;
        sum_prod_0 =  aloh + 2*aloh2 + 3*aloh3 + 4*aloh4 + oh;
    }

    double sum_reac_1;
    double sum_prod_1;
    double T_1 = 1e-6;
    if (T_1 >= 0)
    {
        sum_reac_1 = al + aloh + aloh2 + aloh3 + aloh4;
        sum_prod_1 = T_1;
    }
    else
    {
        sum_reac_1 = -T_1 + al + aloh + aloh2 + aloh3 + aloh4;
        sum_prod_1 = 0;
    }

    double theta_0;
    double theta_1;

    if ( sum_reac_0 > sum_prod_0)
    {
        theta_0 = 0.9 - (sum_prod_0/sum_reac_0)*0.8;
    }
    else if (sum_reac_0 < sum_prod_0)
    {
        theta_0 = 0.9 - (sum_reac_0/sum_prod_0)*0.8;
    }
    if ( sum_reac_1 > sum_prod_1)
    {
        theta_1 = 0.9 - (sum_prod_1/sum_reac_1)*0.8;
    }
    else if (sum_reac_1 < sum_prod_1)
    {
        theta_1 = 0.9 - (sum_reac_1/sum_prod_1)*0.8;
    }
    solution(0) = std::log10(h * ( 1 + theta_0 * (sum_prod_0/sum_reac_0 - 1)));
    solution(1) = std::log10(al * ( 1 + theta_1 * (sum_prod_1/sum_reac_1 - 1)));

    double eps_0 = std::fabs(sum_reac_0 - sum_prod_0)/(sum_reac_0 + sum_prod_0);
    double eps_1 = std::fabs(sum_reac_1 - sum_prod_1)/(sum_reac_1 + sum_prod_1);

    if (eps_0 < tol and eps_1 < tol) {return 1;}
    else {return 0;}

}

int run_pcf(VectorT& solution, int max_it, double tol)
{
    int cnt;
    for (cnt=0; cnt<max_it; ++cnt)
    {
        int ret = positive_continuous_fraction(solution, tol);
        if (ret > 0)
        {
            break;
        }
    }
    return cnt;
}

void onerun_map_aluminium_speciation_pcf(Answer& answer)
{
    int n = 2;
    VectorT solution(n);
    solution(0) = - answer.ph;
    solution(1) = answer.logAl;

    answer.iterations_pcf = run_pcf(solution, 100, 0.1);
    //std::cout << solution(0) << "\t" << solution(1) << std::endl;

    NewtonDriver driver(n, residual_base_log_aluminium_speciation);
    driver.get_tolerance()->max_iterations = 100;
    driver.get_tolerance()->residual_tolerance = 1e-10;
    driver.set_linesearch(Backtracking);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    if (ret != IterationSuccess) {
//        answer.iterations = 100;
//        //if (solution.coeff(0) > 14 or solution.coeff(0) < -1) {solution(0) = - answer.ph + 1;}
//        //if (solution.coeff(1) > -1 or solution.coeff(1) < -14) {solution(1) = answer.logAl + 1;}
//        solution(0) =  - answer.ph + 1.0;
//        solution(1) =  answer.logAl + 1.0;
//        answer.iterations_pcf += run_pcf(solution, 200, 0.05);
//        NewtonDriver driver2(n, residual_base_log_aluminium_speciation);
//        driver2.get_tolerance()->max_iterations = 50;
//        driver2.get_tolerance()->residual_tolerance = 1e-10;
//        driver2.set_linesearch(Backtracking);
//        ret = driver2.run_quasinewton_iterations(solution, 1);
//        if (ret != IterationSuccess) {
            answer.iterations = -1;

            VectorT residual(2);
            residual_base_log_aluminium_speciation(solution, residual);
            answer.residual = norm_2(residual);
//        }
//        else {
//            answer.iterations += driver.get_performance()->nb_iterations;
//            answer.ph = - solution(0);
//            answer.logAl = solution(1);
//        }

    }
    else
    {
        answer.iterations = driver.get_performance()->nb_iterations;
        answer.ph = - solution(0);
        answer.logAl = solution(1);
        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);
    }
}

void map_convergence_pcf()
{
    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            onerun_map_aluminium_speciation_pcf(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t" << solution.iterations_pcf <<  "\t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t" << solution.iterations_pcf << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }

        }
    }

 }

// ############### SIMPLEX conditionning #########################################

double simplex_residual(const VectorT& solution)
{
    double h     = std::pow(10.0, solution.coeff(0));
    double oh    = std::pow(10.0, - 14.0 -   solution.coeff(0));
    double al    = std::pow(10.0, solution.coeff(1));
    double aloh  = std::pow(10.0, -  5.0 -   solution.coeff(0) + solution.coeff(1));
    double aloh2 = std::pow(10.0, - 10.1 - 2*solution.coeff(0) + solution.coeff(1));
    double aloh3 = std::pow(10.0, - 16.9 - 3*solution.coeff(0) + solution.coeff(1));
    double aloh4 = std::pow(10.0, - 22.7 - 4*solution.coeff(0) + solution.coeff(1));
    double sum_reac_0;
    double sum_prod_0;


    if (-solution.coeff(0) < 1 or -solution.coeff(0) > 14) return 1e20;
    if (al < 1e-15 or al > 1) return 1e20;

    double T_0 = -3e-6;
    sum_reac_0 = -T_0 + h ;
    sum_prod_0 =  aloh + 2*aloh2 + 3*aloh3 + 4*aloh4 + oh;

    double sum_reac_1;
    double sum_prod_1;
    double T_1 = 1e-6;
    sum_reac_1 = al + aloh + aloh2 + aloh3 + aloh4;
    sum_prod_1 = T_1;

    double eps_0 = std::fabs(sum_reac_0 - sum_prod_0); //(sum_reac_0 + sum_prod_0);
    double eps_1 = std::fabs(sum_reac_1 - sum_prod_1); ///(sum_reac_1 + sum_prod_1);

    return eps_0 + eps_1;
}

double simplex_residual_2(const VectorT& solution)
{
    VectorT residual(2);
    residual_base_log_aluminium_speciation(solution, residual);
    return residual.dot(residual);
}

void onerun_map_aluminium_speciation_vertex(Answer& answer)
{
    int n = 2;
    Eigen::ArrayXXd start(3, 2);
    start << -(-1.0+answer.ph), (-1.0+answer.logAl),
             -(answer.ph), answer.logAl+1.0,
            -(1.0 + answer.ph), answer.logAl;

    SimplexSolver simplsol(start, simplex_residual_2);
    answer.iterations_pcf = simplsol.solve();

    VectorT solution = simplsol.get_best_approximation();


    NewtonDriver driver(n, residual_base_log_aluminium_speciation);
    driver.get_tolerance()->max_iterations = 100;
    driver.get_tolerance()->residual_tolerance = 1e-10;
    driver.set_linesearch(Backtracking);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    if (ret != IterationSuccess) {
        answer.iterations = -1;

        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);


    }
    else
    {
        answer.iterations = driver.get_performance()->nb_iterations;
        answer.ph = - solution(0);
        answer.logAl = solution(1);
        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);
    }
}

void map_convergence_simplex()
{
    std::cout << " ### Aluminium speciation - Map convergence - simplex ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            onerun_map_aluminium_speciation_vertex(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t" << solution.iterations_pcf <<  "\t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t" << solution.iterations_pcf << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }

        }
    }

 }


void print_res()
{

    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            VectorT solution(2);
            solution << -pH0, logAl0;
            VectorT res(2);
            residual_base_log_aluminium_speciation(solution, res);
            std::cout << -solution(0) << "\t" << solution(1) << "\t" << std::abs(res(0)) << "\t" << std::abs(res(1)) << "\t" << res.dot(res) << std::endl;
        }
    }
}
/*
struct LVFunctor: Eigen:: DenseFunctor<double>
{
    LVFunctor(): Eigen::DenseFunctor<double>(2,2) {}
    int operator()(const Eigen::Vector &x, Eigen::Vector &fvec)
    {
        residual_base_log_aluminium_speciation(x, fvec);
        return 0;
    }

};

void onerun_map_aluminium_speciation_levenberg(Answer& answer)
{
    int n = 2;
    VectorT solution(n);
    solution(0) = - answer.ph;
    solution(1) = answer.logAl;


    LVFunctor functor;
    LevenbergMarquardt<LVFunctor> lm(functor);
    info = lm.lmder1(solution);

    if (info != 1) {
        answer.iterations = -1;

        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);


    }
    else
    {
        answer.iterations = driver.get_performance()->nb_iterations;
        answer.ph = - solution(0);
        answer.logAl = solution(1);
        VectorT residual(2);
        residual_base_log_aluminium_speciation(solution, residual);
        answer.residual = norm_2(residual);
    }
}


void map_convergence_levenberg()
{
    std::cout << " ### Aluminium speciation - Map convergence - simplex ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            onerun_map_aluminium_speciation_levenberg(solution);


            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t" << solution.iterations_pcf <<  "\t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t" << solution.iterations_pcf << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }

        }
    }

 }
*/
int main()
{

    //test_simple_aluminium_speciation();
    //test_aluminium_speciation();
    //test_basis_aluminium_speciation();
    //map_convergence();
    //map_convergence_pcf();
    map_convergence_simplex();
    //map_convergence_levenberg();
    //print_res();
}
