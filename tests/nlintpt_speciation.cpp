#include "dfpmsolver/nlintpt/nlintpt.hpp"

#include <chrono>
#include <ctime>

const double A = 0.5092;
const double B = 0.3283;
const double Bdot = 0.001;

class Thermodata
{
public:
    Thermodata(const Eigen::MatrixXd& nu,
               const Eigen::VectorXd& logk,
               const Eigen::VectorXd& charges,
               const Eigen::VectorXd& ao):
        m_nu(nu),
        m_logk(logk),
        m_charges(charges),
        m_ao(ao)
    {}

    int nb_reactions() const {return m_nu.rows();}
    int nb_components() const {return m_nu.cols();}

    double logk(int r) const {
        return m_logk.coeff(r);
    }

    double k(int r) const {
        return std::pow(10.0, m_logk.coeff(r));
    }

    double nu(int r, int j) const {
        return m_nu.coeff(r, j);
    }

    double charge(int i) const {
        return m_charges(i);
    }

    double ao(int i) {return m_ao(i);}

private:
    Eigen::MatrixXd m_nu;
    Eigen::VectorXd m_logk;
    Eigen::VectorXd m_charges;
    Eigen::VectorXd m_ao;

};

class Speciation : public dfpmsolver::nlintpt::NonLinearProgram<Speciation, double>
{
public:
    Speciation(std::shared_ptr<Thermodata> thedata,
               Eigen::VectorXd& tot_conc):
        data(thedata),
        totconc(tot_conc),
        nc_a(thedata->nb_components()),
        nr_a(thedata->nb_reactions()),
        id_eq(2*nc_a+2*nr_a+1)
    {
        scaled_speciation = true;
        number_equations();
        l = Eigen::VectorXd::Constant(mneq, -30);
        u = Eigen::VectorXd::Constant(mneq, 0.0);
        b = Eigen::VectorXd::Constant(mneq, 0.0);
        r = Eigen::VectorXd::Constant(mneq, 1e-8);
        fudge_scaling = 1.0;
        fudge_scaling_g = 1.0;
    }

    void number_equations()
    {
        simple_system = true;
        id_eq = Eigen::VectorXi::Constant(2*nc_a+2*nr_a+1, -1);
        int neq =0;
        for (int i=0; i<nc_a; ++i)
        {
            id_eq(gsid_p(i)) = neq;
            ++neq;
        }
        for (int j=0; j<nr_a; ++j)
        {
            id_eq(gsid_s(j)) = neq;
            ++neq;
        }
        if (simple_system == false)
        {
            id_eq(gsid_i()) = neq;
            ++neq;
            for (int i=0; i<nc_a+nr_a; ++i)
            {
                if (charges(i) != 0)
                {
                    id_eq(gsid_g(i)) = neq;
                    ++neq;
                }
                else
                {
                    id_eq(gsid_g(i)) = -1;
                }
            }
        }
        mneq = neq;
    }

    int get_nb_eq() {return mneq;}
    int get_number_variables() {return mneq;}
    int get_number_constraints() {return mneq;}

    int gsid_p(int i) { return i;}
    int gsid_s(int i) { return i+nc_a;}
    int gsid_i() {return nc_a + nr_a;}
    int gsid_g(int i) {return gsid_i() + 1 + i;}
    int gsid(int i) {return i;}

    int gid(int i) {
        return id_eq(i);
    }

    int gid_p(int i) {
        return id_eq(gsid_p(i));
    }
    int gid_s(int i) {
        return id_eq(gsid_s(i));
    }
    int gid_i() {
        return id_eq(gsid_i());
    }
    int gid_g(int i) {
        return id_eq(gsid_g(i));
    }

    double logkr(int i) {return data->logk(i);}
    double nu(int j, int i) {return data->nu(j, i);}
    double charges(int i) {return data->charge(i);}
    double ao(int i) {return data->ao(i);}

    double logconc(int i, const Eigen::VectorXd& x) {
        assert(i < nc_a + nr_a);
        return x(gid(i));
    }
    double conc(int i, const Eigen::VectorXd& x) {
        return std::pow(10.0, logconc(i, x));
    }
    double logconc_primary(int i, const Eigen::VectorXd& x) {
        assert(i < nc_a);
        return x(gid_p(i));
    }
    double conc_primary(int i, const Eigen::VectorXd& x) {
        return std::pow(10.0, logconc_primary(i, x));
    }
    double logconc_secondary(int i, const Eigen::VectorXd& x) {
        assert(i < nr_a);
        return x(gid_s(i));
    }
    double conc_secondary(int i, const Eigen::VectorXd& x) {
        return std::pow(10.0, logconc_secondary(i, x));
    }
    double logionic_strength(const Eigen::VectorXd& x) {
        return x(gid_i());
    }
    double ionic_strength(const Eigen::VectorXd& x) {
        return std::pow(10.0, logionic_strength(x));
    }
    double loggamma(int i, const Eigen::VectorXd& x) {
        if (simple_system) return m_loggamma(i);
        if (gid_g(i) < 0) return 0;
        else return x(gid_g(i));
    }

    double tot_conc_residual(int i, const Eigen::VectorXd& x)
    {
        assert(i < nc_a);
        // R_i = 1 - c_i/T_i - sum(j,0,nr,nu_ji * c_j)/T_i
        double res = totconc(i) - conc_primary(i,x);
        for (int j=0;j<nr_a;++j)
        {
            res -= nu(j ,i)*conc_secondary(j, x);
        }
        if (scaled_speciation)
            return fudge_scaling * res/std::abs(totconc(i));
        else
            return res;
    }

    double aq_equilibrium_residual(int j, const Eigen::VectorXd& x)
    {
        assert(j < nr_a);
        // secondary species
        // R_j = logkr_j + x_j + logg_j -sum(i,0,nc,nu_ji*(x_i + logg_i))
        double res = logkr(j) + logconc_secondary(j, x) + loggamma(gsid_s(j), x);
        for (int i=0; i<nc_a;++i)
        {
            res -= nu(j,i)*(logconc_primary(i, x) + loggamma(gsid_p(i), x));
        }
        return res;
    }

    double ionic_strength_residual(const Eigen::VectorXd& x)
    {
        double res = ionic_strength(x);
        for (int i=0; i<nc_a+nr_a; ++i)
        {
            res -= conc(i, x)*charges(i)*charges(i);
        }
        return res/2;
    }

    double log_activity_coefficient(int id, double ionic_strength)
    {
        assert(id < nc_a + nr_a);
        return (-A*charges(id)*charges(id)*std::sqrt(ionic_strength))/(1+ao(id)*B*std::sqrt(ionic_strength));
    }

    double loggamma_residual(int i, const Eigen::VectorXd& x)
    {
        assert(i < nc_a+nr_a);
        assert(gid_g(i) >= 0);
        double reslogg = loggamma(i, x);
        reslogg -= log_activity_coefficient(i, ionic_strength(x));
        return fudge_scaling_g*reslogg;
    }

    double objective_function(const traits::vector_t& x) { return 0.0;}
    traits::vector_t gradient_objective_function(const traits::vector_t& x) {
         traits::vector_t gradient(traits::vector_t::Zero(get_number_variables()));
         return gradient;

    }
    void hessian_mod_objective_function(const traits::vector_t& x,
                                        traits::list_triplets_t& triplet_list,
                                        const traits::index_t offset_row,
                                        const traits::index_t offset_col)
   {}

    traits::vector_t constraints(const traits::vector_t& x)
    {
        traits::vector_t residual(get_number_constraints());
        // primary species
        for (int i=0;i<nc_a;++i)
        {
            residual(gid_p(i)) = tot_conc_residual(i, x);
        }
        // secondary species
        for (int j=0;j<nr_a;++j)
        {
            if (gid_s(j) >= 0) residual(gid_s(j)) = aq_equilibrium_residual(j, x);
        }
        // ionic strength
        if (gid_i() >= 0) residual(gid_i()) = ionic_strength_residual(x);
        // Activity coefficients
        for (int i=0; i<nc_a+nr_a; ++i)
        {
            if (gid_g(i) >= 0) residual(gid_g(i)) = loggamma_residual(i, x);
        }
        //std::cout << "Residual :\n----------\n" << residual << "\n---------\n";
        return residual;
    }

    void add_in_list(typename traits::list_triplets_t& triplet_list,
                     const typename traits::index_t offset_row,
                     const typename traits::index_t offset_col,
                     int row, int col, double value)
    {

        triplet_list.push_back(traits::triplet_t(offset_row+row,
                                                 offset_col+col,
                                                 value));
    }

    void jacobian_tot_conc(int i,
                           const typename traits::vector_t& x,
                           typename traits::list_triplets_t& triplet_list,
                           const typename traits::index_t offset_row,
                           const typename traits::index_t offset_col)
    {
        assert(i < nc_a);
        double unsurti = 1.0;
        if (scaled_speciation)
            unsurti = fudge_scaling/std::abs(totconc(i));
        //(p, p) => -1/Ti * c_i * log(10)
        add_in_list(triplet_list, offset_row, offset_col,
                    gid_p(i), gid_p(i),
                    -unsurti*conc_primary(i,x)*std::log(10.0));
        for (int j=0;j<nr_a;++j)
        {
            if (nu(j, i) !=0 )
            {
                add_in_list(triplet_list, offset_row, offset_col,
                            gid_p(i), gid_s(j),
                            -unsurti* nu(j,i)*conc_secondary(j,x)*std::log(10.0));
            }
        }

    }

    void jacobian_aq_equilibrium(int j,
                                 const typename traits::vector_t& x,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col)
    {
        assert(j < nr_a);
        add_in_list(triplet_list, offset_row, offset_col,
                    gid_s(j), gid_s(j), 1.0);
        for (int i=0; i<nc_a; ++i)
        {
            if (nu(j, i) != 0)
            {
                add_in_list(triplet_list, offset_row, offset_col,
                            gid_s(j), gid_p(i), -nu(j,i));
            }
        }
        if (gid_g(gsid_s(j)) >= 0)
        {
            add_in_list(triplet_list, offset_row, offset_col,
                        gid_s(j), gid_g(gsid_s(j)), 1.0);
            for (int i=0; i<nc_a; ++i)
            {
                if (nu(j, i) != 0)
                {
                    add_in_list(triplet_list, offset_row, offset_col,
                                gid_s(j), gid_g(gsid_p(i)), -nu(j,i));
                }
            }
        }
    }

    void jacobian_ionic_strength(const typename traits::vector_t& x,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col)
    {
        add_in_list(triplet_list, offset_row, offset_col,
                    gid_i(), gid_i(), ionic_strength(x)*std::log(10.0)/2);
        for (int i=0; i<nc_a+nr_a; ++i)
        {
            if (gid(i) >= 0 and charges(i) != 0)
                add_in_list(triplet_list, offset_row, offset_col,
                            gid_i(), gid(gsid(i)), -conc(i, x)*charges(i)*charges(i)*std::log(10.0)/2);
        }
    }

    double deriv_log_gamma_wrt_i(int id, double ionic_strength)
    {
        // A*zi^2/2*( B*ao/(B*I^(1/2)*ao + 1)^2 - 1/(I^(1/2)*(B*I^(1/2)*ao + 1)))
        double denom = B*std::sqrt(ionic_strength)*ao(id) + 1;
        double term = 1/(2*std::sqrt(ionic_strength)*denom*denom);
        return -A*charges(id)*charges(id)*term ;
    }

    void jacobian_loggamma(int i,
                           const typename traits::vector_t& x,
                           typename traits::list_triplets_t& triplet_list,
                           const typename traits::index_t offset_row,
                           const typename traits::index_t offset_col)
    {
        assert(i < nc_a+nr_a);
        add_in_list(triplet_list, offset_row, offset_col,
                    gid_g(i), gid_g(i), fudge_scaling_g);
        if (gid_i() >= 0)
                add_in_list(triplet_list, offset_row, offset_col,
                            gid_g(i), gid_i(),
                            -fudge_scaling_g*std::log(10)*ionic_strength(x)*deriv_log_gamma_wrt_i(i, ionic_strength(x)));
    }

    void jacobian_constraints(const typename traits::vector_t& x,
                              typename traits::list_triplets_t& triplet_list,
                              const typename traits::index_t offset_row,
                              const typename traits::index_t offset_col)
    {
        triplet_list.reserve(triplet_list.size() + 3*nc_a*nr_a+3*(nr_a+nc_a)+1);
        for (int i=0;i<nc_a;++i)
        {
            jacobian_tot_conc(i, x, triplet_list, offset_row, offset_col);
        }
        for (int j=0;j<nr_a;++j)
        {
            if (gid_s(j) >= 0) jacobian_aq_equilibrium(j, x, triplet_list, offset_row, offset_col);
        }
        if (gid_i() >= 0) jacobian_ionic_strength(x, triplet_list, offset_row, offset_col);
        for (int i=0; i<nc_a+nr_a; ++i)
        {
            if (gid_g(i) >= 0)
               jacobian_loggamma(i, x, triplet_list, offset_row, offset_col);
        }
//        Eigen::SparseMatrix<double> plop(2*mneq, 2*mneq);
//        plop.setFromTriplets(triplet_list.begin(), triplet_list.end());
//        std::cout << plop << std::endl;
        //for (auto it=triplet_list.begin(); it!=triplet_list.end();++it)
        //{
        //    std::cout << it->row() << " - " << it->col() << " : " << it->value() << std::endl;
        //}
    }

    double second_deriv_log_gamma_wrt_i(int id, double ionic_strength)
    {
        const double sqrti = std::pow(ionic_strength, 1.0/2.0);
        const double prod = B*sqrti*ao(id);
        const double nume = 1-prod;
        double denom = 1 + prod;
        denom = 4 * sqrti * std::pow(denom, 3.0);
        return A*charges(id)*charges(id)*nume/denom;
    }

    //! Return sum of + y_i times hessian_i
    void hessian_mod_constraints(const typename traits::vector_t& x,
                                 const typename traits::vector_t& y,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col)
    {
        const double sqlog10 = std::log(10.0)*std::log(10.0);
        triplet_list.reserve(triplet_list.size() +  nc_a*(1+nr_a) + nr_a + nc_a + nr_a +1);

        // primary species
        for (int i=0; i<nc_a; ++i)
        {
           // std::cout << "y(" << i << ") : " << y(i) << "  - conc : " << conc_primary(i,x) << " -- " << conc(i, x) << std::endl;
            double unsurti = 1.0;
            if (scaled_speciation)
                unsurti =  fudge_scaling/std::abs(totconc(i));
            add_in_list(triplet_list, offset_row, offset_col,
                        gid_p(i), gid_p(i),
                        -y(gid_p(i))*unsurti*conc_primary(i,x)*sqlog10);
            for (int j=0;j<nr_a;++j)
            {
                if (gid_s(j) >= 0 and nu(j,i) != 0)
                {
                    add_in_list(triplet_list, offset_row, offset_col,
                                gid_s(j), gid_s(j),
                                -y(gid_p(i))*unsurti*nu(j,i)*conc_secondary(j,x)*sqlog10);
                }
            }
        }
        // ionic strength
        if (gid_i() >= 0)
        {
            const double the_ionic_strength = ionic_strength(x);
            add_in_list(triplet_list, offset_row, offset_col,
                        gid_i(), gid_i(), y(gid_i())*the_ionic_strength*sqlog10/2.0);
            for (int i=0; i<nc_a+nr_a; ++i)
            {
                if (gid(i) >= 0 and charges(i) > 0)
                    add_in_list(triplet_list, offset_row, offset_col,
                                gid(i), gid(i), -y(gid_i())*conc(i, x)*charges(i)*charges(i)*sqlog10/2.0);
            }

        }
        // loggamma
        for (int i=0; i<nr_a+nc_a;++i)
        {
            if (gid_g(i) >= 0)
            {

                const double the_ionic_strength = ionic_strength(x);
                add_in_list(triplet_list, offset_row, offset_col,
                            gid_i(), gid_i(),
                     -fudge_scaling_g*y(gid_g(i))*the_ionic_strength*sqlog10*second_deriv_log_gamma_wrt_i(i, the_ionic_strength));
            }
        }
    }

    void init_conc_from_primary(traits::vector_t& x)
    {
        enforce_mass_excess(x);

        if (gid_i() >= 0)
        {
            double sum = 0;
            for (int i=0; i<nc_a+nr_a; ++i)
            {
                sum += conc(i, x)*charges(i)*charges(i);
            }
            //std::cout << "Sum : " << sum << std::endl;
            x(gid_i()) = std::log10(sum);
        }
        for (int i=0; i<nc_a+nr_a; ++i)
        {
            if (gid_g(i) >= 0)
            x(gid_g(i)) = log_activity_coefficient(i, ionic_strength(x));
        }
        if (simple_system)
        {
           m_loggamma = Eigen::VectorXd(nc_a+nr_a);
           double I =0;
           for (int i=0; i<nc_a+nr_a; ++i)
           {
               I += conc(i, x)*charges(i)*charges(i)/2.0;
           }
           for (int i=0; i<nc_a+nr_a; ++i)
           {
               m_loggamma(i) = log_activity_coefficient(i, I);
           }
        }
    }

    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& lower_bounds_constraints() {return b;}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& upper_bounds_constraints() {return r;}

    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& lower_bounds_variables() {return l;}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& upper_bounds_variables() {return u;}

    void hook_end_iter(typename traits::vector_t& x) {
        if (simple_system)
        {
           m_loggamma = Eigen::VectorXd(nc_a+nr_a);
           double I =0;
           for (int i=0; i<nc_a+nr_a; ++i)
           {
               I += conc(i, x)*charges(i)*charges(i)/2.0;
           }
           for (int i=0; i<nc_a+nr_a; ++i)
           {
               m_loggamma(i) = log_activity_coefficient(i, I);
           }
        }
    }


    void enforce_mass_excess(traits::vector_t& x)
    {
        bool tmpnoscaling = scaled_speciation;
        scaled_speciation = false;

        //std::cout << x(0) << " - " << x(1) << std::endl;
        int cnt =0;
        while(cnt < 100)
        {
            // compute secondary conc
            for (int r=0; r<nr_a; ++r)
            {
                if (gid_s(r) >= 0)
                {
                    double concr = - logkr(r);
                    for (int i=0; i<nc_a; ++i)
                    {
                        concr += nu(r, i)*logconc_primary(i, x);
                    }
                    if (concr < -10) concr = -10;
                    if (concr > -1)  concr = -1;
                    x(gid_s(r)) = concr;
                }
            }

            // find minimum of alpha i
            int minind  =-1;
            double alpha = 0;
            for (int i=0; i<nc_a; ++i)
            {
                double tmp = -tot_conc_residual(i, x);
                if (tmp < alpha)
                {
                    minind = i;
                    alpha = tmp;
                }

            }
            // make correction if needed
            if (minind != -1)
            {
                double ci = std::pow(10.0, x(minind));
                double dc = 0.5*ci;
                x(minind) = std::log10(ci + dc);
            }
            else
            {
                break;
            }
            ++cnt;
        }
        scaled_speciation = tmpnoscaling;
    }

private:
    std::shared_ptr<Thermodata> data;
    Eigen::VectorXd totconc;

    bool simple_system;
    bool scaled_speciation;

    int nc_a;
    int nr_a;
    Eigen::VectorXi id_eq;
    int mneq;

    Eigen::VectorXd l;
    Eigen::VectorXd u;
    Eigen::VectorXd b;
    Eigen::VectorXd r;

    Eigen::VectorXd m_loggamma;

    double fudge_scaling;
    double fudge_scaling_g;
};


struct Answer{
    double ph;
    double logAl;
    int iterations;
    double residual;
    int termcode;
};

void one_iter(Answer& answer)
{
    int nc = 2;
    int nr = 5;
    Eigen::MatrixXd nu(nr, nc);
    nu << -1, 1, -2, 1, -3, 1, -4, 1, -1, 0;
    Eigen::VectorXd logkr(nr);
    logkr << 5.0, 10.1, 16.9, 22.7, 14.0;
    Eigen::VectorXd charges(nc+nr);
    charges << 1, 3, 2, 1, 0, -1, -1;
    Eigen::VectorXd ao(nc+nr);
    ao << 9.0, 9.0, 5.4, 5.4, 0, 4.5 ,3.5;

    std::shared_ptr<Thermodata> data = std::make_shared<Thermodata>(nu, logkr, charges, ao);

    Eigen::VectorXd totconc(nc);
    totconc << -3e-3, 1e-3;
    std::shared_ptr<Speciation> specprog = std::make_shared<Speciation>(data, totconc);

    Eigen::VectorXd x(Eigen::VectorXd::Constant(specprog->get_number_variables(), 0));
    x(0) = - answer.ph;
    x(1) = answer.logAl;
    specprog->init_conc_from_primary(x);
    //std::cout << x << std::endl;

    dfpmsolver::nlintpt::NonLinearIntPoint<Speciation, double>  test(specprog);
    test.get_options().max_iter = 500;
    //test.get_options().tol_norm = 1e-5;
    //test.get_options().delta = 0.1;
    test.get_options().init_slack = 1.0;
    int retcode = test.optimize(x);
    dfpmsolver::nlintpt::NonLinearIntPointPerformance& perf = test.get_performance();
    if (retcode != 1)
    {
        answer.iterations = perf.nb_iterations;
        answer.termcode = retcode;
        answer.residual = specprog->constraints(x).norm();
    }
    else
    {
        answer.iterations = perf.nb_iterations;
        answer.logAl = x(1);
        answer.ph = -x(0);
        answer.residual = specprog->constraints(x).norm();
        answer.termcode = retcode;
    }
}

void test_map()
{
    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            one_iter(solution);
            if (solution.termcode != 1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << solution.termcode << "\t"
                          << solution.iterations << "\t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.termcode << "\t"
                          << solution.iterations << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }
        }
    }
}

void test()
{
    int nc = 2;
    int nr = 5;
    Eigen::MatrixXd nu(nr, nc);
    nu << -1, 1, -2, 1, -3, 1, -4, 1, -1, 0;
    Eigen::VectorXd logkr(nr);
    logkr << 5.0, 10.1, 16.9, 22.7, 14.0;
    Eigen::VectorXd charges(nc+nr);
    charges << 1, 3, 2, 1, 0, -1, -1;
    Eigen::VectorXd ao(nc+nr);
    ao << 9.0, 9.0, 5.4, 5.4, 0, 4.5 ,3.5;

    std::shared_ptr<Thermodata> data = std::make_shared<Thermodata>(nu, logkr, charges, ao);

    Eigen::VectorXd totconc(nc);
    totconc << -3e-3, 1e-3;
    std::shared_ptr<Speciation> specprog = std::make_shared<Speciation>(data, totconc);

    Eigen::VectorXd x(Eigen::VectorXd::Constant(specprog->get_number_variables(), 0));
    x(0) = -9.5;
    x(1) = -9;
    //x(7) = -6;
    //x(SpeciationProgram::n-1) = 1e-2;
    specprog->init_conc_from_primary(x);
    //std::cout << specprog->constraints(x) << std::endl;
    std::cout << "concentration : \n" << x.block(0,0,7,1) << std::endl
              ;//<< "Ionic Strength : " << x(7) << std::endl
              ;//<< "log gamma : \n " << x.block(8,0,6,1) << std::endl;
    dfpmsolver::nlintpt::NonLinearIntPoint<Speciation, double>  test(specprog);

    test.get_options().init_slack = 1.0;
    test.get_options().max_iter = 500;
    test.optimize(x);
    std::cout << "concentration : \n" << x.block(0,0,7,1) << std::endl
             ;// << "Ionic Strength : " << x(7) << std::endl
             ;// << "log gamma : \n " << x.block(8,0,6,1) << std::endl;

}

int main()
{
    //test();
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    //test();
    test_map();
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);


    std::cout << "finished computation at " << std::ctime(&end_time)
              << "elapsed time: " << elapsed_seconds.count() << "s\n";


    return EXIT_SUCCESS;
}
