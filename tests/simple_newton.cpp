/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : tests/simple_newton.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include <iostream>
#include <functional>
#include "dfpmsolver/newton_dense.hpp"

using namespace dfpmsolver;



// ###################################################################

void res_linear_system(const VectorT& solution, VectorT& residual)
{
    residual(0) = 3 - solution(0) - solution(1);
    residual(1) = 1 - solution(0) + solution(1);
}

void jac_linear_system(const VectorT& solution, Eigen::MatrixXd& jacobian)
{
    jacobian << -1, -1, -1, +1;
}


void test_solve_linear_system()
{
    std::cout << " ### Linear system ### " << std::endl;
    Eigen::VectorXd solution(2);
    solution << 0., 0.;

    Eigen::VectorXd good_solution(2);
    good_solution << 2.0, 1.0;


    NewtonDriver linear_driver(2, res_linear_system, jac_linear_system);
    IterationReturnCode ret = linear_driver.run_newton_iterations(solution);

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system" << std::endl;}
    else if (norm_inf(solution -good_solution) <= 1e-6) {std::cout << "Solution found" << std::endl;}
    else {std::cerr << "Wrong solution found : " << std::endl << solution << std::endl;}

    PerformanceCounter* perf = linear_driver.get_performance();

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;
}

// ############################################################################

void res_rosenbrock(const VectorT& solution, VectorT& residual)
{
    residual(0) = 10 * (solution(1) - solution(0)*solution(0));
    residual(1) = 1 - solution(0);
}

void jac_rosenbrock(const VectorT& solution, Eigen::MatrixXd& jacobian)
{
    jacobian << -20*solution(0), 10, -1, 0;
}


void test_rosenbrock()
{
    std::cout << " ### Rosenbrock ### " << std::endl;
    Eigen::VectorXd solution(2);
    solution << -120.0, 100.0;

    Eigen::VectorXd good_solution(2);
    good_solution << 1.0, 1.0;


    NewtonDriver driver(2, res_rosenbrock, jac_rosenbrock);
    IterationReturnCode ret = driver.run_newton_iterations(solution);
    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system" << std::endl;}
    else if (norm_inf(solution -good_solution) <= 1e-6) {std::cout << "Solution found" << std::endl;}
    else {std::cerr << "Wrong solution found : " << std::endl << solution << std::endl;}

    PerformanceCounter* perf = driver.get_performance();

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;

    std::cout << " ### Rosenbrock - FDJAC  ### " << std::endl;
    solution << -120.0, 100.0;
    NewtonDriver driver2(2, res_rosenbrock, jac_rosenbrock);
    ret = driver2.run_newton_iterations(solution);
    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system" << std::endl;}
    else if (norm_inf(solution -good_solution) <= 1e-6) {std::cout << "Solution found" << std::endl;}
    else {std::cerr << "Wrong solution found : " << std::endl << solution << std::endl;}

    perf = driver2.get_performance();

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;

}


void test_rosenbrock_quasi()
{
    std::cout << " ### Rosenbrock Quasi-Newton ### " << std::endl;
    Eigen::VectorXd solution(2);
    solution << -120.0, 100.0;

    Eigen::VectorXd good_solution(2);
    good_solution << 1.0, 1.0;


    NewtonDriver driver(2, res_rosenbrock, jac_rosenbrock);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 10);
    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system" << std::endl;}
    else if (norm_inf(solution -good_solution) <= 1e-6) {std::cout << "Solution found" << std::endl;}
    else {std::cerr << "Wrong solution found : " << std::endl << solution << std::endl;}

    PerformanceCounter* perf = driver.get_performance();

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;
}


// ######################## Trigonometric function ##########################

void trigo_residual(int n, const VectorT& solution, VectorT& residual)
{
    for (int i=0; i<n; ++i)
    {
        double sum = n;
        for (int j=0; j<n; ++j)
        {
            sum -= std::cos(solution(j)) + (i+1)*(1-std::cos(solution(i)))-std::sin(solution.coeff(i));
        }
        residual(i) = sum;
    }
}

void test_trigonometric()
{
    int n = 10;
    std::cout << " ### Trigonometric ### " << std::endl;
    VectorT solution(n);
    for (int i=0; i<n; ++i) solution(i)= 1.0/10;

    NewtonDriver driver(n, std::bind(trigo_residual, n, std::placeholders::_1, std::placeholders::_2));
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;
}

void test_trigonometric_strang()
{
    int n = 10;
    std::cout << " ### Trigonometric Strang ### " << std::endl;
    VectorT solution(n);
    for (int i=0; i<n; ++i) solution(i)= 1.0/10;

    NewtonDriver driver(n, std::bind(trigo_residual, n, std::placeholders::_1, std::placeholders::_2));
    driver.set_linesearch(Strang);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;

}

void test_trigonometric_backtracking()
{
    int n = 10;
    std::cout << " ### Trigonometric backtracking ### " << std::endl;
    VectorT solution(n);
    for (int i=0; i<n; ++i) solution(i)= 1.0/10;

    NewtonDriver driver(n, std::bind(trigo_residual, n, std::placeholders::_1, std::placeholders::_2));
    driver.set_linesearch(Backtracking);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;}

    std::cout << "Performance : " << std::endl
              << "    Iterations       -> " << perf->nb_iterations << std::endl
              << "    Nb call residual -> " << perf->nb_call_residual << std::endl
              << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;
}



int main ()
{
    test_solve_linear_system();
    test_rosenbrock();
    test_rosenbrock_quasi();
    test_trigonometric();
    test_trigonometric_strang();
    test_trigonometric_backtracking();
}


