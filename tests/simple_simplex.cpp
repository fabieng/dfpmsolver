/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : tests/simple_simplex.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include <iostream>
#include "dfpmsolver/simplex.hpp"

// linear system

double res_linear_system(const Eigen::VectorXd& solution)
{
    return std::pow(3 - solution(0) - solution(1), 2) + std::pow(1 - solution(0) + solution(1), 2);
}

void minim_linear()
{
    Eigen::ArrayXXd starting_point(3, 2);
    starting_point << 0, 0, 5, 5, 5, 0;
    dfpmsolver::SimplexSolver solver(starting_point, res_linear_system);
    std::cout << "Nb iter : " << solver.solve() << std::endl;
    std::cout << solver.get_best_approximation() << std::endl;
}

// rosenbrock

double res_rosenbrock(const Eigen::VectorXd& solution)
{
    return std::pow(10 * (solution(1) - solution(0)*solution(0)), 2) + std::pow( 1 - solution(0), 2);
}

void minim_rosenbrock()
{
    Eigen::ArrayXXd starting_point(3, 2);
    starting_point << 0, 2, 0, 0, 5, 3;
    dfpmsolver::SimplexSolver solver(starting_point, res_rosenbrock);
    std::cout << "Nb iter : " << solver.solve() << std::endl;
    std::cout << solver.get_best_approximation() << std::endl;
}


int main()
{
    minim_linear();
    minim_rosenbrock();


}
