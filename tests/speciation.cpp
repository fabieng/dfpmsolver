/*-------------------------------------------------------

        dfpmsolver : Newton methods and related

 - File : tests/aluminium_speciation.cpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#include <iostream>
#include <functional>
#include "dfpmsolver/newton_dense.hpp"
#include "dfpmsolver/simplex.hpp"
#include "test.h"

#include "dfpmsolver/nesolver.hpp"

// ipopt solver
#include "coin/IpTNLP.hpp"
#include "coin/IpIpoptData.hpp"
#include "coin/IpIpoptApplication.hpp"

#include <chrono>
#include <ctime>

using Index = Ipopt::Index;
using Number = Ipopt::Number;
// ############################### ReactionTable ###############################


using namespace dfpmsolver;


class ReactionTable
{
public:
    ReactionTable(const Eigen::MatrixXd& nu, const Eigen::VectorXd& logk):
        m_nu(nu),
        m_logk(logk)
    {}

    int nb_reactions() const {return m_nu.rows();}
    int nb_components() const {return m_nu.cols();}

    double logk(int r) const {
        return m_logk.coeff(r);
    }

    double k(int r) const {
        return std::pow(10.0, m_logk.coeff(r));
    }

    double nu(int r, int j) const {
        return m_nu.coeff(r, j);
    }

private:
    Eigen::MatrixXd m_nu;
    Eigen::VectorXd m_logk;

};

// ############################### ChemicalSystem ###############################

class ChemicalSystem : public Ipopt::ReferencedObject
{
public:

    ChemicalSystem(const ReactionTable& reactions):
        nb_primary(reactions.nb_components()),
        nb_secondary(reactions.nb_reactions()),
        m_reactions(reactions)
    {}

    void set_total_concentration(const Eigen::VectorXd& totconc) {
        m_tot_conc = totconc;
    }
    double primary_concentration(int j, const Number* logconc) const {
        return std::pow(10.0, logconc[j]);
    }

    double secondary_concentration(int r, const Number* logconc) const {
        return std::pow(10.0, logconc[nb_primary+r]);
    }

    double primary_log_concentration(int j, const Number* logconc) const {
        return logconc[j];
    }

    double secondary_log_concentration(int r, const Number* logconc) const {
        return logconc[nb_primary+r];
    }

    double primary_concentration(int j, const Eigen::VectorXd& logconc) const {
        return std::pow(10.0, logconc.coeff(j));
    }

    double secondary_concentration(int r, const Eigen::VectorXd& logconc) const {
        return std::pow(10.0, logconc.coeff(nb_primary+r));
    }

    double primary_log_concentration(int j, const Eigen::VectorXd& logconc) const {
        return logconc.coeff(j);
    }

    double secondary_log_concentration(int r, const Eigen::VectorXd& logconc) const {
        return logconc.coeff(nb_primary+r);
    }

    void residuals(const Eigen::VectorXd& logconc, Eigen::VectorXd& residual) const;
    void residuals_ipopt(const Number* logconc, Number* residual) const;

    //void jacobian(Eigen::VectorXd& logconc); // fancy stuff later

    double residual_mole_balance(int j, const Eigen::VectorXd& logconc) const;
    double residual_mole_balance(int j, const Number* logconc) const;
    double residual_equilibrium(int r, const Eigen::VectorXd& logconc) const;
    double residual_equilibrium(int r, const Number* logconc) const;

    int jacobian_ipopt_structure(Index* iRow, Index* iCol);
    int jacobian_ipopt_values(const Number* x, Number* values);
    int hessian_ipopt_structure(Index* iRow, Index* jCol);
    int hessian_ipopt_values(const Number *x, const Number *lambda, Number* values);


    void init_concentration_from_primary(Eigen::VectorXd& logconc);

    int get_nb_tot() const {return nb_primary + nb_secondary;}
    int get_nb_primary() const {return nb_primary;}
    int get_nb_secondary() const {return nb_secondary;}

    int secondary(int i) {return nb_primary +i;}



private:

    int nb_primary;
    int nb_secondary;

    Eigen::VectorXd m_tot_conc;

    ReactionTable m_reactions;
};

void ChemicalSystem::residuals(const Eigen::VectorXd& logconc, Eigen::VectorXd& residual) const
{
    for (int j=0; j<nb_primary;++j)
    {
        residual(j) = residual_mole_balance(j, logconc);
    }
    for (int r=0; r<nb_secondary;++r)
    {
        residual(r+nb_primary) = residual_equilibrium(r, logconc);
    }
    //std::cout << "Res : " << std::endl << residual << std::endl;
}


void ChemicalSystem::residuals_ipopt(const Number* logconc, Number* residual) const
{
    for (int j=0; j<nb_primary;++j)
    {
        residual[j] = residual_mole_balance(j, logconc);
    }
    for (int r=0; r<nb_secondary;++r)
    {
        residual[r+nb_primary] = residual_equilibrium(r, logconc);
    }
    //std::cout << "Res : " << std::endl << residual << std::endl;
}

double ChemicalSystem::residual_mole_balance(int j, const Eigen::VectorXd& logconc) const
{
    double sum = m_tot_conc(j) - primary_concentration(j, logconc);
    for (int r=0; r<nb_secondary; ++r)
    {
        sum -= m_reactions.nu(r, j)*secondary_concentration(r, logconc);
    }
    return sum;
}

double ChemicalSystem::residual_mole_balance(int j, const Number* logconc) const
{
    double sum = m_tot_conc(j) - primary_concentration(j, logconc);
    for (int r=0; r<nb_secondary; ++r)
    {
        sum -= m_reactions.nu(r, j)*secondary_concentration(r, logconc);
    }
    return sum;
}

double ChemicalSystem::residual_equilibrium(int r, const Eigen::VectorXd& logconc) const
{
    double sum = m_reactions.logk(r) + secondary_log_concentration(r, logconc);
    for (int j=0;j<nb_primary;++j)
    {
        sum -= m_reactions.nu(r, j)*primary_log_concentration(j, logconc);
    }
    return -sum;
}

double ChemicalSystem::residual_equilibrium(int r, const Number* logconc) const
{
    double sum = m_reactions.logk(r) + secondary_log_concentration(r, logconc);
    for (int j=0;j<nb_primary;++j)
    {
        sum -= m_reactions.nu(r, j)*primary_log_concentration(j, logconc);
    }
    return -sum;
}

void ChemicalSystem::init_concentration_from_primary(Eigen::VectorXd& logconc)
{
    for (int r=0; r<nb_secondary; ++r)
    {
        double concr = - m_reactions.logk(r);
        for (int j=0; j<nb_primary; ++j)
        {
            concr += m_reactions.nu(r, j)*logconc(j);
        }
        logconc(nb_primary+r) = concr;
    }
}


int ChemicalSystem::jacobian_ipopt_structure(Index* iRow, Index* iCol)
{
    Index nele = 0;
    for (Index j=0; j<nb_primary; ++j)
    {
        iRow[nele] = j;
        iCol[nele] = j;
        ++nele;

        for (Index r=0; r<nb_secondary; ++r)
        {
            iRow[nele] = j;
            iCol[nele] = secondary(r);
            ++nele;
        }
    }
    for (Index r=0; r<nb_secondary; ++r)
    {
        for (Index j=0; j<nb_primary; ++j)
        {
            iRow[nele] = secondary(r);
            iCol[nele] = j;
            ++nele;
        }
        iRow[nele] = secondary(r);
        iCol[nele] = secondary(r);
        ++nele;
    }
    return nele;
}


int ChemicalSystem::jacobian_ipopt_values(const Number *x, Number* values)
{
    const double ln10 = std::log(10);
    Index nele = 0;
    for (Index j=0; j<nb_primary; ++j)
    {
        values[nele] = -ln10*primary_concentration(j, x);
        ++nele;

        for (Index r=0; r<nb_secondary; ++r)
        {
            values[nele] = -ln10*m_reactions.nu(r, j) * secondary_concentration(r, x);
            //values[nele] = -m_reactions.nu(r, j) * (std::pow(10.0, x[secondary(r)]+1e-8)-std::pow(10.0, x[secondary(r)]))/1e-8;
            ++nele;
        }
    }
    for (Index r=0; r<nb_secondary; ++r)
    {
        for (Index j=0; j<nb_primary; ++j)
        {
            values[nele] = m_reactions.nu(r, j);
            ++nele;
        }
        values[nele] = -1;
        ++nele;
    }
    return nele;
}



int ChemicalSystem::hessian_ipopt_structure(Index* iRow, Index* jCol)
{
    for (int i=0; i<get_nb_tot(); ++i)
    {
        iRow[i] = i;
        jCol[i] = i;
    }
    return get_nb_tot();
}


int ChemicalSystem::hessian_ipopt_values(const Number *x, const Number* lambda, Number* values)
{
    for (Index i=0; i<get_nb_tot(); ++i)
    {
        values[i] = 0;
    }
    const double ln10 = std::log(10);
    for (Index j=0; j<get_nb_primary(); ++j)
    {
        values[j] += -lambda[j]*ln10*ln10*primary_concentration(j, x);
        for (Index r=0; r<get_nb_secondary(); ++r)
        {
            values[secondary(r)] += -lambda[j]*ln10*ln10*m_reactions.nu(r, j) *secondary_concentration(r, x);
        }
    }
    return get_nb_tot();
}

// ######################

struct Answer{
    double ph;
    double logAl;
    int iterations;
    double residual;
    int iterations_pcf;
    int termcode;
};


void aluminum_speciation(Answer& answer)
{
    Eigen::MatrixXd nu(5, 2);
    nu << -1, 1, -2, 1, -3, 1, -4, 1, -1, 0;

    Eigen::VectorXd logk(5);
    logk << 5.0, 10.1, 16.9, 22.7, 14.0;

    Eigen::VectorXd totconc(2);
    totconc << -3e-6, 1e-6;

    ReactionTable reactions(nu, logk);

    ChemicalSystem alum_system(reactions);
    alum_system.set_total_concentration(totconc);

    Eigen::VectorXd solution(7);
    solution(0) = - answer.ph;
    solution(1) = answer.logAl;
    alum_system.init_concentration_from_primary(solution);

    //std::cout << solution << std::endl << " ---------- " << std::endl;

    NewtonDriver driver(7, std::bind(std::mem_fn(&ChemicalSystem::residuals),
                                     alum_system,
                                     std::placeholders::_1,
                                     std::placeholders::_2));
    driver.get_tolerance()->max_iterations = 150;
    driver.set_linesearch(Backtracking);
    IterationReturnCode ret = driver.run_quasinewton_iterations(solution, 1);

    PerformanceCounter* perf = driver.get_performance();

    if (ret != IterationSuccess) {
        //std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;
        answer.iterations = -1;

        VectorT residual(7);
        alum_system.residuals(solution, residual);
        answer.residual = norm_2(residual);

    }
    else
    {
        answer.iterations = perf->nb_iterations;
        answer.ph = -solution(0);
        answer.logAl = solution(1);

        VectorT residual(7);
        alum_system.residuals(solution, residual);
        answer.residual = norm_2(residual);
    }
    //print_performance(std::cout, perf);

}


void map_convergence()
{
    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            aluminum_speciation(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }
        }
    }
 }

// using nesolver

void using_nesolver(Answer& answer)
{
    Eigen::MatrixXd nu(5, 2);
    nu << -1, 1, -2, 1, -3, 1, -4, 1, -1, 0;

    Eigen::VectorXd logk(5);
    logk << 5.0, 10.1, 16.9, 22.7, 14.0;

    Eigen::VectorXd totconc(2);
    totconc << -3e-3, 1e-3;

    ReactionTable reactions(nu, logk);

    ChemicalSystem alum_system(reactions);
    alum_system.set_total_concentration(totconc);

    Eigen::VectorXd solution(7);
    solution(0) = - answer.ph;
    solution(1) = answer.logAl;
    alum_system.init_concentration_from_primary(solution);
    //std::cout << solution << std::endl;

    //std::cout << solution << std::endl << " ---------- " << std::endl;

    NESolver driver(7, std::bind(std::mem_fn(&ChemicalSystem::residuals),
                                     alum_system,
                                     std::placeholders::_1,
                                     std::placeholders::_2));
    //driver.get_options().global = HookTrust;
    driver.get_options().maxstep = 10.0;
    driver.get_options().maxiter_maxstep = 10;
    //Eigen::VectorXd scalingf(7);
    //scalingf << 1/totconc(0),1/totconc(1),1,1,1,1,1;
    //driver.set_scaling_residual(scalingf);
    int termcode = driver.newton_driver(solution);
    VectorT residual(7);
    alum_system.residuals(solution, residual);
    answer.residual = norm_2(residual);
    answer.termcode = termcode;
    if ((termcode < NewtonSuccess)
            or (termcode == ErrorMinimized and answer.residual > 1e-8)
            ) {
        //std::cerr << "Error when solving linear system : " << std::endl << ret << " : " << perf->message;
        answer.iterations = -1;

    }
    else
    {
        answer.iterations = driver.get_nb_iter();
        answer.ph = -solution(0);
        answer.logAl = solution(1);
    }
    //std::cout << solution << std::endl;
    //std::cout << "------" << std::endl << "Termcode : " << termcode << std::endl;
}


void map_convergence_nesolver()
{
    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    Answer solution;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            solution.logAl = logAl0;
            solution.ph = pH0;
            using_nesolver(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t NaN \t NaN \t" <<
                             solution.residual << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                             "\t" << solution.residual << std::endl;
            }
        }
    }
 }

void oneiter_nesolver()
{
    double logAl0 = -9.423;
    double pH0 = 6.4745;
//    double logAl0 = -3.5;
//    double pH0 = 12.25;

    std::cout << " ### Aluminium speciation - NESolver ### " << std::endl;
    Answer solution;
    solution.logAl = logAl0;
    solution.ph = pH0;
    using_nesolver(solution);
    if (solution.iterations == -1)
    {
        std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t NaN \t NaN \t" <<
                     solution.residual << std::endl;
    }
    else
    {
        std::cout << pH0 << "\t" << logAl0 << "\t"
                  << solution.iterations << "\t"
                  << solution.ph << "\t"  << solution.logAl <<
                     "\t" << solution.residual << std::endl;
    }
    std::cout << "Termcode : " << solution.termcode << std::endl;
 }


// using ipopt solver
class SolveIpopt: public Ipopt::TNLP
{
public :
    SolveIpopt(Answer& answer):
        Ipopt::TNLP(),
        m_answer(answer)
    {
        Eigen::MatrixXd nu(5, 2);
        nu << -1, 1, -2, 1, -3, 1, -4, 1, -1, 0;

        Eigen::VectorXd logk(5);
        logk << 5.0, 10.1, 16.9, 22.7, 14.0;

        Eigen::VectorXd totconc(2);
        totconc << -3e-6, 1e-6;

        ReactionTable reactions(nu, logk);

        m_system = Ipopt::SmartPtr<ChemicalSystem>(new ChemicalSystem(reactions));
        m_system->set_total_concentration(totconc);
    }


    virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                              Index& nnz_h_lag, IndexStyleEnum& index_style);

    virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                                 Index m, Number* g_l, Number* g_u);


    virtual bool get_starting_point(Index n, bool init_x, Number* x,
                                    bool init_z, Number* z_L, Number* z_U,
                                    Index m, bool init_lambda, Number* lambda);

    virtual bool eval_f(Index n, const Number* x,
                        bool new_x, Number& obj_value)
    {
        obj_value = 0;
        return true;
    }

    virtual bool eval_grad_f(Index n, const Number* x, bool new_x,
                             Number* grad_f) {
        for (Index i=0; i<n; ++i) {
            grad_f[i] = 0;
        }
        return true;
    }

    virtual bool eval_g(Index n, const Number* x,
                        bool new_x, Index m, Number* g);

    virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
                            Index m, Index nele_jac, Index* iRow,
                            Index *jCol, Number* values);

    virtual bool eval_h(Index n, const Number* x, bool new_x,
                        Number obj_factor, Index m, const Number* lambda,
                        bool new_lambda, Index nele_hess, Index* iRow,
                        Index* jCol, Number* values);

    virtual void finalize_solution(Ipopt::SolverReturn status, Index n,
                                   const Number* x, const Number* z_L,
                                   const Number* z_U, Index m, const Number* g,
                                   const Number* lambda, Number obj_value,
                                   const Ipopt::IpoptData* ip_data,
                                   Ipopt::IpoptCalculatedQuantities* ip_cq);

private:
    Answer& m_answer;
    Ipopt::SmartPtr<ChemicalSystem> m_system;
};

bool SolveIpopt::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                          Index& nnz_h_lag, Ipopt::TNLP::IndexStyleEnum& index_style)
{
    n = m_system->get_nb_tot();
    m = m_system->get_nb_tot();

    nnz_jac_g =  (1+m_system->get_nb_secondary())*m_system->get_nb_primary() +  m_system->get_nb_secondary()*(1+m_system->get_nb_primary());
    nnz_h_lag = m_system->get_nb_tot(); // ?

    index_style = Ipopt::TNLP::C_STYLE;

    return true;
}

bool SolveIpopt::get_bounds_info(Index n, Number* x_l, Number* x_u,
                             Index m, Number* g_l, Number* g_u)
{
    assert(n==7);
    assert(m==7);

    for (Index i=0; i<7; ++i)
    {
        x_l[i] = -2e19;
        x_u[i] = 2e19;
    }
    for (Index i=0; i<7; ++i)
    {
        g_l[i] = g_u[i] = 0;
    }
    return true;
}

bool SolveIpopt::get_starting_point(Index n, bool init_x, Number* x,
                                bool init_z, Number* z_L, Number* z_U,
                                Index m, bool init_lambda, Number* lambda)
{
    assert(init_x == true);
    assert(init_z == false);
    assert(init_lambda == false);

    vector_t start(7);
    start(0) = -m_answer.ph  ;
    start(1) = m_answer.logAl;
    m_system->init_concentration_from_primary(start);

    for (Index i=0; i<7; ++i){
        x[i] = start(i);
    }
    return true;
}

bool SolveIpopt::eval_g(Index n, const Number* x,
                    bool new_x, Index m, Number* g)
{
    m_system->residuals_ipopt(x, g);
    return true;
}

bool SolveIpopt::eval_jac_g(Index n, const Number* x, bool new_x,
                        Index m, Index nele_jac, Index* iRow,
                        Index *jCol, Number* values)
{
    int nele;
    if (values == NULL)  {
        nele = m_system->jacobian_ipopt_structure(iRow, jCol);
    }
    else {
        nele = m_system->jacobian_ipopt_values(x, values);
    }
    assert(nele == nele_jac);
    return true;
}

bool SolveIpopt::eval_h(Index n, const Number* x, bool new_x,
                    Number obj_factor, Index m, const Number* lambda,
                    bool new_lambda, Index nele_hess, Index* iRow,
                    Index* jCol, Number* values)
{
    int nele;
    if (values == NULL)  {
        nele = m_system->hessian_ipopt_structure(iRow, jCol);
    }
    else {
        nele = m_system->hessian_ipopt_values(x, lambda, values);
    }
    assert(nele == nele_hess);
    return true;
}

void SolveIpopt::finalize_solution(Ipopt::SolverReturn status, Index n,
                               const Number* x, const Number* z_L,
                               const Number* z_U, Index m, const Number* g,
                               const Number* lambda, Number obj_value,
                               const Ipopt::IpoptData* ip_data,
                               Ipopt::IpoptCalculatedQuantities* ip_cq)
{
    if (status != Ipopt::SUCCESS) {
        m_answer.iterations = -1;
    }
    else
    {
        m_answer.logAl = x[1];
        m_answer.ph = - x[0];
        m_answer.iterations = ip_data->iter_count();
    }
    //printf("\n\nSolution of the primal variables, x\n");
    //for (Index i=0; i<n; i++) {
    //  printf("x[%d] = %e\n", i, x[i]);
    //}

}

void run_one_iteration(Answer& solution)
{
    Ipopt::ApplicationReturnStatus status;
    // Create a new instance of IpoptApplication
    //  (use a SmartPtr, not raw)
    // We are using the factory, since this allows us to compile this
    // example with an Ipopt Windows DLL
    Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();

    // Change some options
    // Note: The following choices are only examples, they might not be
    //       suitable for your optimization problem.
    app->Options()->SetNumericValue("tol", 1e-9);
    app->Options()->SetStringValue("mu_strategy", "adaptive");
    app->Options()->SetStringValue("output_file", "ipopt.out");
    app->Options()->SetStringValue("linear_solver", "mumps");
    app->Options()->SetStringValue("derivative_test", "second-order");
    app->Options()->SetIntegerValue("print_level", 0);

    // Intialize the IpoptApplication and process the options
    status = app->Initialize();
    if (status != Ipopt::Solve_Succeeded) {
        printf("\n\n*** Error during initialization!\n");
        return;
    }

    Ipopt::SmartPtr<Ipopt::TNLP> mynlp = new SolveIpopt(solution);
    status = app->OptimizeTNLP(mynlp);
    return;
}


int run_ipopt()
{


    std::cout << " ### Aluminium speciation - Map convergence ### " << std::endl;
    for (double pH0 = 1; pH0<14; pH0=pH0+0.25)
    {
        for (double logAl0 = -15; logAl0 <-1; logAl0=logAl0+0.25)
        {
            Answer solution;

            solution.logAl = logAl0;
            solution.ph = pH0;
            run_one_iteration(solution);
            if (solution.iterations == -1)
            {
                std::cout << pH0 << "\t" << logAl0 << "\t" << "150 \t NaN \t NaN " << std::endl;
            }
            else
            {
                std::cout << pH0 << "\t" << logAl0 << "\t"
                          << solution.iterations << "\t"
                          << solution.ph << "\t"  << solution.logAl <<
                          std::endl;
            }
        }
    }
    return 0;
}

int main()
{
    //map_convergence();
    //using_nesolver();
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    map_convergence_nesolver();
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    std::cout << "finished computation at " << std::ctime(&end_time)
              << "elapsed time: " << elapsed_seconds.count() << "s\n";
    //oneiter_nesolver();
    //run_ipopt();
}
