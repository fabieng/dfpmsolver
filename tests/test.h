#ifndef DFPM_TEST_H
#define DFPM_TEST_H

#include <ostream>
#include "dfpmsolver/newton.hpp"

inline std::ostream& print_performance(std::ostream& os, dfpmsolver::PerformanceCounter* perf)
{
    os << "Performance : " << std::endl
            << "    Iterations       -> " << perf->nb_iterations << std::endl
            << "    Nb call residual -> " << perf->nb_call_residual << std::endl
            << "    Nb call jacobian -> " << perf->nb_call_jacobian << std::endl;
    return os;
}

#endif //DFPM_TEST_H
