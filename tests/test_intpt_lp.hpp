/*-------------------------------------------------------

 - Module : tests
 - File : test_intpt_lp.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef TEST_INTPT_LP_HPP
#define TEST_INTPT_LP_HPP

#include <cxxtest/TestSuite.h>
#include <dfpmsolver/intpt/linear_problem.hpp>

//! \file test_intpt_lp.hpp test for linear problem

using namespace dfpmsolver::intpt;

class TestLinearProblem : public CxxTest::TestSuite
{
public:

    void test_initialization()
    {
        int n = 4;
        int m=2;
        LinearProblem apb(n,m);
        vector_t obj(n);
        obj << 6, 8, 5, 9;
        apb.set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        apb.lower_bounds_variables() = lowb;

        list_triplet_t truc(m*n);
        truc[0] = triplet_t(0, 0, 2);
        truc[1] = triplet_t(0, 1, 1);
        truc[2] = triplet_t(0, 2, 1);
        truc[3] = triplet_t(0, 3, 3);
        truc[4] = triplet_t(1, 0, 1);
        truc[5] = triplet_t(1, 1, 3);
        truc[6] = triplet_t(1, 2, 1);
        truc[7] = triplet_t(1, 3, 2);
        apb.set_constraints(truc);

        vector_t cons_up(m);
        cons_up << 5, 3;

        apb.upper_bounds_constraints() = cons_up;
    }
};


#endif // TEST_INTPT_LP_HPP
