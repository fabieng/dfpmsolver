/*-------------------------------------------------------

 - Module : tests
 - File : test_intpt_lp_intpt.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef TESTS_DFPM_INTPT_LP_INTPT
#define TESTS_DFPM_INTPT_LP_INTPT

//! \file test_intpt_lp_intpt.hpp tests for interior point method for linear programming;
//!

#include "dfpmsolver/intpt/lp_intpt.hpp"

using namespace dfpmsolver::intpt;

class TestLPIntPt : public CxxTest::TestSuite
{
public:

    // test if basic elimination works well
    void test_init()
    {
        lpptr_t lpptr = std::make_shared<LinearProblem>(3, 4);
        InteriorPointLP intlp(lpptr);
    }

    // simple system : solved by hand
    void test_stepbystep()
    {
        int n = 4;
        int m=2;
        lpptr_t lpptr = std::make_shared<LinearProblem>(n, m, 20);
        vector_t obj(n);
        obj << 6, 8, 5, 9;
        lpptr->set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        lpptr->lower_bounds_variables() = lowb;

        list_triplet_t truc(m*n);
        truc[0] = triplet_t(0, 0, 2);
        truc[1] = triplet_t(0, 1, 1);
        truc[2] = triplet_t(0, 2, 1);
        truc[3] = triplet_t(0, 3, 3);
        truc[4] = triplet_t(1, 0, 1);
        truc[5] = triplet_t(1, 1, 3);
        truc[6] = triplet_t(1, 2, 1);
        truc[7] = triplet_t(1, 3, 2);
        lpptr->set_constraints(truc);

        vector_t cons_up(m);
        cons_up << 5, 3;

        lpptr->upper_bounds_constraints() = cons_up;
        InteriorPointLP intlp(lpptr);
        intlp.get_options().init_slack = 1;
        intlp.get_options().delta = 0.02;
        intlp.initialization();
        vector_t x(vector_t::Zero(n));
        intlp.one_iteration(x);

        int plop;
        int cnt = 0;
        do
        {
            plop = intlp.one_iteration(x);
            ++cnt;
        } while (plop == 0 and cnt < 100);
        //std::cout << plop << " - " << cnt << std::endl;
        //std::cout << x << std::endl;

        TS_ASSERT(cnt == 16);

        vector_t good_sol(n);
        good_sol << 2, 0, 1, 0;
        TS_ASSERT_LESS_THAN_EQUALS((good_sol - x).norm(), 1e-8);

        TS_ASSERT_LESS_THAN_EQUALS(std::abs(intlp.primal_objective(x) - 17.0), 1e-8);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(intlp.dual_objective() - 17.0), 1e-8);
    }

    // simple system
    void test_solve()
    {
        int n = 4;
        int m=2;
        lpptr_t lpptr = std::make_shared<LinearProblem>(n, m, 20);
        vector_t obj(n);
        obj << 6, 8, 5, 9;
        lpptr->set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        lpptr->lower_bounds_variables() = lowb;

        list_triplet_t truc(m*n);
        truc[0] = triplet_t(0, 0, 2);
        truc[1] = triplet_t(0, 1, 1);
        truc[2] = triplet_t(0, 2, 1);
        truc[3] = triplet_t(0, 3, 3);
        truc[4] = triplet_t(1, 0, 1);
        truc[5] = triplet_t(1, 1, 3);
        truc[6] = triplet_t(1, 2, 1);
        truc[7] = triplet_t(1, 3, 2);
        lpptr->set_constraints(truc);

        vector_t cons_up(m);
        cons_up << 5, 3;

        lpptr->upper_bounds_constraints() = cons_up;
        InteriorPointLP intlp(lpptr);
        intlp.get_options().init_slack = 1;
        intlp.get_options().delta = 0.02;
        intlp.initialization();
        vector_t x(vector_t::Zero(n));
        ReturnCodeIntPtLP retcode = intlp.solve(x);

        TS_ASSERT(retcode == Optimal);

        vector_t good_sol(n);
        good_sol << 2, 0, 1, 0;
        TS_ASSERT_LESS_THAN_EQUALS((good_sol - x).norm(), 1e-8);

        TS_ASSERT_LESS_THAN_EQUALS(std::abs(intlp.primal_objective(x) - 17.0), 1e-8);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(intlp.dual_objective() - 17.0), 1e-8);
    }

    // infeasible system
    void test_solve_2()
    {
        int n = 2;
        int m = 3;
        lpptr_t lpptr = std::make_shared<LinearProblem>(n, m);
        vector_t obj(n);
        obj << 1, 3;
        lpptr->set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        lpptr->lower_bounds_variables() = lowb;

        list_triplet_t truc(m*n);
        truc[0] = triplet_t(0, 0, -1);
        truc[1] = triplet_t(0, 1, -1);
        truc[2] = triplet_t(1, 0, -1);
        truc[3] = triplet_t(1, 1, 1);
        truc[4] = triplet_t(1, 0, 1);
        truc[5] = triplet_t(1, 1, 2);
        lpptr->set_constraints(truc);

        vector_t cons_up(m);
        cons_up << -3, -1, 2;

        lpptr->upper_bounds_constraints() = cons_up;
        InteriorPointLP intlp(lpptr);
        intlp.get_options().init_slack = 1;
        intlp.get_options().delta = 0.02;
        intlp.initialization();
        vector_t x(vector_t::Zero(n));
        ReturnCodeIntPtLP retcode = intlp.solve(x);

        TS_ASSERT(retcode == DualInfeasible);
   }

    // Unbounded system
    void test_solve_3()
    {
        int n = 2;
        int m = 3;
        lpptr_t lpptr = std::make_shared<LinearProblem>(n, m, 1e50);
        vector_t obj(n);
        obj << 1, 3;
        lpptr->set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        lpptr->lower_bounds_variables() = lowb;

        list_triplet_t truc(m*n);
        truc[0] = triplet_t(0, 0, -1);
        truc[1] = triplet_t(0, 1, -1);
        truc[2] = triplet_t(1, 0, -1);
        truc[3] = triplet_t(1, 1, 1);
        truc[4] = triplet_t(1, 0, -1);
        truc[5] = triplet_t(1, 1, 2);
        lpptr->set_constraints(truc);

        vector_t cons_up(m);
        cons_up << -3, -1, 2;

        lpptr->upper_bounds_constraints() = cons_up;
        InteriorPointLP intlp(lpptr);
        intlp.get_options().init_slack = 1;
        intlp.get_options().delta = 0.02;
        intlp.initialization();
        vector_t x(vector_t::Zero(n));
        ReturnCodeIntPtLP retcode = intlp.solve(x);

        TS_ASSERT(retcode == PrimalInfeasible);
   }

    // more important system with every bound
    void test_solve_4()
    {
        int n = 40;
        int m = 10;
        lpptr_t lpptr = std::make_shared<LinearProblem>(n, m, 1e19);
        vector_t obj(n);
        for (int i=0; i<n;++i) obj(i) = 1;
        lpptr->set_objective(obj);
        vector_t lowb(vector_t::Zero(n));
        lpptr->lower_bounds_variables() = lowb;
        vector_t highb(vector_t::Constant(n, 40));
        lpptr->upper_bounds_variables() = highb;


        list_triplet_t truc(m*3);
        for (int i=0; i<m; ++i)
        {
            truc[3*i]   = triplet_t(i, 2*i, 1);
            truc[3*i+1] = triplet_t(i, 2*i+10, -1);
            truc[3*i+2] = triplet_t(i, 2*i+20, 1);

        }
        lpptr->set_constraints(truc);

        vector_t cons_up(vector_t::Constant(m, 10));
        lpptr->upper_bounds_constraints() = cons_up;
        vector_t low_up(vector_t::Zero(m));
        lpptr->lower_bounds_constraints() = low_up;

        InteriorPointLP intlp(lpptr);
        intlp.get_options().init_slack = 10;
        intlp.get_options().delta = 0.02;
        intlp.get_options().max_iter = 20;
        intlp.initialization();
        vector_t x(vector_t::Constant(n, 10));
        ReturnCodeIntPtLP retcode = intlp.solve(x);

        TS_ASSERT_EQUALS(retcode, Optimal);

   }
};


#endif // TESTS_DFPM_INTPT_LP_INTPT
