#ifndef TEST_NESOLVER_HPP
#define TEST_NESOLVER_HPP

#include <cxxtest/TestSuite.h>
#include "dfpmsolver/nesolver.hpp"

using namespace dfpmsolver;

//# test residual

void simple_residual(const vector_t& x, vector_t& f)
{
    f(0) = x(0);
    f(1) = x(1);
}


void simple_residual_2(const vector_t& x, vector_t& f)
{
    f(0) = 3*x(0) + 2*x(1);
    f(1) = 5*x(0) + 6*x(1);
}

void res_rosenbrock(const vector_t& solution, vector_t& residual)
{
    residual(0) = 10 * (solution(1) - solution(0)*solution(0));
    residual(1) = 1 - solution(0);
}

void jac_rosenbrock(const vector_t& solution, matrix_t& jacobian)
{
    jacobian << -20*solution(0), 10, -1, 0;
}



class TestSuite_Decomposition : public CxxTest::TestSuite
{
public :

    void test_qr_solve()
    {
        matrix_t M(2, 2);
        M << 2, 0, 0, 2;
        vector_t M1(2);
        vector_t M2(2);

        vector_t sol(2);
        sol << 6, 4;
        vector_t good_sol(2);
        good_sol << 3, 2;

        qr_decomposition(M, M1, M2);
        qr_solve(M, M1, M2, sol);
        TS_ASSERT_vector_are_almost_equal(sol, good_sol);
    }

    void test_qr_solve_2()
    {
        matrix_t M(2, 2);
        M << 4, 5, 6, 3;
        vector_t M1(2);
        vector_t M2(2);

        vector_t sol(2);
        sol << 6, 3;
        vector_t good_sol(2);
        good_sol << -1.0/6.0, 4.0/3.0;

        qr_decomposition(M, M1, M2);
        qr_solve(M, M1, M2, sol);

    }

    void test_cholesky_decomp()
    {
        matrix_t test(3, 3);
        test << 4, 12, -16, 12, 37, -43, -16, -43, 98;

        matrix_t good_l(3, 3);
        good_l << 2, 0, 0, 6, 1, 0, -8, 5, 3;

        matrix_t L(matrix_t::Zero(3, 3));
        cholesky_decomposition(test, 0, L);
        TS_ASSERT_matrix_are_almost_equal(L, good_l);
    }

    void test_cholesky_solve()
    {
        matrix_t test(3, 3);
        test << 4, 12, -16, 12, 37, -43, -16, -43, 98;

        vector_t grad(3);
        grad << -3, -3, -3;
        vector_t solution(vector_t::Zero(3));

        vector_t good_solution(3);
        good_solution << 113.75, -31.0, 5.0;

        matrix_t L(matrix_t::Zero(3, 3));
        cholesky_decomposition(test, 0, L);
        cholesky_solve(L, grad, solution);
        TS_ASSERT_vector_are_almost_equal(solution, good_solution);
    }



private:
    double norm_inf(const vector_t& x1)
    {
        return (x1).cwiseAbs().maxCoeff();
    }

    void TS_ASSERT_vector_are_almost_equal(const vector_t& x1, const vector_t& x2, double tol=1e-8)
    {
        TS_ASSERT_LESS_THAN_EQUALS(norm_inf(x1 - x2), tol);
    }
    void TS_ASSERT_matrix_are_almost_equal(const matrix_t& x1, const matrix_t& x2, double tol=1e-6)
    {
        TS_ASSERT(x1.isApprox(x2, tol));
    }
};

class TestSuite_NESolver : public CxxTest::TestSuite
{
public:

    void test_residual_comp()
    {
        NESolver solver(2, simple_residual);
        vector_t solution(2);
        solution << 1.0, 1.0;
        solver.compute_residual(solution);
        TS_ASSERT_vector_are_almost_equal(solution, solver.get_residual_ref());
        TS_ASSERT_EQUALS(solver.squared_residual(solution),  1.0);

    }


    void test_jacobian_comp()
    {
        NESolver solver(2, simple_residual);
        vector_t solution(2);
        solution << 1.0, 1.0;
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);
        TS_ASSERT_matrix_are_almost_equal(Eigen::MatrixXd::Identity(2, 2), solver.get_jacobian_ref());
    }

    void test_jacobian_comp_2()
    {
        NESolver solver(2, simple_residual_2);
        vector_t solution(2);
        solution << 1.0, 1.0;
        matrix_t good_sol(2, 2);
        good_sol << 3, 2, 5, 6;
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);
        TS_ASSERT_matrix_are_almost_equal(good_sol, solver.get_jacobian_ref());
    }

    void test_qrdecomp()
    {
        matrix_t test_1(2, 2);
        test_1 << 1, 1, 0, 1;
        vector_t m1_1(2);
        vector_t m2_1(2);

        bool truc = qr_decomposition(test_1, m1_1, m2_1);

        matrix_t test(3, 3);
        test << 12, -51, 4, 6, 167, -68, -4, 24, -41;
        vector_t m1(3);
        vector_t m2(3);

        truc = qr_decomposition(test, m1, m2);
    }


    void test_nemodel()
    {

        vector_t update(2);
        matrix_t M(2, 2);
        vector_t M2(2);
        matrix_t H(2, 2);
        matrix_t L(2, 2);
        vector_t solution(2);
        solution << 1.0, 1.0;

        NESolver solver(2, simple_residual);
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);

        solver.newton_model(update, M, M2, H, L);
        TS_ASSERT_vector_are_almost_equal(update, -solution);
    }



    void test_driver()
    {
        vector_t solution(2);
        solution << 1.0, 1.0;
        vector_t good_solution(2);
        good_solution << 0.0, 0.0;

        NESolver solver(2, simple_residual);
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);

        solver.newton_driver(solution);
        TS_ASSERT_vector_are_almost_equal(solution, good_solution);
    }

    void test_driver_2()
    {
        vector_t solution(2);
        solution << 1.0, 1.0;
        vector_t good_solution(2);
        good_solution << 0.0, 0.0;

        NESolver solver(2, simple_residual_2);
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);

        solver.newton_driver(solution);
        TS_ASSERT_vector_are_almost_equal(solution, good_solution);
    }

    void test_driver_rosenbrock()
    {
        vector_t solution(2);
        solution << -1.2, 1.0;
        vector_t good_solution(2);
        good_solution << 1.0, 1.0;

        NESolver solver(2, res_rosenbrock);
        solver.set_anal_jac(jac_rosenbrock, true);
        solver.compute_residual(solution);
        solver.compute_jacobian(solution);

        solver.newton_driver(solution);
        TS_ASSERT_vector_are_almost_equal(solution, good_solution);
    }

private:

    double norm_inf(const vector_t& x1)
    {
        return (x1).cwiseAbs().maxCoeff();
    }

    void TS_ASSERT_vector_are_almost_equal(const vector_t& x1, const vector_t& x2, double tol=1e-6)
    {
        TS_ASSERT_LESS_THAN_EQUALS(norm_inf(x1 - x2), tol);
    }
    void TS_ASSERT_matrix_are_almost_equal(const matrix_t& x1, const matrix_t& x2, double tol=1e-6)
    {
        TS_ASSERT(x1.isApprox(x2, tol));
    }
};

#endif // TEST_NESOLVER_HPP
