/*-------------------------------------------------------

 - Module : test
 - File : test_nlintpt.hpp
 - Author : Fabien Georget

 Copyright (c) 2014, Fabien Georget, Princeton University

---------------------------------------------------------*/

#ifndef TEST_NL_INTPT_HPP
#define TEST_NL_INTPT_HPP

#include "cxxtest/TestSuite.h"
#include "dfpmsolver/nlintpt/nlintpt.hpp"

using namespace dfpmsolver::nlintpt;

class NonLinearProgramTest: public NonLinearProgram<NonLinearProgramTest,double>
{
public:
    static const int n = 3;
    static const int m = 2;

    NonLinearProgramTest():
        lower_var(traits::vector_t::Zero(n)),
        upper_var(traits::vector_t::Constant(n,10.0)),
        lower_cons(traits::vector_t::Zero(m)),
        upper_cons(traits::vector_t::Constant(m,10.0))
    {}

    // Return the number of variables
    int get_number_variables() {return n;}
    // Return the number of constraints
    int get_number_constraints() {return m;}

    //! Return objective function value
    double objective_function(const traits::vector_t& x) {
        assert(x.rows() == n);
        return x(0)-x(1)+x(2)*x(2);
    }
    //! Return gradient of objective function
    traits::vector_t gradient_objective_function(const traits::vector_t& x) {
        assert(x.rows() == n);
        traits::vector_t gradient(n);
        gradient << 1, -1, 2*x(2);
        return gradient;
    }
    //! Return minus hessian of objective function
    void hessian_mod_objective_function(const typename traits::vector_t& x,
                                        typename traits::list_triplets_t& triplet_list,
                                        const typename traits::index_t offset_row,
                                        const typename traits::index_t offset_col)
    {
        triplet_list.reserve(triplet_list.size() + 1);
        triplet_list.push_back(traits::triplet_t(offset_row+2,offset_col+2,-2));
    }

    //! Direct access to vector of lower bounds
    traits::vector_t& lower_bounds_variables() { return lower_var;}
    //! Direct access to vector of upper bounds
    traits::vector_t& upper_bounds_variables() { return upper_var;}

    //! Return value constraints
    traits::vector_t constraints(const traits::vector_t& x) {
        traits::vector_t vector(get_number_constraints());
        vector << x(0)+x(1), x(1)*x(1)+x(2);
        return vector;
    }
    //! Return Jacobian constraints
    void jacobian_constraints(const typename traits::vector_t& x,
                              typename traits::list_triplets_t& triplet_list,
                              const typename traits::index_t offset_row,
                              const typename traits::index_t offset_col) {

        triplet_list.reserve(triplet_list.size() + 4);
        triplet_list.push_back(traits::triplet_t(offset_row,offset_col,x(1)));
        triplet_list.push_back(traits::triplet_t(offset_row,offset_col+1,x(0)));
        triplet_list.push_back(traits::triplet_t(offset_row+1,offset_col+1,2*x(1)));
        triplet_list.push_back(traits::triplet_t(offset_row+1,offset_col+2,1.0));
    }
    //! Return sum of + y_i times hessian_i
    void hessian_mod_constraints(const typename traits::vector_t& x,
                                 const typename traits::vector_t& y,
                                 typename traits::list_triplets_t& triplet_list,
                                 const typename traits::index_t offset_row,
                                 const typename traits::index_t offset_col)
    {
        triplet_list.reserve(triplet_list.size() + 3);
        triplet_list.push_back(traits::triplet_t(offset_row,offset_col+1, 1.0*y(0)));
        triplet_list.push_back(traits::triplet_t(offset_row+1,offset_col, 1.0*y(0)));
        triplet_list.push_back(traits::triplet_t(offset_row+1,offset_col+1, 2.0*y(1)));
    }


    //! Direct access to vector of lower bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& lower_bounds_constraints() {return lower_cons;}
    //! Direct access to vector of upper bounds
    //!
    //! Breaks encapsulation, but needed for performance
    typename traits::vector_t& upper_bounds_constraints() {return upper_cons;}

private:
    traits::vector_t lower_var;
    traits::vector_t upper_var;
    traits::vector_t lower_cons;
    traits::vector_t upper_cons;
};


class TestNLIntPt : public CxxTest::TestSuite
{
public:
    void test_truc() {
        Eigen::VectorXd x(Eigen::VectorXd::Zero(NonLinearProgramTest::n));
        NonLinearIntPoint<NonLinearProgramTest, double>  test(std::make_shared<NonLinearProgramTest>());
        test.get_options().max_iter = 100;
        test.optimize(x);
        std::cout << x << std::endl;
    }

};



#endif // TEST_NL_INTPT_HPP
