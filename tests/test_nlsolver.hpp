#include "dfpmsolver/nlsolver/nlsolver.hpp"

using namespace dfpmsolver::nlsolver;

class LinearSystem: public NLSystem<double, false>
{
public:
    const int n = 2;

    LinearSystem() {
        m_scaling_x = types::vector_t(2);
        m_scaling_x << 1, 1;
        m_scaling_f = types::vector_t(2);
        m_scaling_f << 1, 1;
    }

    int get_number_variables() {return n;}

    void get_residual(const typename types::vector_t& x,
                      typename types::vector_t& residual)
    {
        residual << x(0) + x(1) - 2,  x(0) - x(1) - 3;
    }

    void get_jacobian(const typename types::vector_t& x,
                      typename types::matrix_t& jacobian)
    {
        jacobian << 1, 1, 1, -1;
    }

    const typename types::vector_t& scaling_x() const {return m_scaling_x;}
    double& scaling_x(int i) {return m_scaling_x(i);}
    const typename types::vector_t& scaling_f() const {return m_scaling_f;}
    double& scaling_f(int i) {return m_scaling_f(i);}

private:
    types::vector_t m_scaling_x;
    types::vector_t m_scaling_f;

};


class NonLinearSystem: public NLSystem<double, false>
{
public:
    // Rosenbrock function
    const int n = 2;

    NonLinearSystem() {
        m_scaling_x = types::vector_t(2);
        m_scaling_x << 1, 1;
        m_scaling_f = types::vector_t(2);
        m_scaling_f << 1, 1;
    }

    int get_number_variables() {return n;}

    void get_residual(const typename types::vector_t& x,
                      typename types::vector_t& residual)
    {
        residual << 10*(x(1) - x(0)*x(0)),  1 - x(0);
    }

    void get_jacobian(const typename types::vector_t& x,
                      typename types::matrix_t& jacobian)
    {
        jacobian << -20*x(0), 10, -1, 0;
    }

    const typename types::vector_t& scaling_x() const {return m_scaling_x;}
    double& scaling_x(int i) {return m_scaling_x(i);}
    const typename types::vector_t& scaling_f() const {return m_scaling_f;}
    double& scaling_f(int i) {return m_scaling_f(i);}

private:
    types::vector_t m_scaling_x;
    types::vector_t m_scaling_f;

};

class SparseLinearSystem: public NLSystem<double, true>
{
public:
    const int n = 6;

    SparseLinearSystem() {
        m_scaling_x = types::vector_t::Constant(6, 1);
        m_scaling_f = types::vector_t::Constant(6, 1);
    }

    int get_number_variables() {return n;}

    void get_residual(const typename types::vector_t& x,
                      typename types::vector_t& residual)
    {
        residual << 2*x(0)+x(1) - 2,
                    2*x(1)+x(2) - 2,
                    2*x(2)+x(3) - 2,
                    2*x(3)+x(4) - 2,
                    2*x(4)+x(5) - 2,
                    2*x(5)+x(0) - 2;
    }


    void get_structure_jacobian(typename types::list_triplets_t& triplet_list)
    {
        triplet_list.reserve(n*2);
        for (int i=0; i<n; ++i)
        {
            add_nonzero(triplet_list, i, i);
            add_nonzero(triplet_list, i, (i+1) % n);
        }
    }

    void get_jacobian(const typename types::vector_t& x,
                      typename types::matrix_t& jacobian)
    {
        for (int i=0; i<n; ++i)
        {
            jacobian.coeffRef(i, i) = 2;
            jacobian.coeffRef(i, (i+1) % n) = 1;
        }
    }

    const typename types::vector_t& scaling_x() const {return m_scaling_x;}
    double& scaling_x(int i) {return m_scaling_x(i);}
    const typename types::vector_t& scaling_f() const {return m_scaling_f;}
    double& scaling_f(int i) {return m_scaling_f(i);}

private:
    types::vector_t m_scaling_x;
    types::vector_t m_scaling_f;

};

class name : public CxxTest::TestSuite
{
public:

    void test_linear_atest()
    {
        std::shared_ptr<LinearSystem> psys = std::make_shared<LinearSystem>();
        NLSolver<LinearSystem, double, false> solver(psys);
        Eigen::VectorXd x(psys->get_number_variables());
        x << 0, 0;
        NLSolverReturnCode retcode = solver.solve<GlobalMethod::Backtracking>(x);
        TS_ASSERT(retcode > NLSolverReturnCode::NewtonSuccess);
        TS_ASSERT_EQUALS(solver.get_performance().nb_iterations, 1);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(0) - 2.5), 1e-6);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(1) + 0.5), 1e-6);

    }

    void test_nonlinear_test()
    {
        std::shared_ptr<NonLinearSystem> psys = std::make_shared<NonLinearSystem>();
        NLSolver<NonLinearSystem, double, false> solver(psys);
        Eigen::VectorXd x(psys->get_number_variables());
        x << -1.2, 0;
        NLSolverReturnCode retcode = solver.solve<GlobalMethod::Backtracking>(x);
        TS_ASSERT(retcode > NLSolverReturnCode::NewtonSuccess);
        TS_ASSERT_EQUALS(solver.get_performance().nb_iterations, 2);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(0) - 1.0), 1e-6);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(1) - 1.0), 1e-6);

    }

    void test_sparse_linear_test()
    {
        std::shared_ptr<SparseLinearSystem> psys = std::make_shared<SparseLinearSystem>();
        NLSolver<SparseLinearSystem, double, true> solver(psys);
        Eigen::VectorXd x(psys->get_number_variables());
        x << -1.2, 0;
        NLSolverReturnCode retcode = solver.solve<GlobalMethod::Backtracking>(x);
        TS_ASSERT(retcode > NLSolverReturnCode::NewtonSuccess);
        TS_ASSERT_EQUALS(solver.get_performance().nb_iterations, 2);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(0) - 1.0), 1e-6);
        TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(1) - 1.0), 1e-6);

    }
};
